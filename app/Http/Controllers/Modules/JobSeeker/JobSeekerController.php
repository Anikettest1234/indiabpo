<?php

namespace App\Http\Controllers\Modules\JobSeeker;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Model\Category;
use App\Model\Job;
use Carbon\Carbon;

class JobSeekerController extends Controller
{	

	/**
    * Method : index
    * Description : Job seeker page of this project
    * Author : Rajib
    * Date : 29-06-2020
    */
    public function index(Request $request , $slug = NULL) {
	
    	$data['categories'] = Category::with('subCategory')->where(['parent_id' => 0, 'status' => 'A'])->get();
        $jobseeker = User::with('category', 'subCategory')
                         ->where(['user_type' => 'J', 'status' => 'A'])
                         ->orderBy('id', 'desc');

        if ($slug) {
            $category = Category::where('slug', $slug)->first();
            if($category->parent_id==0) {
                $jobseeker = $jobseeker->where('category_id', $category->id);
            } else {
                $jobseeker = $jobseeker->where('sub_category_id', $category->id);
            }
        }

        if(@$request->keyword){
            $jobseeker = $jobseeker->where(function($q) use ($request) {
                $q = $q->where('name', 'like', '%'.$request->keyword.'%');
            });
        }

        $jobseeker = $jobseeker->orderBy('id', 'desc')->get();
        if(@$request->lat && @$request->lng) {
            $radious    = @$value->radius ? $value->radius : 1000;             
            $lat        = $request->lat;
            $lng        = $request->lng;
            $jobseeker    = $jobseeker->filter(function ($val) use ($lat, $lng, $radious) {
                $latitudeTo         = @$val->lat;
                $longitudeTo        = @$val->lng;
                $nearByDistance     = $this->haversineGreatCircleDistance($lat, $lng, $latitudeTo, $longitudeTo);
                $val->distanceInKm  = $nearByDistance;
                $val->distance      = $this->fnToGetCalculatedDistamce($nearByDistance);
                return $nearByDistance <= $radious;
            })->sortBy('distanceInKm');
        }

        if($request['min_age'] || $request['max_age']){
            $maxAge = (int) 10000;
            $minAge = (int) $request['min_age'];
            if($request['max_age']){
                $maxAge = (int) $request['max_age'];
            }
            $minDate = Carbon::today()->subYears($maxAge); 
            $maxDate = Carbon::today()->subYears($minAge)->endOfDay();
            $jobseeker = $jobseeker->whereBetween('dob', [$minDate, $maxDate]);
        }

        if($request['min_salary']){
            $minFilter = (int) $request['min_salary'];
            $jobseeker = $jobseeker->where('min_salary', '>=', $minFilter);
        }

        if($request['max_salary']){
            $maxFilter = (int) $request['max_salary'];
            $jobseeker = $jobseeker->where('max_salary', '<=', $maxFilter);
        }

        if($request->gender){
            $jobseeker = $jobseeker->where('gender', $request->gender);
        }
        
        $data['min_age']    = @$request->min_age;
        $data['max_age']    = @$request->max_age;
        $data['min_salary'] = @$request->min_salary;
        $data['max_salary'] = @$request->max_salary;
        $data['gender']     = @$request->gender;
        $data['jobseeker']  = $jobseeker;
        //dd($data);
        return view('modules.jobseeker.jobseeker')->with($data);

    }

    /*
    *Method : fnToGetCalculatedDistamce
    *Description : for calculate distance in km or m
    *Author : Rajib
    */
    private function fnToGetCalculatedDistamce($distanceInKm) {
        $distanceInMeter = $distanceInKm * 1000;
        if ($distanceInMeter <= 1000) {
            $distanceInMeter = floor(100 + ($distanceInMeter - $distanceInMeter % 100));
            $distanceInMeter = 'Within ' . $distanceInMeter . 'm';
        } else {
            $distanceInMeter = floor((1000 + ($distanceInMeter - $distanceInMeter % 1000)) / 1000);
            $distanceInMeter = 'Within ' . $distanceInMeter . 'km';
        }
        return $distanceInMeter;
    }

    /*
    *Method : haversineGreatCircleDistance
    *Description : for nearby location by latitude and longitude
    *Author : Rajib
    */
    private function haversineGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371) {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);
        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;
        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
            cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return $angle * $earthRadius;
    }

    public function details($id = NULL) {
        $data['jobseeker'] = User::with('category', 'subCategory', 'country', 'state', 'city', 'appliedjob', 'appliedjob.job', 'appliedjob.job.cityName', 'appliedjob.job.JobType')->where('id', decrypt($id))->first();
        //dd($data);
        return view('modules.jobseeker.profile')->with($data);
    }

}
