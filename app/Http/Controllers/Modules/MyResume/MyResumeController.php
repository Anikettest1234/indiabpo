<?php

namespace App\Http\Controllers\Modules\MyResume;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Model\Resume;

class MyResumeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $resume = Resume::where('user_id', Auth::id());
        if(@$request->keyword) {
            $resume = $resume->where(function($q) use ($request) {
                $q = $q->where('name', 'like', '%'.$request->keyword.'%')
                    ->orWhere('file', 'like', '%'.$request->keyword.'%');
            });
        }
        $data['keyword'] = @$request->keyword;
        $data['resume'] = $resume->orderBy('id', 'desc')->get();
        return view('modules.my_resume.my_resume')->with($data);
    }

    /* For add company */
    public function create()
    {
        return view('modules.my_resume.add_resume');
    }

    /* For store company */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'name'      => 'required',
            'resume'    => 'required'
        ]);

        $new['name']    = @$request->name;
        $new['user_id'] = Auth::id();

        if(@$request->resume) {
            $filename = time().'_'.$request->resume->getClientOriginalName();
            $destinationPath = "storage/app/public/upload/resume/";
            $request->resume->move($destinationPath, $filename);
            
            $new['file'] = $filename;
        }

        $add = Resume::create($new);

        if(@$add) {
            \Session::flash('success', 'New resume has been saved successfully.');
            return redirect()->back();
        } else {
            \Session::flash('error', 'Somethings went\'s to wrong. Please try again.');
            return redirect()->back();
        }
    }

    /* For edit company */
    public function edit($id)
    {
        $data['resume'] = Resume::where(['user_id' => Auth::id()])->find($id);
        if(@$data['resume']) {
            return view('modules.my_resume.edit_resume')->with($data);
        } else {
            return redirect()->back();
        }
    }

    /* For update company */
    public function update(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'id'            => 'required',
            'name'          => 'required',
            'logo'          => 'nullable'
        ],[
            'id.required'   => 'Unautharize access.'
        ]);

        $company = Resume::where(['user_id' => Auth::id()])->find($request->id);

        if(@$company) {
            $new['name']    = @$request->name;
            $new['user_id'] = Auth::id();

            if(@$request->resume) {
                $filename = time().'_'.$request->resume->getClientOriginalName();
                $destinationPath = "storage/app/public/upload/resume/";
                $request->resume->move($destinationPath, $filename);
                
                $new['file'] = $filename;
            }

            Resume::whereId($request->id)->update($new);

        
            \Session::flash('success', 'Resume has been updated successfully.');
            return redirect()->back();
        } else {
            \Session::flash('error', 'Somethings went\'s to wrong. Please try again.');
            return redirect()->back();
        }
    }

    /* For delete company */
    public function delete($id)
    {
        // dd($id);
        $resume = Resume::where(['user_id' => Auth::id()])->find($id);
        if(@$resume) {
            Resume::destroy($id);
            session()->flash('success', 'Resume has been deleted successfully.');
            return redirect()->back();
        } else {
            session()->flash('error', 'Unauthorize access.');
            return redirect()->back();
        }
    }
}
