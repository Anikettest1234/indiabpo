<?php

namespace App\Http\Controllers\Modules\MyTraining;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\Model\Training;
use App\Model\Company;
use App\Model\Country;


class MyTrainingController extends Controller
{
    /**
    * Method : index
    * Description : my training page of this project
    * Author : Rajib
    * Date : 14-07-2020
    */
    public function index() {
    	$data['training'] = Training::with('company','city')
    								->where('user_id', Auth()->user()->id)
    								->where('status', '!=', 'D')
    								->orderBy('id', 'desc')
    								->paginate(5);
    	return view('modules.my_training.my_training')->with($data);
    }

  	public function add() {
  		$data['companies'] = Company::where('user_id', Auth()->user()->id)->get();
  		$data['countries'] = Country::get();
  		return view('modules.my_training.add_training')->with($data);
  	}

  	public function addtraining(Request $request)
  	{
  		$request->validate([
            'company_id'   => 'required',
            'name'         => 'required',
            'image'    	   => 'nullable|image|mimes:jpeg,jpg,png',
            'description'  => 'required',
            'price'        => 'required|numeric',
            'time'         => 'required',
            'phone'        => 'required|numeric',
            'email'        => 'required',
            'country_id'   => 'required',
            'state_id'     => 'required',
            'city_id'      => 'required',
            'address'      => 'required'
        ]);

        $new['user_id']         = Auth()->user()->id;
        $new['company_id']      = @$request->company_id;
        $new['name']            = @$request->name;
        $new['description']     = @$request->description;
        $new['price']      		= @$request->price;
        $new['time']      		= @$request->time;
        $new['phone']           = @$request->phone;
        $new['email']     		= @$request->email;
        $new['country_id']      = @$request->country_id;
        $new['state_id']        = @$request->state_id;
        $new['city_id']         = @$request->city_id;
        $new['address']         = @$request->address;

        if(@$request->image) {
            $filename = time().'_'.$request->image->getClientOriginalName();
            $destinationPath = "storage/app/public/upload/training/";
            $request->image->move($destinationPath, $filename);
            
            $new['image'] = $filename;
        }

        $add = Training::create($new);

        $slug = str_slug(@$request->name);
        $checkSlug = Training::where('slug', $slug)->first();
        
        if(@$checkSlug){
            $slug=$slug.'-'.$add->id;
        }
        Training::where('id', $add->id )->update(['slug' => $slug]);

        if(@$add) {
            \Session::flash('success', 'New Training has been created successfully.');
            return redirect()->back();
        } else {
            \Session::flash('error', 'Somethings went wrong. Please try again.');
            return redirect()->back();
        }
  	}

  	public function edittraining($slug=NULL) {
  		$data['training'] = Training::with('citys', 'states')->where(['slug' => $slug, 'user_id' => Auth()->user()->id])->first();
  		if($data['training']!='null') {
  			$data['companies'] = Company::get();
	        $data['countries'] = Country::get();
	        return view('modules.my_training.edit_training')->with($data);
  		}
  		else{
  			\Session::flash('error', 'Somethings went wrong. Please try again.');
            return redirect()->back();
  		}
  	}

  	public function updatetraining(Request $request) {
  		//dd($request->All());
  		$request->validate([
            'company_id'   => 'required',
            'name'         => 'required',
            'image'    	   => 'nullable|image|mimes:jpeg,jpg,png',
            'description'  => 'required',
            'price'        => 'required|numeric',
            'time'         => 'required',
            'phone'        => 'required|numeric',
            'email'        => 'required',
            'country_id'   => 'required',
            'state_id'     => 'required',
            'city_id'      => 'required',
            'address'      => 'required'
        ]);

        $new['company_id']      = @$request->company_id;
        $new['name']            = @$request->name;
        $new['description']     = @$request->description;
        $new['price']      		= @$request->price;
        $new['time']      		= @$request->time;
        $new['phone']           = @$request->phone;
        $new['email']     		= @$request->email;
        $new['country_id']      = @$request->country_id;
        $new['state_id']        = @$request->state_id;
        $new['city_id']         = @$request->city_id;
        $new['address']         = @$request->address;

        if(@$request->image) {
            $filename = time().'_'.$request->image->getClientOriginalName();
            $destinationPath = "storage/app/public/upload/training/";
            $request->image->move($destinationPath, $filename);
            
            $new['image'] = $filename;
        }

        $training = Training::where(['user_id' => Auth()->user()->id, 'id' => $request->job_id ])->update($new);

        if(@$training) {
            \Session::flash('success', 'Training has been updated successfully.');
            return redirect()->back();
        } else {
            \Session::flash('error', 'Somethings went wrong. Please try again.');
            return redirect()->back();
        }
  	}

  	public function changetraining($id)
  	{
  		// dd($id);
        $training = Training::find($id);
        if($training->status=='A') {
            $training = Training::where(['user_id' => Auth::id(), 'id' => $id ])->update(['status' => 'I']);
        }
        else {
            $training = Training::where(['user_id' => Auth::id(), 'id' => $id ])->update(['status' => 'A']);
        }
        session()->flash('success', 'Training status has been change successfully.');
        return redirect()->back();
  	}

  	public function deletetraining($id)
  	{
  		// dd($id);
        $training = Training::where(['user_id' => Auth::id(), 'id' => $id ])->update(['status' => 'D']);
        session()->flash('success', 'Training has been deleted successfully.');
        return redirect()->back();
  	}
}
