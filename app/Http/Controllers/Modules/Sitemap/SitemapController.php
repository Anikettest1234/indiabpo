<?php

namespace App\Http\Controllers\Modules\Sitemap;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Category;

class SitemapController extends Controller
{
    /**
    * Method : index
    * Description : Sitemap page of this project
    * Author : Rajib
    * Date : 01-07-2020
    */
    public function index(Request $request) {
    	$data['categories'] = Category::with('jobs','subCategory', 'subCategory.jobs')->where(['parent_id' => 0, 'status' => 'A'])->get();
    	//dd($data['categories']);
    	return view('modules.sitemap.sitemap')->with($data);
    }
}