<?php

namespace App\Http\Controllers\Modules\MyAd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\User;
use App\Model\Ad;
use App\Model\Package;

class MyAdController extends Controller
{
    public function index() {
    	$data['ads'] = Ad::where('user_id', Auth()->user()->id)->orderBy('id', 'desc')->get();
        $data['package'] = Package::where('id', Auth()->user()->subscription_id)->first();
    	return view('modules.my_ad.my_ads')->with($data);
    }

    public function add() {
    	return view('modules.my_ad.add_ads');
    }

    public function addad(Request $request) {
    	$request->validate([
            'title'        => 'required',
            'image'    	   => 'nullable|image|mimes:jpeg,jpg,png',
            'description'  => 'required',
            'url'          => 'required',
            'type'         => 'required'
        ]);

        $new['user_id']         = Auth()->user()->id;
        $new['type']      		= @$request->type;
        $new['title']           = @$request->title;
        $new['description']     = @$request->description;
        $new['url']             = @$request->url;
        $new['expiry']			= date('Y-m-d', strtotime('+'.@Auth()->user()->plan->ad_expiry_in.'day'));

        if(@$request->image) {
            $filename = time().'_'.$request->image->getClientOriginalName();
            $destinationPath = "storage/app/public/upload/ads/";
            $request->image->move($destinationPath, $filename);
            
            $new['image'] = $filename;
        }
        if(@Auth()->user()->ads != '0'){
            $add = Ad::create($new);
        }
        if(@$add) {
            $adslimit['ads'] = @Auth()->user()->ads - '1';
            User::where('id', Auth()->user()->id)->update($adslimit);
            \Session::flash('success', 'New Ads has been created successfully.');
            return redirect()->route('my.ads');
        } else {
            \Session::flash('error', 'Somethings went wrong. Please try again.');
            return redirect()->route('my.ads');
        }
    }

    public function edit($id) {
    	$data['ad'] = Ad::where(['id' => $id, 'user_id'=>Auth()->user()->id])->first();
    	return view ('modules.my_ad.edit_ads')->with($data);
    }

    public function update(Request $request) {
    	$request->validate([
    		'title'        => 'required',
            'image'    	   => 'nullable|image|mimes:jpeg,jpg,png',
            'description'  => 'required',
            'url'          => 'required',
            'type'         => 'required'
    	]);
        $new['title']           = @$request->title;
        $new['type']      		= @$request->type;
        $new['description']     = @$request->description;
        $new['url']             = @$request->url;
        if(@$request->image) {
            $filename = time().'_'.$request->image->getClientOriginalName();
            $destinationPath = "storage/app/public/upload/ads/";
            $request->image->move($destinationPath, $filename);
            $new['image'] = $filename;
        }
        $update = Ad::where(['user_id' => Auth::id(), 'id' => $request->id ])->update($new);
        if(@$update) {
            \Session::flash('success', 'Ads has been update successfully.');
            return redirect()->back();
        } else {
            \Session::flash('error', 'Somethings went wrong. Please try again.');
            return redirect()->back();
        }
    }

    public function change($id)
  	{
        $ad = Ad::find($id);
        if($ad->status=='A') {
            Ad::where(['user_id' => Auth::id(), 'id' => $id ])->update(['status' => 'I']);
        }
        else {
            Ad::where(['user_id' => Auth::id(), 'id' => $id ])->update(['status' => 'A']);
        }
        session()->flash('success', 'Ad status has been change successfully.');
        return redirect()->back();
  	}

  	public function delete($id)
  	{
  		$ad = Ad::where(['user_id'=>Auth()->user()->id,'id'=>$id])->first();
        if(@$ad) {
        	Ad::destroy($id);
        	session()->flash('success', 'Ad has been deleted successfully.');
        	return redirect()->back();
    	} else {
            session()->flash('error', 'Unauthorize access.');
            return redirect()->back();
        }
  	}
}
