<?php

namespace App\Http\Controllers\Modules\MyJob;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use Mail;
use App\User;
use App\Model\Job;
use App\Model\City;
use App\Model\State;
use App\Model\Company;
use App\Model\Setting;
use App\Model\JobType;
use App\Model\Category;
use App\Model\Country;
use App\Model\AppliedJob;
use App\Model\Payment;
use App\Mail\JobMail;

# payment
use App\Http\Controllers\Modules\Paypal\PaypalClientController;
use PayPalCheckoutSdk\Orders\OrdersCreateRequest;
use PayPalCheckoutSdk\Orders\OrdersAuthorizeRequest;
use PayPalCheckoutSdk\Orders\OrdersCaptureRequest;
use PayPalCheckoutSdk\Orders\OrdersGetRequest;


class MyJobController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $data['jobs']=Job::with('companyName', 'Category', 'subCategory', 'cityName', 'JobType')->orderBy('id', 'desc')->where('status', '!=', 'D')->where('user_id', Auth()->user()->id);
        if($request->keywords) {
            $data['jobs'] = $data['jobs']->where('title', 'LIKE', '%' . $request->keywords . '%');
        }
        $data['jobs'] = $data['jobs']->paginate(5);
        return view('modules.my_job.my_job')->with($data);
    }

    //
    /**
    * Method : postJob
    * Description : post a job page of this project
    * Author : Rajib
    * Date : 27-06-2020
    */
    public function postJob(Request $request) {
        $data['categories'] = Category::where(['parent_id' => 0, 'status' => 'A'])->get();
        $data['subcategory'] = Category::where('parent_id', '!=', 0)->where('status', 'A')->get();
        $data['companies'] = Company::where('user_id', Auth()->user()->id)->get();
        $data['job_types'] = JobType::get();
        $data['countries'] = Country::get();
        $data['setting'] = Setting::first();
        return view('modules.my_job.post_job')->with($data);
    }
    
    //
    /**
    * Method : postJob
    * Description : For  fetch Sub categories
    * Author : Rajib
    * Date : 27-06-2020
    */
    public function fetchSubcat(Request $request){  
        $response = [
            'jsonrpc'   => '2.0'
        ];      
        $value = $request->get('value');
        $response['result']['sub_category']  = Category::select([
            'id',
            'parent_id',
            'name',
            'status'
        ])
        ->where([
            'parent_id' =>  $value,
            'status'    =>  'A'
        ])
        ->get();
        return response()->json($response, 200);
    }

    /* For Add Job */
    public function addJob(Request $request)
    {
        $request->validate([
            'company_id'        => 'required',
            'category_id'       => 'required',
            'subcategory_id'    => 'required',
            'title'             => 'required',
            'job_image'         => 'nullable|image|mimes:jpeg,jpg,png',
            'job_type'          => 'required',
            'min_salary'        => 'required|numeric',
            'max_salary'        => 'required|numeric',
            'salary_type'       => 'required',
            'code'              => 'required|numeric',
            'phone'             => 'required|numeric',
            'country_id'        => 'required',
            'state_id'          => 'required',
            'city_id'           => 'required',
            'address'           => 'required',
            'application_url'   => 'nullable',
            'job_premium'       => 'required'
        ]);

        $new['user_id']         = Auth()->user()->id;
        $new['company_id']      = @$request->company_id;
        $new['description']     = @$request->description;
        $new['category_id']     = @$request->category_id;
        $new['subcategory_id']  = @$request->subcategory_id;
        $new['title']           = @$request->title;
        $new['job_type']        = @$request->job_type;
        $new['min_salary']      = @$request->min_salary;
        $new['max_salary']      = @$request->max_salary;
        $new['salary_type']     = @$request->salary_type;
        $new['negotiable']      = @$request->negotiable;
        $new['code']            = @$request->code;
        $new['phone']           = @$request->phone;
        $new['hide_phone']      = @$request->hide_phone;
        $new['country_id']      = @$request->country_id;
        $new['state_id']        = @$request->state_id;
        $new['city_id']         = @$request->city_id;
        $new['address']         = @$request->address;
        $new['lat']             = @$request->lat;
        $new['lng']             = @$request->lng;
        $new['application_url'] = @$request->application_url;
        $new['tags']            = @$request->tags;
        $new['job_premium']     = @$request->job_premium;

        if(@$request->job_image) {
            $filename = time().'_'.$request->job_image->getClientOriginalName();
            $destinationPath = "storage/app/public/upload/job/";
            $request->job_image->move($destinationPath, $filename);
            
            $new['image'] = $filename;
        }

        $add = Job::create($new);

        $slug = str_slug(@$request->title);
        $checkSlug = Job::where('slug', $slug)->first();
        
        if(@$checkSlug){
            $slug=$slug.'-'.$add->id;
        }
        Job::where('id', $add->id )->update(['slug' => $slug]);

        # payment option goes here
        if(@$request->job_premium == 'P') {
            $day = 0;
            $payableAmount = 0;
            $update = [];
            if(Auth::user()->subscription_started <= date('Y-m-d') && Auth::user()->subscription_ended >= date('Y-m-d')) {
                if(@$request->featured) {
                    $update['featured_valid_till'] = date('Y-m-d', strtotime('+'.Auth::user()->plan->featured_ad_days.'day'));
                    $update['is_featured'] = 'Y';
                    $update['featured_on'] = date('Y-m-d');
                    $payableAmount += Auth::user()->plan->featured_ad_fee;
                }
                if(@$request->urgent) {
                    $update['is_urgent'] = 'Y';
                    $update['urgent_on'] = date('Y-m-d');
                    $update['urgent_valid_till'] = date('Y-m-d', strtotime('+'.Auth::user()->plan->urgent_ad_days.'day'));
                    $payableAmount += Auth::user()->plan->urgent_ad_fee;
                }
                if(@$request->highlight) {
                    $update['is_highlight'] = 'Y';
                    $update['highlight_on'] = date('Y-m-d');
                    $update['highlight_valid_till'] = date('Y-m-d', strtotime('+'.Auth::user()->plan->highlight_ad_days.'day'));
                    $payableAmount += Auth::user()->plan->highlight_ad_fee;
                }
                
            } else {
                $setting = Setting::first();
                if(@$request->featured) {
                    $update['is_featured'] = 'Y';
                    $update['featured_on'] = date('Y-m-d');
                    $update['featured_valid_till'] = date('Y-m-d', strtotime('+'.$setting->featured_days.'day'));
                    $payableAmount += $setting->featured_amount;
                }
                if(@$request->urgent) {
                    $update['is_urgent'] = 'Y';
                    $update['urgent_on'] = date('Y-m-d');
                    $update['urgent_valid_till'] = date('Y-m-d', strtotime('+'.$setting->urgent_days.'day'));
                    $payableAmount += $setting->urgent_amount;
                }
                if(@$request->highlight) {
                    $update['is_highlight'] = 'Y';
                    $update['highlight_on'] = date('Y-m-d');
                    $update['highlight_valid_till'] = date('Y-m-d', strtotime('+'.$setting->highlight_days.'day'));
                    $payableAmount += $setting->highlight_amount;
                }
            }
           
            

            $update['status'] = 'P';
            Job::where(['id' => $add->id])->update($update);

            # init the payment
            $payment = $this->initPayment($add, $payableAmount);

            # redirect paypal site for accept payment
            foreach ($payment->result->links as $key => $value) {
                if($value->rel == 'approve') {
                    # redirect to paypal 
                    return redirect()->to($value->href);
                }
            }
        }

        if(@$add) {
            \Session::flash('success', 'New job has been created successfully.');
            return redirect()->back();
        } else {
            \Session::flash('error', 'Somethings went wrong. Please try again.');
            return redirect()->back();
        }
    }

    /**
    * method: makePayment
    * Description: This method is used to make payment
    * Author: Sanjoy
    */
    private function initPayment($job = [], $payableAmount) {
        # insert to payment table
        $payment = $this->makePayment($job, $payableAmount);

        # create order on paypal
        $request = new OrdersCreateRequest();
        $request->prefer('return=representation');
        $request->body = $this->buildRequestBody([
            'amount'            => $payableAmount,
            'custom_id'         => $payment->id.'@'.$job->id,
            'description'       => 'Payment for Job ID #'.$job->id,
            'soft_descriptor'   => 'Payer ID #'. Auth::user()->id,
            'reference_id'      => $payment->reference_id
        ]);
        
        // 3. Call PayPal to set up a transaction
        $client = PaypalClientController::client();
        $response = $client->execute($request);

        return $response;
    }

    /**
    * Method: makePayment
    * Description: This method is used to insert to payment table
    * Author: Sanjoy
    */
    private function makePayment($job = [], $payableAmount) {
        return $payment = Payment::create([
            'user_id'       => Auth::user()->id,
            'job_id'        => $job->id,
            'status'        => 'I',
            'amount'        => $payableAmount,
            'reference_id'  => strtoupper(str_random(6)).rand(1111,9999)
        ]);
    }

    /**
     * Setting up the JSON request body for creating the order with minimum request body. The intent in the
     * request body should be "AUTHORIZE" for authorize intent flow.
     *
     */
    private static function buildRequestBody($request) {
        return [
            'intent' => 'CAPTURE',
            'application_context' => [
                'return_url' => route('payment.return'),
                'cancel_url' => route('payment.cancel')
            ],
            'purchase_units' => [
                0 => [
                    'reference_id'      => @$request['reference_id'],
                    'description'       => @$request['description'],
                    'custom_id'         => @$request['custom_id'],
                    'soft_descriptor'   => @$request['soft_descriptor'],
                    'amount' => [
                        'currency_code' => 'USD',
                        'value'         => @$request['amount']
                    ]
                ]
            ]
        ];
    }
    
    /**
    * Method: makePayment
    * Description: This method is used to make payment
    * Author: Sanjoy
    */
    private function updatePayment($id, $reqData = []) {
        $payment = Payment::where(['id' => $id])->update($reqData);
    }

    /**
    * Method: paymentReturn
    * Description: This method is used to respond from peyment gateway
    */
    public function paymentReturn(Request $request) {
        try {
            $request = new OrdersCaptureRequest($request->token);

            // 3. Call PayPal to capture an authorization
            $client = PaypalClientController::client();
            $response = $client->execute($request);
            // dd($response);
            // 4. Save the capture ID to your database. Implement logic to save capture to your database for future reference.
            if (@$response->result->status == 'COMPLETED') {
                # update to payment table 
                $update['status'] = 'S';
                $update['response'] = json_encode($response);
                $customId = explode('@', @$response->result->purchase_units[0]->payments->captures[0]->custom_id);
                $paymentId = $customId[0];
                $jobId = $customId[1];
                Payment::where(['id' => $paymentId])->update($update);

                # update to job table
                Job::where(['id' => $jobId])->update(['status' => 'A']);
                return redirect()->route('payment.success',['trackId' => @$paymentId]);
            }
        } catch(\Exception $e) {
            dd($e->getMessage());
        }
    }


    public function paymentSuccess(Request $request ){
        //dd($request->all());
        $data['payment'] = Payment::with('job')->where('id', $request->trackId)->first();
        if($data){
            $mailArr = [
                'name' => $data['payment']->user->name,
                'email' => $data['payment']->user->email,
                'title' => $data['payment']->job->title,
                'reference_id' => $data['payment']->reference_id,
                'amount'  => $data['payment']->amount
            ];

        Mail::send(new JobMail($mailArr));
        }
        return view('modules.payment.success')->with($data);
    }

    /**
    */
    public function paymentCancel(Request $request) {
        $orderId = $request->token;
        // 3. Call PayPal to get the transaction details
        $client = PaypalClientController::client();
        $response = $client->execute(new OrdersGetRequest($orderId));
        if(@$response->result->status == 'CREATED') {
            //$update['status'] = 'C'
            //$payment = Payment::where(['id' => $payment->id])->update($update);
        }
        return view('modules.payment.error');
    }

    


    /* For update job */
    public function edit($id)
    {
        $data['job'] = job::with('citys', 'states', 'countryName')->find($id);
        $data['categories'] = Category::where(['parent_id' => 0, 'status' => 'A'])->get();
        $data['subcategory'] = Category::where('parent_id', $data['job']->category_id)->where('status', 'A')->get();
        $data['companies'] = Company::get();
        $data['job_types'] = JobType::get();
        $data['countries'] = Country::get();
        return view('modules.my_job.edit_job')->with($data);
    }

    public function update(Request $request)
    {
        $request->validate([
            'company_id'        => 'required',
            'category_id'       => 'required',
            'subcategory_id'    => 'required',
            'title'             => 'required',
            'job_image'         => 'nullable|image|mimes:jpeg,jpg,png',
            'job_type'          => 'required',
            'min_salary'        => 'required|numeric',
            'max_salary'        => 'required|numeric',
            'salary_type'       => 'required',
            'code'             => 'required|numeric',
            'phone'             => 'required|numeric',
            'country_id'        => 'required',
            'state_id'          => 'required',
            'city_id'           => 'required',
            'address'           => 'required',
            'application_url'   => 'nullable',
            'job_premium'       => 'required'
        ]);

        $new['user_id']         = Auth()->user()->id;
        $new['company_id']      = @$request->company_id;
        $new['description']     = @$request->description;
        $new['category_id']     = @$request->category_id;
        $new['subcategory_id']  = @$request->subcategory_id;
        $new['title']           = @$request->title;
        $new['job_type']        = @$request->job_type;
        $new['min_salary']      = @$request->min_salary;
        $new['max_salary']      = @$request->max_salary;
        $new['salary_type']     = @$request->salary_type;
        $new['negotiable']      = @$request->negotiable;
        $new['code']            = @$request->code;
        $new['phone']           = @$request->phone;
        $new['hide_phone']      = @$request->hide_phone;
        $new['country_id']      = @$request->country_id;
        $new['state_id']        = @$request->state_id;
        $new['city_id']         = @$request->city_id;
        $new['address']         = @$request->address;
        $new['lat']             = @$request->lat;
        $new['lng']             = @$request->lng;
        $new['application_url'] = @$request->application_url;
        $new['tags']            = @$request->tags;
        $new['job_premium']     = @$request->job_premium;

        if(@$request->job_image) {
            $filename = time().'_'.$request->job_image->getClientOriginalName();
            $destinationPath = "storage/app/public/upload/job/";
            $request->job_image->move($destinationPath, $filename);
            
            $new['image'] = $filename;
        }

        $Job = Job::where(['user_id' => Auth::id(), 'id' => $request->job_id ])->update($new);

        if(@$Job) {
            \Session::flash('success', 'Job has been updated successfully.');
            return redirect()->back();
        } else {
            \Session::flash('error', 'Somethings went wrong. Please try again.');
            return redirect()->back();
        }
    }    

    /* For edit company */
    public function change($id)
    {
        // dd($id);
        $job = Job::find($id);
        if($job->status=='A') {
            $job = Job::where(['user_id' => Auth::id(), 'id' => $id ])->update(['status' => 'I']);
        }
        elseif($job->status=='I') {
            $job = Job::where(['user_id' => Auth::id(), 'id' => $id ])->update(['status' => 'A']);
        } else {
            \Session::flash('error', 'Somethings went wrong. Please try again.');
            return redirect()->back();
        }
        session()->flash('success', 'Job status has been change successfully.');
        return redirect()->back();
    }

    /* For delete company */
    public function delete($id)
    {
        // dd($id);
        $job = Job::where(['user_id' => Auth::id(), 'id' => $id ])->update(['status' => 'D']);
        session()->flash('success', 'Job has been deleted successfully.');
        return redirect()->back();
    }

    public function appliedjobseeker($id = NULL){
        $data['jobseeker']=AppliedJob::with('user', 'user.category', 'user.subCategory', 'resume')->orderBy('id', 'desc')->where('job_id', $id)->paginate(5);
        //dd($data);
        return view('modules.my_job.applied_user')->with($data);
    }

    public function jobseekerdetails($id = NULL) {
        $data['jobseeker'] = User::with('category', 'subCategory', 'country', 'state', 'city', 'appliedjob', 'appliedjob.job', 'appliedjob.job.cityName', 'appliedjob.job.JobType')->where('id', decrypt($id))->first();
        //dd($data);
        return view('modules.my_job.profile')->with($data);
    }
}
