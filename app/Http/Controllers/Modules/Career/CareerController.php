<?php

namespace App\Http\Controllers\Modules\Career;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Career;

class CareerController extends Controller
{
    public function index()
    {
    	$data['career'] = Career::with('category')->where('status', 'A')->get();
    	return view('modules.career.career')->with($data);
    }
}
