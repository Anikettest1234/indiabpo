<?php

namespace App\Http\Controllers\Modules\FavouriteUser;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\FavouriteUser;

class FavouriteUserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['favouriteuser'] = FavouriteUser::with('jobseeker')->where('user_id', Auth()->user()->id)->paginate(10);
        return view('modules.favourite_user.favourite_user')->with($data);
    }

    public function add($id = NULL) {
        $favouriteuser = FavouriteUser::where(['user_id'=>Auth()->user()->id, 'jobseeker_id'=>decrypt($id)])->first();
        if($favouriteuser){
            \Session::flash('error', 'User already on your favourite list.');
            return redirect()->back();
        } else {
            $new['user_id']         = Auth()->user()->id;
            $new['jobseeker_id']    = decrypt($id);
            $add = FavouriteUser::create($new);
            if(@$add) {
                \Session::flash('success', 'User add on your favourite list.');
                return redirect()->back();
            } else {
                \Session::flash('error', 'Somethings went wrong. Please try again.');
                return redirect()->back();
            }
        }
    }

    public function delete($id) {
        $favouriteuser = FavouriteUser::where(['user_id'=>Auth()->user()->id, 'id'=>decrypt($id)])->first();
        if($favouriteuser){
            FavouriteUser::destroy(decrypt($id));
            \Session::flash('success', 'Jobseeker has been removed successfully.');
            return redirect()->back();
        } else {
            \Session::flash('error', 'User mot find on your favourite list. Please try again.');
            return redirect()->back();
        }
    }
}
