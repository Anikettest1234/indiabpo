<?php

namespace App\Http\Controllers\Modules\MyCompany;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\Model\Job;
use App\Model\City;
use App\Model\State;
use App\Model\Country;
use App\Model\Company;

class MyCompanyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $company = Company::with('jobs', 'cityName', 'stateName', 'countryName')->orderBy('id', 'desc')->where('user_id', Auth::id())->where('status', '!=', 'D');
        if(@$request->keyword) {
            $company = $company->where(function($q) use ($request) {
                $q = $q->where('company_name', 'like', '%'.$request->keyword.'%')
                    ->orWhere('state', 'like', '%'.$request->keyword.'%')
                    ->orWhere('city', 'like', '%'.$request->keyword.'%')
                    ->orWhere('country', 'like', '%'.$request->keyword.'%')
                    ->orWhere('description', 'like', '%'.$request->keyword.'%');
            });
        }
        $data['keyword'] = @$request->keyword;
        $data['company'] = $company->orderBy('id', 'desc')->get();
        //dd($data);
        return view('modules.my_company.my_company')->with($data);
    }

    public function fetchState(Request $request){  
        $response = [
            'jsonrpc'   => '2.0'
        ];      
        $value = $request->get('value');
        $response['result']['state']  = State::select([
            'id',
            'country_id',
            'name',
            'status'
        ])
        ->where([
            'country_id' =>  $value,
            'status'    =>  'A'
        ])
        ->get();
        return response()->json($response, 200);
    }

    public function fetchCity(Request $request){  
        $response = [
            'jsonrpc'   => '2.0'
        ];      
        $value = $request->get('value');
        $response['result']['city']  = City::select([
            'id',
            'state_id',
            'name',
            'status'
        ])
        ->where([
            'state_id' =>  $value,
            'status'    =>  'A'
        ])
        ->get();
        return response()->json($response, 200);
    }

    /* For add company */
    public function create()
    {
        $data['countries'] = Country::get();
        //dd($data);
        return view('modules.my_company.add_company')->with($data);
    }

    /* For store company */
    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'name'          => 'required',
            'description'   => 'required',
            'city'          => 'required',
            'state'         => 'required',
            'email'         => 'required',
            'country'       => 'required',
            'address'       => 'required',
            'phone'         => 'required',
            'fax'           => 'nullable',
            'website'       => 'nullable',
            'linkedin'      => 'nullable',
            'logo'          => 'nullable|image|mimes:jpeg,jpg,png'
        ]);

        $new['company_name']    = @$request->name;
        $new['user_id']         = Auth::id();
        $new['description']     = @$request->description ? nl2br($request->description) : '';
        $new['country']         = @$request->country;
        $new['city']            = @$request->city;
        $new['state']           = @$request->state;
        $new['address']         = @$request->address;
        $new['phone_number']    = @$request->phone;
        $new['email']           = @$request->email;
        $new['fax']             = @$request->fax;
        $new['website']         = @$request->website;
        $new['linkedin']        = @$request->linkedin;
        $new['status']          = 'A';


        if(@$request->logo) {
            $filename = time().'_'.$request->logo->getClientOriginalName();
            $destinationPath = "storage/app/public/upload/company/";
            $request->logo->move($destinationPath, $filename);
            
            $new['company_logo'] = $filename;
        }

        $add = Company::create($new);

        $slug = str_slug(@$request->name);
        $checkSlug = Company::where('slug', $slug)->first();
        
        if(@$checkSlug){
            $slug=$slug.'-'.$add->id;
        }
        Company::where('id', $add->id )->update(['slug' => $slug]);

        if(@$add) {
            \Session::flash('success', 'New company has been created successfully.');
            return redirect()->back();
        } else {
            \Session::flash('error', 'Somethings went\'s to wrong. Please try again.');
            return redirect()->back();
        }
    }

    /* For edit company */
    public function edit($id)
    {
        $data['company'] = Company::with('citys', 'states', 'countryName')->where(['user_id' => Auth::id()])->find($id);
        //$data['company'] = Company::where(['user_id' => Auth::id()])->find($id);
        $data['countries'] = Country::get();
        //$data['states'] = State::where('country_id',$data['company']->country)->get();
        //$data['citys'] = State::where('state_id',$data['company']->state)->get();
        //dd($data);
        if(@$data['company']) {
            return view('modules.my_company.edit_company')->with($data);
        } else {
            return redirect()->back();
        }
    }

    /* For update company */
    public function update(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'id'            => 'required',
            'name'          => 'required',
            'description'   => 'required',
            'city'          => 'required',
            'state'         => 'required',
            'email'         => 'required',
            'country'       => 'required',
            'address'       => 'required',
            'phone'         => 'required',
            'fax'           => 'nullable',
            'website'       => 'nullable',
            'linkedin'      => 'nullable',
            'logo'          => 'nullable|image|mimes:jpeg,jpg,png'
        ],[
            'id.required'   => 'Unautharize access.'
        ]);

        $company = Company::where(['user_id' => Auth::id()])->find($request->id);

        if(@$company) {
            $new['company_name']    = @$request->name;
            $new['user_id']         = Auth::id();
            $new['description']     = @$request->description ? nl2br($request->description) : '';
            $new['country']         = @$request->country;
            $new['city']            = @$request->city;
            $new['state']           = @$request->state;
            $new['address']         = @$request->address;
            $new['phone_number']    = @$request->phone;
            $new['email']           = @$request->email;
            $new['fax']             = @$request->fax;
            $new['website']         = @$request->website;
            $new['linkedin']        = @$request->linkedin;

            if(@$request->logo) {
                $filename = time().'_'.$request->logo->getClientOriginalName();
                $destinationPath = "storage/app/public/upload/company/";
                $request->logo->move($destinationPath, $filename);
                
                $new['company_logo'] = $filename;
            }

            Company::whereId($request->id)->update($new);

        
            \Session::flash('success', 'Company has been updated successfully.');
            return redirect()->back();
        } else {
            \Session::flash('error', 'Somethings went\'s to wrong. Please try again.');
            return redirect()->back();
        }
    }

    /* For delete company */
    public function delete($id)
    {
        $company = Company::where(['user_id' => Auth::id()])->find($id);
        if(@$company) {
            //Company::destroy($id);
            Company::where('id', $id)->update(['status' => 'D']);
            session()->flash('success', 'Company has been deleted successfully.');
            return redirect()->back();
        } else {
            session()->flash('error', 'Unauthorize access.');
            return redirect()->back();
        }
    }

    /* For view company details */
    public function view($slug = NULL)
    {
        $data['company'] = Company::with('jobs' ,'jobs.cityName', 'jobs.JobType')->where(['user_id' => Auth::id(), 'slug' => $slug])->first();
        if(@$data['company']) {
            return view('modules.my_company.view_company')->with($data);
        } else {
            session()->flash('error', 'Unauthorize access.');
            return redirect()->back();
        }
    }
}
