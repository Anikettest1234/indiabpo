<?php

namespace App\Http\Controllers\Modules\About;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AboutController extends Controller
{
    /**
    * Method : index
    * Description : About us page of this project
    * Author : Rakesh
    * Date : 24-06-2020
    */
    public function index(Request $request) {
    	return view('modules.about_us.about_us');
    }
}
