<?php

namespace App\Http\Controllers\Modules\Training;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Training;
use Mail;
use App\Mail\TrainingMail;

class TrainingController extends Controller
{
    /**
    * Method : index
    * Description : Traning page of this project
    * Author : Rajib
    * Date : 14-07-2020
    */
    public function index(Request $request) {
    	$training = Training::with('company', 'country', 'state', 'city')->where('status', '!=', 'D');
    	
    	if(@$request->location){
            $training = $training->where('address', 'like', '%'.$request->location.'%');
        }
        if(@$request->keyword){
            $training = $training->where('name', 'like', '%'.$request->keyword.'%');
        }
        if($request->min_price){
            $minFilter = (int) $request->min_price;
            $training = $training->where('price', '>=', $minFilter);
        }

        if($request->max_price){
            $maxFilter = (int) $request->max_price;
            $training = $training->where('price', '<=', $maxFilter);
        }

        if($request->time){
        	$training = $training->whereIn('time', $request->time);
        }
    	$data['training'] = $training->get();
    	return view('modules.training.training')->with($data);
    }

    public function details(Request $request ,$slug = NULL) {
    	$data['training'] = Training::with('employer', 'company', 'country', 'state', 'city')->where('slug', $slug)->first();
    	//dd($request->all());
        //dd($data['training']->name);
        if($request->all()) {
            $mailArr = [
                'name' => $request->name,
                'email' => $request->email,
                'employer_email' => $request->employer_email,
                'phone'=> $request->phone,
                'message' => $request->message
            ];

            Mail::send(new TrainingMail($mailArr));
            \Session::flash('success', 'Your mail is send successfully.');
            return redirect()->back();
        }
    	return view('modules.training.details')->with($data);
    }
}
