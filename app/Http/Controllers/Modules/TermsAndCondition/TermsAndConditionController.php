<?php

namespace App\Http\Controllers\Modules\TermsAndCondition;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TermsAndConditionController extends Controller
{
    /**
    * Method : index
    * Description : Terms and condition page of this project
    * Author : Rakesh
    * Date : 24-06-2020
    */
    public function index(Request $request) {
    	return view('modules.terms_and_condition.terms_and_condition');
    }

    /**
    * Method : index
    * Description : Terms and condition page of this project
    * Author : Rajib
    * Date : 24-06-2020
    */
    public function privacyPolicy(Request $request) {
    	return view('modules.terms_and_condition.privacy_policy');
    }
}
