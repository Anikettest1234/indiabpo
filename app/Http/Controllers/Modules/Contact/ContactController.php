<?php

namespace App\Http\Controllers\Modules\Contact;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;
use App\Mail\ContactUsMail;
class ContactController extends Controller
{
    /**
    * Method : index
    * Description : Contact us page of this project
    * Author : Rakesh
    * Date : 24-06-2020
    */
    public function index(Request $request) {
    	if($request->all()) {
    		$mailArr = [
    			'name' => $request->name,
    			'email' => $request->email,
    			'subject'=> $request->subject,
    			'message' => $request->message
    		];

    		Mail::send(new ContactUsMail($mailArr));
    		\Session::flash('success', 'Your mail is send successfully.');
            return redirect()->back();
    	}
    	return view('modules.contact.contact');
    }
}
