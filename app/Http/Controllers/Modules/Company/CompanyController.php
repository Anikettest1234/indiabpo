<?php

namespace App\Http\Controllers\Modules\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Job;
use App\Model\City;
use App\Model\Company;
use App\Model\JobType;

class CompanyController extends Controller
{
    /**
    * Method : index
    * Description : Company page of this project
    * Author : Rakesh
    * Date : 14-06-2020
    */
    public function index(Request $request) {
    	$company = Company::with('jobs')->orderBy('id', 'desc')->where('id', '!=', 0);
        if(@$request->keyword) {
            $company = $company->where(function($q) use ($request) {
                $q = $q->where('company_name', 'like', '%'.$request->keyword.'%')
                    /*->orWhere('state', 'like', '%'.$request->keyword.'%')
                    ->orWhere('city', 'like', '%'.$request->keyword.'%')
                    ->orWhere('country', 'like', '%'.$request->keyword.'%')
                    ->orWhere('description', 'like', '%'.$request->keyword.'%')*/;
            });
        }
        $data['keyword'] = @$request->keyword;
        $data['company'] = $company->orderBy('id', 'desc')->paginate(9);
        //dd($data);
    	return view('modules.company.company')->with($data);
    }

    /**
    * Method: details
    * Description: This method is used to show details of company
    * Author: Rajib 
    */
    public function details($slug = NULL) {
        $data['company'] = Company::with('jobs' ,'jobs.cityName', 'jobs.JobType')->where('slug', $slug)->first();
        return view('modules.company.details')->with($data);
    }


}
