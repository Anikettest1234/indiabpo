<?php

namespace App\Http\Controllers\Modules\Feedback;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FeedbackController extends Controller
{
    /**
    * Method : index
    * Description : feedback page of this project
    * Author : Rakesh
    * Date : 25-06-2020
    */
    public function index(Request $request) {
    	return view('modules.feedback.feedback');
    }
}
