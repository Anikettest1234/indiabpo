<?php

namespace App\Http\Controllers\Modules\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Ad;
use App\Model\Job;
use App\Model\Category;

class HomeController extends Controller
{
    /**
    * Method : index
    * Description : Index page of this project
    * Author : Rajib
    * Date : 30-06-2020
    */
    public function index(Request $request) {

    	/*$data['categories'] = Category::where('parent_id', 0)->where(['status' => 'A'])->get();

        $data['featured_jobs'] = Job::with('companyName', 'cityName', 'JobType')
        ->where('status', 'A')
        ->where(['is_featured' => 'Y'])
        ->where('featured_valid_till', '>=', date('Y-m-d'))
        ->inRandomOrder()
        ->take(2)
        ->get();
        
        $data['jobs'] = Job::with('companyName', 'cityName', 'JobType')
        ->where('status', 'A')
        ->orderBy('id', 'desc')
        ->take(6)
        ->get();

        $data['ads'] = Ad::with('employer')
        ->where('status', 'A')
        ->where('expiry', '>=', date('Y-m-d'))
        ->inRandomOrder()
        ->take(3)
        ->get();*/
    	//dd($data);
    	return view('modules.home.home');
    }

}