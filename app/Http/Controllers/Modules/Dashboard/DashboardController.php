<?php

namespace App\Http\Controllers\Modules\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Model\Job;
use App\Model\City;
use App\Model\State;
use App\Model\Country;
use App\Model\Category;
use App\Model\Package;
use Hash;


class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['user'] = Auth::user();
        if(@Auth::user()->user_type == 'J') {
            $data['country'] =Country::get();
            $data['state'] = State::where('country_id', $data['user']->country_id)->get();
            $data['city'] = City::where('state_id', $data['user']->state_id)->get();
            $data['category'] = Category::where(['parent_id' => 0])->orderBy('name')->get();
            $data['subcategory'] = Category::where('parent_id', $data['user']->category_id)->where('status', '!=', 'D')->orderBy('name')->get();
            //dd($data);
            return view('modules.dashboard.dashboard')->with($data);
        } elseif(@Auth::user()->user_type == 'E') {
            $data['country'] =Country::get();
            $data['state'] = State::where('country_id', $data['user']->country_id)->get();
            $data['city'] = City::where('state_id', $data['user']->state_id)->get();
            $data['package'] = Package::where('id', Auth::user()->subscription_id )->first();
            $data['jobs'] = Job::where('status', '!=', 'D')->where('user_id', Auth::user()->id)->get();
            //dd($data);
            return view('modules.dashboard.dashboard1')->with($data);
        }
    }

    // for update profile
    public function updateProfile(Request $request)
    {
        $id = Auth::id();
        if(@Auth::user()->user_type == 'J') {
            // dd($request->all());
            $request->validate([
                'name'      => 'required',
                'username'  => 'required|unique:users,username,'.$id,
                'email'     => 'required|email|unique:users,email,'.$id,
                'phone'     => 'required|unique:users,mobile,'.$id,
                'gender'    => 'required',
                'city'      => 'required',
                'state'     => 'required',
                'country'   => 'required',
                'address'   => 'required',
                'about_me'  => 'required',
                'website'   => 'nullable',
                'linked_in' => 'nullable',
                'marketing' => 'nullable',
                'category' => 'nullable',
                'sub_category' => 'nullable',
                'salary_min' => 'nullable',
                'salary_max' => 'nullable',
                'dob' => 'nullable',
                'image'     => 'nullable|image|mimes:jpeg,jpg,png'
            ]);

            $update['name']         = @$request->name;
            $update['username']     = @$request->username;
            $update['mobile']       = @$request->phone;
            $update['gender']       = @$request->gender;
            $update['city_id']         = @$request->city;
            $update['state_id']        = @$request->state;
            $update['country_id']      = @$request->country;
            $update['address']      = @$request->address;
            $update['lng']          = @$request->lng;
            $update['lat']          = @$request->lat;
            $update['about_me']     = @$request->about_me;
            $update['website']      = @$request->website;
            $update['linked_in']    = @$request->linked_in;
            $update['dob']    = @$request->dob ? date('Y-m-d', strtotime($request->dob)) : '';
            $update['marketing']    = @$request->marketing;
            $update['category_id']    = @$request->category;
            $update['sub_category_id']    = @$request->sub_category;
            $update['min_salary']    = @$request->salary_min;
            $update['max_salary']    = @$request->salary_max;
            $update['current_salary']    = @$request->current_salary;

            if(@$request->image) {
                $filename = time().'_'.$request->image->getClientOriginalName();
                $destinationPath = "storage/app/public/upload/profile/";
                $request->image->move($destinationPath, $filename);
                
                $update['image'] = $filename;
            }

            User::whereId($id)->update($update);

            \Session::flash('success', 'Profile has been updated successfully.');
            return redirect()->back();
        } elseif(@Auth::user()->user_type == 'E') {
            $request->validate([
                'name'      => 'required',
                'username'  => 'required|unique:users,username,'.$id,
                'email'     => 'required|email|unique:users,email,'.$id,
                'phone'     => 'required|unique:users,mobile,'.$id,
                'gender'    => 'required',
                'city'      => 'required',
                'state'     => 'required',
                'country'   => 'required',
                'address'   => 'required',
                'about_me'  => 'required',
                'website'   => 'nullable',
                'linked_in' => 'nullable',
                'image'     => 'nullable|image|mimes:jpeg,jpg,png'
            ]);

            $update['name']         = @$request->name;
            $update['username']     = @$request->username;
            $update['mobile']       = @$request->phone;
            $update['gender']       = @$request->gender;
            $update['city_id']         = @$request->city;
            $update['state_id']        = @$request->state;
            $update['country_id']      = @$request->country;
            $update['address']      = @$request->address;
            $update['about_me']     = @$request->about_me;
            $update['website']      = @$request->website;
            $update['linked_in']    = @$request->linked_in;

            if(@$request->image) {
                $filename = time().'_'.$request->image->getClientOriginalName();
                $destinationPath = "storage/app/public/upload/profile/";
                $request->image->move($destinationPath, $filename);
                
                $update['image'] = $filename;
            }

            User::whereId($id)->update($update);

            \Session::flash('success', 'Profile has been updated successfully.');
            return redirect()->back();
        }
    }

    // for update password
    public function updatePassword(Request $request)
    {
        // dd($request->all());
        $id = Auth::id();
        $request->validate([
            'password'                  => 'required|min:6|max:15|confirmed',
            'password_confirmation'     => 'required'
        ]);

        $update['password'] = \Hash::make($request->password);
        User::whereId($id)->update($update);

        \Session::flash('success', 'Password has been updated successfully.');
        return redirect()->back();
    }

    /*
    *Method : getSubCategory
    *Description : for get sub category
    *Author : Rakesh
    */
    public function getSubCategory(Request $request)
    {
        $response = array(
            'jsonrpc'=> '2.0'
        );

        $parent_id = $request->data['parent_id'];
        $response['id'] = $request->data['parent_id'];
        $response['result']['subCategory'] = Category::select('id', 'name')
                                            ->where([
                                                'parent_id' => $parent_id,
                                                'status' => 'A'
                                            ])
                                            ->orderBy('name', 'ASC')
                                            ->get();
        
        return response()->json($response);
    }
}
