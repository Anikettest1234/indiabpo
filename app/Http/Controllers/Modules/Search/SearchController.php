<?php

namespace App\Http\Controllers\Modules\Search;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Ad;
use App\Model\Job;
use App\Model\City;
use App\Model\Company;
use App\Model\JobType;
use App\Model\Category;

class SearchController extends Controller
{
    /**
    * Method : index
    * Description : Search page of this project
    * Author : Rajib
    * Date : 29-06-2020
    */
    public function index(Request $request, $slug = NULL) {
    	$data['jobType'] = JobType::get();
    	$data['categories'] = Category::with('subCategory')->where(['parent_id' => 0, 'status' => 'A'])->get();
        $data['ads'] = Ad::with('employer')->where('status', 'A')
                                           ->where('expiry', '>=', date('Y-m-d'))
                                           ->inRandomOrder()
                                           ->first();
        $data['highlight_jobs'] = Job::with('companyName', 'Category', 'subCategory', 'cityName', 'JobType')
                    ->where('status', 'A')
                    ->inRandomOrder()
                    ->first();

    	$jobs = Job::with('companyName', 'Category', 'subCategory', 'cityName', 'JobType')
                    ->orderBy('id', 'desc')
                    ->where('status', 'A');
                    
        if ($slug) {
            $category = Category::where('slug', $slug)->first();
            if($category->parent_id==0) {
                $jobs = $jobs->where('category_id', $category->id);
            } else {
                $jobs = $jobs->where('subcategory_id', $category->id);
            }
        }

        if(@$request->keyword){
            $jobs = $jobs->where(function($q) use ($request) {
                $q = $q->where('title', 'like', '%'.$request->keyword.'%')
                    /*->where('address', 'like', '%'.$request->location.'%')
                    /*->orWhere('state', 'like', '%'.$request->keyword.'%')
                    ->orWhere('city', 'like', '%'.$request->keyword.'%')
                    ->orWhere('country', 'like', '%'.$request->keyword.'%')
                    ->orWhere('description', 'like', '%'.$request->keyword.'%')*/;
            });
        }

        if($request->job_type){
            $jobs = $jobs->whereIn('job_type', $request->job_type);
        }

        if($request->salary_type){
            $jobs = $jobs->whereIn('salary_type', $request->salary_type);
        }
        
        if($request->min_salary){
            $minFilter = (int) $request->min_salary;
            $jobs = $jobs->where('min_salary', '>=', $minFilter);
        }

        if($request->max_salary){
            $maxFilter = (int) $request->max_salary;
            $jobs = $jobs->where('max_salary', '<=', $maxFilter);
        }

        $jobs = $jobs->orderBy('id', 'desc')->get();
        if(@$request->lat && @$request->lng) {
            $radious    = @$value->radius ? $value->radius : 1000;             
            $lat        = $request->lat;
            $lng        = $request->lng;
            $jobs    = $jobs->filter(function ($val) use ($lat, $lng, $radious) {
                $latitudeTo         = @$val->lat;
                $longitudeTo        = @$val->lng;
                $nearByDistance     = $this->haversineGreatCircleDistance($lat, $lng, $latitudeTo, $longitudeTo);
                $val->distanceInKm  = $nearByDistance;
                $val->distance      = $this->fnToGetCalculatedDistamce($nearByDistance);
                return $nearByDistance <= $radious;
            })->sortBy('distanceInKm');
        }

        $data['job_type']    = @$request->job_type;
        $data['salary_type'] = @$request->salary_type;
        $data['min_salary']  = @$request->min_salary;
        $data['max_salary']  = @$request->max_salary;
        $data['jobs']        = $jobs;
        return view('modules.search.search')->with($data);

    }


    /*
    *Method : fnToGetCalculatedDistamce
    *Description : for calculate distance in km or m
    *Author : Rajib
    */
    private function fnToGetCalculatedDistamce($distanceInKm) {
        $distanceInMeter = $distanceInKm * 1000;
        if ($distanceInMeter <= 1000) {
            $distanceInMeter = floor(100 + ($distanceInMeter - $distanceInMeter % 100));
            $distanceInMeter = 'Within ' . $distanceInMeter . 'm';
        } else {
            $distanceInMeter = floor((1000 + ($distanceInMeter - $distanceInMeter % 1000)) / 1000);
            $distanceInMeter = 'Within ' . $distanceInMeter . 'km';
        }
        return $distanceInMeter;
    }

    /*
    *Method : haversineGreatCircleDistance
    *Description : for nearby location by latitude and longitude
    *Author : Rajib
    */
    private function haversineGreatCircleDistance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371) {
        // convert from degrees to radians
        $latFrom = deg2rad($latitudeFrom);
        $lonFrom = deg2rad($longitudeFrom);
        $latTo = deg2rad($latitudeTo);
        $lonTo = deg2rad($longitudeTo);
        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;
        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
            cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));
        return $angle * $earthRadius;
    }
}