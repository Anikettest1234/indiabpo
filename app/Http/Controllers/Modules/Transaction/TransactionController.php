<?php

namespace App\Http\Controllers\Modules\Transaction;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Payment;

class TransactionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index() {
        $data['payment'] = Payment::with('job', 'subscription.package')
                                    ->where('user_id', Auth()->user()->id)
                                    ->orderBy('id', 'desc')
                                    ->get();
        //dd($data);
        return view('modules.transaction.transaction')->with($data);
    }
}
