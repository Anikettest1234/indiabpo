<?php

namespace App\Http\Controllers\Modules\Paypal;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use PayPalCheckoutSdk\Core\PayPalHttpClient;
use PayPalCheckoutSdk\Core\SandboxEnvironment;
use PayPalCheckoutSdk\Core\ProductionEnvironment;

class PaypalClientController extends Controller
{
    /**
     * Returns PayPal HTTP client instance with environment that has access
     * credentials context. Use this instance to invoke PayPal APIs, provided the
     * credentials have access.
     */
    public static function client()
    {
        return new PayPalHttpClient(self::environment());
    }

    /**
     * Set up and return PayPal PHP SDK environment with PayPal access credentials.
     * This sample uses SandboxEnvironment. In production, use ProductionEnvironment.
     */
    public static function environment()
    {
    	if(env('PAYPAL_ENV') == 'sandbox') {
    		$clientId = env("PAYPAL_SANDBOX_CLIENT_ID");
	        $clientSecret = env("PAYPAL_SANDBOX_SECRET");
	        return new SandboxEnvironment($clientId, $clientSecret);
    	} else {
    		$clientId = env("PAYPAL_CLIENT_ID");
	        $clientSecret = env("PAYPAL_SECRET");
	        return new ProductionEnvironment($clientId, $clientSecret);
    	}
        
    }
}
