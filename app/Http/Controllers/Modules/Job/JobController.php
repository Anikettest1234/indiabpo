<?php

namespace App\Http\Controllers\Modules\Job;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Job;
use App\Model\City;
use App\Model\Company;
use App\Model\JobType;
use App\Model\Category;
use App\User;
use App\Model\AppliedJob;
use App\Model\Resume;

class JobController extends Controller
{
    //
    /**
    * Method : index
    * Description :  for job details page of this project
    * Author : Rajib
    * Date : 27-06-2020
    */
    public function index($slug = NULL)
    {
        $data['job']=Job::with('appliedJob', 'userFav', 'companyName', 'Category', 'subCategory', 'cityName', 'JobType', 'employer')->where('slug', $slug)->first();
        //dd($data);
        return view('modules.job.job_details')->with($data);
    }

    /**
    *
    */
    public function appliedjob(Request $request){
        $request->validate([
            'phone' => 'required',
            'email' => 'required',
            'address' => 'required',
            'message' => 'required',
            'resume_file'  => 'required'
        ]);

        $new['user_id']       = Auth()->user()->id;
        $new['phone']         = @$request->phone;
        $new['email']         = @$request->email;
        $new['company']       = @$request->company;
        $new['currentctc']    = @$request->currentctc;
        $new['expectedctc']   = @$request->expectedctc;
        $new['noticeperiod']  = @$request->noticeperiod;
        $new['lastworkdate']  = date('Y-m-d', strtotime(@$request->lastworkdate));
        $new['totalexp']      = @$request->totalexp;
        $new['relaventexp']   = @$request->relaventexp;
        $new['address']       = @$request->address;
        $new['message']       = @$request->message;
        $new['job_id']        = @$request->job_id;

        $data['user']=User::where('id', Auth()->user()->id)->first();
        if($data['user']->user_type=='J'){
            if(@$request->resume_file) {
                $filename = time().'_'.$request->resume_file->getClientOriginalName();
                $destinationPath = "storage/app/public/upload/resume/";
                $request->resume_file->move($destinationPath, $filename);

                $cv['file'] = $filename;
                $cv['name'] = @$request->resume_name;
                $cv['user_id'] = Auth()->user()->id;

                $add = Resume::create($cv);
                $new['resume_id'] = @$add->id;
            }
            $added = AppliedJob::create($new);
            if(@$added){
                \Session::flash('success', 'You have been successfully applied for this job.');
                return redirect()->back();
            }
            else{
                \Session::flash('error', 'Somethings went\'s to wrong. Please try again.');
                return redirect()->back();
            }
        }
        else{
            \Session::flash('error', 'You are not applicable for apply this job.');
            return redirect()->back();
        }
    }
}
