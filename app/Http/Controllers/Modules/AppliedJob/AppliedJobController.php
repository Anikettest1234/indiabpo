<?php

namespace App\Http\Controllers\Modules\AppliedJob;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\AppliedJob;
use App\Model\Job;

class AppliedJobController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['appliedjob']=AppliedJob::with('job', 'job.companyName', 'job.cityName', 'job.JobType', 'job.userFav')->where('user_id', Auth()->user()->id)->orderBy('id', 'desc')->paginate(5);
        //dd($data);
        return view('modules.applied_job.applied_job')->with($data);
    }
}
