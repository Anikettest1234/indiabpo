<?php

namespace App\Http\Controllers\Modules\Membership;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Package;
use App\Model\UserSubscription;
use App\Model\Payment;
use App\User;
use Auth;
use Mail;
use App\Mail\SubscriptionMail;
# payment
use App\Http\Controllers\Modules\Paypal\PaypalClientController;
use PayPalCheckoutSdk\Orders\OrdersCreateRequest;
use PayPalCheckoutSdk\Orders\OrdersAuthorizeRequest;
use PayPalCheckoutSdk\Orders\OrdersCaptureRequest;
use PayPalCheckoutSdk\Orders\OrdersGetRequest;

class MembershipController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['membership'] = Package::get();
        return view('modules.membership.membership')->with($data);
    }

    /**
    * Method: payment
    * Description: This method is used to make payment for subscription
    * Author: Sanjoy
    */
    public function payment(Request $request) {
        # insert to user subscription table
        if(Auth()->user()->subscription_ended>=date('Y-m-d') && Auth()->user()->subscription_id==$request->upgrade){
            $package = Package::find($request->upgrade);
            $userSubscription = UserSubscription::create([
                'user_id'    => Auth::user()->id,
                'package_id' => $request->upgrade,
                'start_date' => date('Y-m-d'),
                'end_date'   => date('Y-m-d', strtotime(Auth()->user()->subscription_ended. ' + 30 day')),
                'status'     => 'I', // it will active when payment is done.,
                'amount'     => $package->plan_price
            ]);
        } else {
            $package = Package::find($request->upgrade);
            $userSubscription = UserSubscription::create([
                'user_id'    => Auth::user()->id,
                'package_id' => $request->upgrade,
                'start_date' => date('Y-m-d'),
                'end_date'   => date('Y-m-d', strtotime('+30 day')),
                'status'     => 'I', // it will active when payment is done.,
                'amount'     => $package->plan_price
            ]);
        }
        # init the payment
        $payment = $this->initPayment($userSubscription, $package);

        # redirect paypal site for accept payment
        foreach ($payment->result->links as $key => $value) {
            if($value->rel == 'approve') {
                # redirect to paypal 
                return redirect()->to($value->href);
            }
        }
        
    }

    /**
    * method: makePayment
    * Description: This method is used to make payment
    * Author: Sanjoy
    */
    private function initPayment($userSubscription = [], $package) {
        # insert to payment table
        $payment = $this->makePayment($userSubscription);

        # create order on paypal
        $request = new OrdersCreateRequest();
        $request->prefer('return=representation');
        $request->body = $this->buildRequestBody([
            'amount'            => $userSubscription->amount,
            'custom_id'         => $payment->id.'@'.$userSubscription->id,
            'description'       => 'Subscription for plan: '.$package->plan_name,
            'soft_descriptor'   => 'Payer ID #'. Auth::user()->id,
            'reference_id'      => $payment->reference_id
        ]);
        
        // 3. Call PayPal to set up a transaction
        $client = PaypalClientController::client();
        $response = $client->execute($request);

        return $response;
    }

    /**
    * Method: makePayment
    * Description: This method is used to insert to payment table
    * Author: Sanjoy
    */
    private function makePayment($userSubscription = []) {
        return $payment = Payment::create([
            'user_id'       => Auth::user()->id,
            'subscription_id' => $userSubscription->id,
            'status'        => 'I',
            'reference_id'  => strtoupper(str_random(6)).rand(1111,9999),
            'amount'        => $userSubscription->amount
        ]);
    }

    /**
     * Setting up the JSON request body for creating the order with minimum request body. The intent in the
     * request body should be "AUTHORIZE" for authorize intent flow.
     *
     */
    private static function buildRequestBody($request) {
        return [
            'intent' => 'CAPTURE',
            'application_context' => [
                'return_url' => route('membership.return'),
                'cancel_url' => route('membership.cancel')
            ],
            'purchase_units' => [
                0 => [
                    'reference_id'      => @$request['reference_id'],
                    'description'       => @$request['description'],
                    'custom_id'         => @$request['custom_id'],
                    'soft_descriptor'   => @$request['soft_descriptor'],
                    'amount' => [
                        'currency_code' => 'USD',
                        'value'         => @$request['amount']
                    ]
                ]
            ]
        ];
    }
    
    /**
    * Method: makePayment
    * Description: This method is used to make payment
    * Author: Sanjoy
    */
    private function updatePayment($id, $reqData = []) {
        $payment = Payment::where(['id' => $id])->update($reqData);
    }

    /**
    * Method: paymentReturn
    * Description: This method is used to respond from peyment gateway
    */
    public function paymentReturn(Request $request) {
        try {
            $request = new OrdersCaptureRequest($request->token);

            // 3. Call PayPal to capture an authorization
            $client = PaypalClientController::client();
            $response = $client->execute($request);
            // dd($response);
            // 4. Save the capture ID to your database. Implement logic to save capture to your database for future reference.
            if (@$response->result->status == 'COMPLETED') {
                # update to payment table 
                $update['status'] = 'S';
                $update['response'] = json_encode($response);
                $customId = explode('@', @$response->result->purchase_units[0]->payments->captures[0]->custom_id);
                $paymentId = $customId[0];
                $membershipId = $customId[1];
                Payment::where(['id' => $paymentId])->update($update);

                # update to user subscription table
                UserSubscription::where(['id' => $membershipId])->update(['status' => 'A']);

                # get data from user subscription
                $userSubscription = UserSubscription::where(['id' => $membershipId])->first();
                $package = Package::where('id', $userSubscription->package_id)->first();
                # update user table
                $update = [];
                $update['subscription_id'] = $userSubscription->package_id;
                $update['subscription_started'] = date('Y-m-d');
                $update['subscription_ended'] = date('Y-m-d', strtotime($userSubscription->end_date));
                $update['ads']  = @$package->ad_post_limit;
                User::where(['id' => Auth::user()->id])->update($update);
                return redirect()->route('membership.success',['trackId' => @$paymentId]);
            }
        } catch(\Exception $e) {
            dd($e->getMessage());
        }
    }


    public function paymentSuccess(Request $request){
        $data['payment'] = Payment::with('user', 'subscription.package')->where('id', $request->trackId)->first();
        if($data){
            $mailArr = [
                'name' => $data['payment']->user->name,
                'email' => $data['payment']->user->email,
                'plan' => $data['payment']->subscription->package->plan_name,
                'amount'  => $data['payment']->amount,
                'expiry' => $data['payment']->subscription->end_date
            ];

        Mail::send(new SubscriptionMail($mailArr));
        }
        return view('modules.payment.success')->with($data);
    }

    /**
    */
    public function paymentCancel(Request $request) {
        $orderId = $request->token;
        // 3. Call PayPal to get the transaction details
        $client = PaypalClientController::client();
        $response = $client->execute(new OrdersGetRequest($orderId));
        if(@$response->result->status == 'CREATED') {
            //$update['status'] = 'C'
            //$payment = Payment::where(['id' => $payment->id])->update($update);
        }
        return view('modules.payment.error');
    }
}
