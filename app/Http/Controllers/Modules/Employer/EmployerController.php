<?php

namespace App\Http\Controllers\Modules\Employer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Model\Job;


class EmployerController extends Controller
{
    //
    /**
    * Method : index
    * Description :  for employer details page of this project
    * Author : Rajib
    * Date : 01-07-2020
    */
    public function index($id = NULL)
    {
        $data['employer'] = User::with('jobs' ,'jobs.cityName', 'jobs.JobType')->where('id', decrypt($id))->first();
        //dd($data);
        return view('modules.employer.employer')->with($data);
    }
}
