<?php

namespace App\Http\Controllers\Modules\FavouriteJob;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\Model\Job;
use App\Model\City;
use App\Model\Company;
use App\Model\JobType;
use App\Model\UserFavoriteJob;

class FavouriteJobController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $data['job']=UserFavoriteJob::with('jobs.companyName', 'jobs.cityName', 'jobs.JobType')->orderBy('id', 'desc')->where('user_id', @Auth::user()->id)->paginate(5);
        //dd($data);
        return view('modules.favourite_job.favourite_job')->with($data);
    }

    /**
    * Method: addRemoveToFav
    * Description: This method is used to add or remove to job seekers favorite list
    * Author: Rajib
    */

    public function addRemoveToFav(Request $request) {
        $response = [
            'josnrpc'   => '2.0'
        ];
        $reqData = $request->params;

        # check already added or not
        $userFav = UserFavoriteJob::where(['job_id' => $reqData['job_id'], 'user_id' => Auth::user()->id])->first();
        if(@$userFav) {
            # if already added then remove it from table
            $userFav = UserFavoriteJob::where(['job_id' => $reqData['job_id'], 'user_id' => Auth::user()->id])->delete();
            $response['result']['message'] = 'Job has been successfully removed from your favorite list.';
            $response['result']['removed'] = true;
        } else {
            $userFav = UserFavoriteJob::create(['job_id' => $reqData['job_id'], 'user_id' => Auth::user()->id]);

            $response['result']['message'] = 'Job has been successfully added to your favorite list.';
            $response['result']['added'] = true;
        }

        return response()->json($response, 200);
    }

    public function delete($id){
        // dd($id);
        $delete = UserFavoriteJob::where(['user_id' => Auth::id()])->find($id);
        if(@$delete) {
            UserFavoriteJob::destroy($id);
            session()->flash('success', 'Job has been successfully removed from your favorite list.');
            return redirect()->back();
        } else {
            session()->flash('error', 'Unauthorize access.');
            return redirect()->back();
        }
    }
}
