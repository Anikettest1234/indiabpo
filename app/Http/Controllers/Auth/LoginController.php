<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

use Socialite;
use SocialiteProviders\Manager\Config;
use App\User;
use Auth;
use Mail;
use File;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }
        
        # check user's cart is empty or not.
        // $this->getCart(session()->getId());

        if ($this->attemptLogin($request)) {

            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

        /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);
    }

    /**
    * Method : socialLogin,handleProviderCallback
    * Desc: For Social Login
    * Author: Rajib
    */
    public function socialLogin($social, $userType = NULL) {
        $socialite = Socialite::driver($social);
        if($userType) {
            $socialite = $socialite->with(['state' => $userType]);
        } else {
            $socialite = $socialite->stateless();
        }
        return $socialite->redirect();
    }

    /**
    * Method: handleProviderCallback
    * Description: This method is used to save user data getting from social site
    * Author: Sanjoy
    */
    public function handleProviderCallback(Request $request, $social)
    {
        $userType = $request->state;
        $userSocial = Socialite::driver($social)->stateless()->user();
        $user = User::where(['email' => $userSocial->getEmail(),'is_email_verified' => 'Y'])->where('status', '!=', 'D')->first();

        if($user) {  
            Auth::login($user);
            return redirect()->route('dashboard');
        } else {
            //split full name
            $name = $userSocial->getName();
            if($userSocial->getEmail()) {
                //save profile image
                $fileContents = file_get_contents($userSocial->getAvatar());
                $file_name = $userSocial->getId() . ".jpg";
                File::put('storage/app/public/upload/profile/' . $file_name, $fileContents);

                if($social=='facebook') {
                    $user = User::create([
                        'fname'         => $first_name,
                        'lname'         => $last_name,
                        'image'         => $file_name,
                        'email'         => $userSocial->getEmail(),
                        'facebook_id'   => $userSocial->getId(),
                        'provider'      => 'facebook',
                        'is_email_verified' => 'Y',
                        'signup_from' => 'F',
                        'status'      => 'A'
                    ]);
                } else {
                    $user = User::create([
                        'name'       => @$name,
                        'image'       => $file_name,
                        'email'       => $userSocial->getEmail(),
                        'google_id'   => $userSocial->getId(),
                        'provider'    => 'google',
                        'is_email_verified'   => 'Y',
                        'signup_from' => 'G',
                        'status'      => 'A',
                        'user_type'   => @$userType?$userType:'J',
                    ]);
                }
                Auth::login($user);
                return redirect()->route('dashboard');
            }
        }
    }

    /**
     * Get the failed login response instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function sendFailedLoginResponse(Request $request)
    {
        $errors = [$this->username() => trans('auth.failed')];
        $user = \App\User::where($this->username(), $request->{$this->username()})->first();

        // Check if user was successfully loaded, that the password matches
        // and active is not 1. If so, override the default error message.
        if ($user && \Hash::check($request->password, $user->password) && $user->status == 'I') {
            $errors = [$this->username() => 'Your account is not active.'];
        }
        // if ($user && \Hash::check($request->password, $user->password) && $user->status == 'AA') {
        //     $errors = [$this->username() => 'Your account is not active by Admin.'];
        // }
        if ($user && \Hash::check($request->password, $user->password) && $user->status == 'U') {
            $errors = ['not verified' => 'Your email is not verified.'];
        }
        
        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }
        // dd($errors);
        return redirect()->back()
        ->withInput($request->only($this->username(), 'remember'))
        ->withErrors($errors);
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        $credentials =  $request->only($this->username(), 'password');
        $credentials['status'] = 'A';

        return $credentials;
    }
}
