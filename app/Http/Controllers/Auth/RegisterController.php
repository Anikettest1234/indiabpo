<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;

use Mail;
use App\Mail\VerificationMail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/register-success';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'user_type' => ['required', 'string'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user =  User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'user_type' => $data['user_type'],
            'email_vcode'=> time(),
            'status'    => 'U',
            'is_email_verified' => 'N'
        ]);
        $this->sendVerificationMail($user);
        return $user;
    }

     /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        // $this->guard()->login($user);

        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }


    public function regisrterSuccess() {
        return view('auth.register_success');
    }

    /**
    * Method: sendVerificationMail
    * Desc: This method is used for send verification mail to user
    */
    public function sendVerificationMail($reqData)  {
        Mail::send(new VerificationMail($reqData));
    }

    public function registerSuccess () {
        return view('auth.register_success');
    }

    public function verify ($vcode, $userId) {
        try {
            $user = User::where('email_vcode', $vcode)->first();
            if(@$user->email_vcode != null && @$user->status == 'U') {
                $update['email_verified_at'] = date('Y-m-d H:i:s');
                $update['email_vcode'] = NULL;
                $update['status'] = 'A';
                $update['is_email_verified'] = 'Y';
                User::where('id', $user->id)->update($update);
                return view('auth.verification_success');
            } else {
                return view('auth.link_expire');
            }
        }
        catch (\Exception $e) {
            dd($e->getMessage());
            $error = [
                'img'     => 'oops.png',
                'heading' => 'Sorry!',
                'message' => 'Something went to wrong.',
                'title'   => 'Sobrandoinggresso',
            ];
            return view('modules.errors.all_errors')->with($error);
        }
    }

    public function verificationSuccess() {
        return view('auth.verification_success');
    }
}
