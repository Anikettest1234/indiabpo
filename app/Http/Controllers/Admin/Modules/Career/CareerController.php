<?php

namespace App\Http\Controllers\Admin\Modules\Career;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Career;
use App\Model\Category;

class CareerController extends Controller
{
    public function index() {
    	$data['career'] = Career::with('category')->get();
    	//dd($data);
    	return view('admin.modules.career.career')->with($data);
    }

    public function add() {
    	$data['category'] = Category::where('parent_id', '0')->where('status', '!=', 'D')->get();
    	return view('admin.modules.career.add')->with($data);
    }

    public function addcareer(Request $request) {
    	$request->validate([
            'title'			=> 'required',
            'category_id'	=> 'required',
            'location'		=> 'required',
            'year'          => 'required',
            'description'	=> 'required'
        ]);
    	$new['title']		= @$request->title;
    	$new['category_id']	= @$request->category_id;
    	$new['location']	= @$request->location;
        $new['year']        = @$request->year;
        $new['description']	= @$request->description;

        $add = Career::create($new);
        if(@$add) {
            \Session::flash('success', 'New career has been created successfully.');
            return redirect()->back();
        } else {
            \Session::flash('error', 'Somethings went wrong. Please try again.');
            return redirect()->back();
        }
    }

    public function edit($id) {
    	$data['category'] = Category::where('parent_id', '0')->where('status', '!=', 'D')->get();
    	$data['career'] = Career::where('id', $id)->first();
    	return view('admin.modules.career.edit')->with($data);
    }

    public function update(Request $request) {
    	$request->validate([
            'title'			=> 'required',
            'category_id'	=> 'required',
            'location'		=> 'required',
            'year'          => 'required',
            'description'	=> 'required'
        ]);
    	$new['title']		= @$request->title;
    	$new['category_id']	= @$request->category_id;
    	$new['location']	= @$request->location;
        $new['year']        = @$request->year;
        $new['description']	= @$request->description;

        $update = Career::where(['id' => $request->id])->update($new);
        if(@$update) {
            \Session::flash('success', 'Career has been updated successfully.');
            return redirect()->back();
        } else {
            \Session::flash('error', 'Somethings went wrong. Please try again.');
            return redirect()->back();
        }
    }

    public function change($id = NULL) {
    	$career = Career::find($id);
        if($career->status=='A') {
            Career::where(['id' => $id ])->update(['status' => 'I']);
        }
        else {
            Career::where(['id' => $id ])->update(['status' => 'A']);
        }
        session()->flash('success', 'Career status has been change successfully.');
        return redirect()->back();
    }

    public function delete($id = NULL) {
        $career = Career::find($id);
        if(@$career) {
            Career::destroy($id);
            session()->flash('success', 'Career has been deleted successfully.');
            return redirect()->back();
        } else {
            session()->flash('error', 'Unauthorize access.');
            return redirect()->back();
        }
    }
}
