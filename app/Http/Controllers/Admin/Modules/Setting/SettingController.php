<?php

namespace App\Http\Controllers\Admin\Modules\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Setting;
use App\Model\Package;

class SettingController extends Controller
{
    /* 
    *metod: index
    *Description: This method is used to add or change premium job settings
    *Author: Rajib
    */
    public function index(){
    	@$data['premium'] = Setting::orderBy('id', 'desc')->first();
    	return view('admin.modules.settings.jobpremium')->with($data);
    }

    public function update(Request $request){
        $request->validate([
            'featured_amount'   => 'required',
            'featured_days'     => 'required',
            'urgent_amount'     => 'required',
            'urgent_days'       => 'required',
            'highlight_amount'  => 'required',
            'highlight_days'    => 'required'
        ]);
        $new['featured_amount']     = @$request->featured_amount;
        $new['featured_days']       = @$request->featured_days;
        $new['urgent_amount']       = @$request->urgent_amount;
        $new['urgent_days']         = @$request->urgent_days;
        $new['highlight_amount']    = @$request->highlight_amount;
        $new['highlight_days']      = @$request->highlight_days;

        $add = Setting::where(['id' => '1' ])->update($new);
        if(@$add) {
            \Session::flash('success', 'Premium job setting has been updated successfully.');
            return redirect()->back();
        } else {
            \Session::flash('error', 'Somethings went wrong. Please try again.');
            return redirect()->back();
        }

    }

    public function subscription(){
        @$data['package'] = Package::get();
    	return view('admin.modules.settings.subscription')->with($data);
    }

    public function updatesubscription(Request $request){
        foreach($request->featured_ad_fee as $key=>$val) {
            $new['ad_post_limit']               = @$request->ad_post_limit[$key];
            $new['ad_expiry_in']                = @$request->ad_expiry_in[$key];
            $new['featured_ad_fee']             = @$request->featured_ad_fee[$key];
            $new['featured_ad_days']            = @$request->featured_ad_days[$key];
            $new['urgent_ad_fee']               = @$request->urgent_ad_fee[$key];
            $new['urgent_ad_days']              = @$request->urgent_ad_days[$key];
            $new['highlight_ad_fee']            = @$request->highlight_ad_fee[$key];
            $new['highlight_ad_days']           = @$request->highlight_ad_days[$key];
            if(@$request->show_ad_on_home_premium[$key]=='Y')
                $new['show_ad_on_home_premium']     = 'Y';
            else
                $new['show_ad_on_home_premium']     = 'N';
            if(@$request->show_add_search_home[$key])
                $new['show_add_search_home']        = 'Y';
            else
                $new['show_add_search_home']        = 'N';
            if(@$request->show_top_search_result_cat[$key])
                $new['show_top_search_result_cat']  = 'Y';
            else
                $new['show_top_search_result_cat']  = 'N';
            $new['plan_price']                  = @$request->plan_price[$key];

            $update = Package::where('id', $key+1)->update($new);
        }

        if(@$update) {
            \Session::flash('success', 'Subscription Setting has been updated successfully.');
            return redirect()->back();
        } else {
            \Session::flash('error', 'Somethings went wrong. Please try again.');
            return redirect()->back();
        }
    }
}
