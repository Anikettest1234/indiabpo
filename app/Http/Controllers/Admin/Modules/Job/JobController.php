<?php

namespace App\Http\Controllers\Admin\Modules\Job;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Job;
use App\Model\AppliedJob;

class JobController extends Controller
{
    public function index()
    {
    	$data['jobs'] = Job::with('companyName', 'Category', 'subCategory', 'JobType', 'employer')->where('status', '!=', 'D')->orderBy('id', 'desc')->get();
    	return view('admin.modules.jobs.jobs')->with($data);
    }

    public function change($id)
    {
        $job = Job::find($id);
        if($job->status=='A') {
            $job = Job::where(['id' => $id ])->update(['status' => 'I']);
        }
        else {
            $job = Job::where(['id' => $id ])->update(['status' => 'A']);
        }
        session()->flash('success', 'Job status has been change successfully.');
        return redirect()->back();
    }

    public function delete($id)
    {
        $job = Job::where(['id' => $id ])->update(['status' => 'D']);
        session()->flash('success', 'Job has been deleted successfully.');
        return redirect()->back();
    }

    public function applieduser($id = NULL){
        $data['applieduser'] = AppliedJob::with('user', 'user.category', 'user.subCategory')->where('job_id',$id)->orderBy('id', 'desc')->get();
        return view('admin.modules.jobs.applieduser')->with($data);
    }
}
