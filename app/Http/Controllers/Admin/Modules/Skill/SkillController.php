<?php

namespace App\Http\Controllers\Admin\Modules\Skill;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Category;
use App\Model\Skill;
use Auth;

class SkillController extends Controller
{
    public function index() {
        $data['skills'] = Skill::where('status', '!=', 'D')->get();
        //dd($data);
        return view('admin.modules.skill.skill')->with($data);
    }

    public function add() {
       
        return view('admin.modules.skill.addskill');
    }

    public function addskills(Request $request) {
        $request->validate([
            'skill'        => 'required'
        ]);

        //dd($request->all());
        $new['skill']      = @$request->skill;
        $new['user_id']      = Auth::guard('admin')->user()->id;
        
        

        

        $category = Skill::where(['skill'=>@$request->skill])->first();

        if(@$category){
            \Session::flash('error', 'Skill already exists. Change name. Please try again.');
            return redirect()->back();
        }
        //dd($category);

        $add = Skill::create($new);

        $slug = str_slug(@$request->name);
        $checkSlug = Skill::where('slug', $slug)->first();
        
        if(@$checkSlug){
            $slug=$slug.'-'.$add->id;
        }
        Skill::where('id', $add->id )->update(['slug' => $slug]);

        if(@$add) {
            \Session::flash('success', 'New skill has been created successfully.');
            return redirect()->back();
        } else {
            \Session::flash('error', 'Somethings went wrong. Please try again.');
            return redirect()->back();
        }
    }

    /* For edit Category */
    public function edit($id)
    {
        //$data['categories'] = Category::where('parent_id' , 0)->where('status', '!=', 'D')->get();
        //$data['category'] = Category::with('subCategory')->where('id', $id)->first();
        $data['skill'] = Skill::where('id', $id)->first();
        //dd($data);
        return view('admin.modules.skill.editskill')->with($data);
    }

    public function update(request $request){
        $request->validate([
            'skill'        => 'required'
            //'image'   => 'nullable|image|mimes:jpeg,jpg,png'
        ]);

        //dd($request->all());
        $new['skill']      = @$request->skill;
        

        /*if(@$request->parent_id){
            $new['parent_id']     = @$request->parent_id;
        }*/

        
        //dd($new);
        $add = Skill::where(['id' => $request->id])->update($new);
        //dd($add);

        if(@$add) {
            \Session::flash('success', 'Skill has been updated successfully.');
            return redirect()->back();
        } else {
            \Session::flash('error', 'Somethings went wrong. Please try again.');
            return redirect()->back();
        }
    }

    public function change($id)
    {
        // dd($id);
        $categories = Skill::find($id);
        if($categories->status=='A') {
            $categories = Skill::where(['id' => $id ])->update(['status' => 'I']);
        }
        else {
            $categories = Skill::where(['id' => $id ])->update(['status' => 'A']);
        }
        session()->flash('success', 'Skill status has been change successfully.');
        return redirect()->back();
    }

    /* For delete Category */
    public function delete($id) {
        // dd($id);
        $categories = Skill::where(['id' => $id ])->update(['status' => 'D']);
        session()->flash('success', 'Skill has been deleted successfully.');
        return redirect()->back();
    }
}
