<?php

namespace App\Http\Controllers\Admin\Modules\Jobseeker;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Admin;
use App\User;
use App\Model\Category;
use App\Model\AppliedJob;

class JobseekerController extends Controller
{
    //
    public function index()
    {
    	$data['joobseeker'] = User::with('category', 'subCategory')->where('user_type', 'J')->where('status', '!=', 'D')->orderBy('id', 'desc')->get();
    	return view('admin.modules.jobseekers.jobseekers')->with($data);
    }

    public function change($id)
    {
        // dd($id);
        $joobseeker = User::find($id);
        if($joobseeker->status=='A') {
            $joobseeker = User::where(['id' => $id ])->update(['status' => 'I']);
        }
        else {
            $joobseeker = User::where(['id' => $id ])->update(['status' => 'A']);
        }
        session()->flash('success', 'Job Seeker status has been change successfully.');
        return redirect()->back();
    }

    public function delete($id)
    {
        // dd($id);
        $joobseeker = User::where(['id' => $id ])->update(['status' => 'D']);
        session()->flash('success', 'Job seeker has been deleted successfully.');
        return redirect()->back();
    }

    public function appliedjob($id){
        $data['appliedjob'] = AppliedJob::with('job', 'job.Category', 'job.subCategory', 'job.JobType', 'job.companyName')->where('user_id', $id)->get();
        //dd($data);
        return view('admin.modules.jobseekers.appliedjob')->with($data);
    }
}
