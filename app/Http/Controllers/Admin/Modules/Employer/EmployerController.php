<?php

namespace App\Http\Controllers\Admin\Modules\Employer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use App\Model\Ad;
use App\Model\Job;
use App\Model\Company;
use App\Model\Training;

class EmployerController extends Controller
{
    public function index()
    {
    	$data['employer'] = User::with('city')->where('user_type', 'E')->where('status', '!=', 'D')->orderBy('id', 'desc')->get();
    	//dd($data);
    	return view('admin.modules.employers.employers')->with($data);
    }

    public function change($id)
    {
        // dd($id);
        $employer = User::find($id);
        if($employer->status=='A') {
            $employer = User::where(['id' => $id ])->update(['status' => 'I']);
        }
        else {
            $employer = User::where(['id' => $id ])->update(['status' => 'A']);
        }
        session()->flash('success', 'Employer status has been change successfully.');
        return redirect()->back();
    }

    public function delete($id)
    {
        // dd($id);
        $employer = User::where(['id' => $id ])->update(['status' => 'D']);
        session()->flash('success', 'Employer has been deleted successfully.');
        return redirect()->back();
    }

    public function company($id = NULL){
        $data['companies'] = Company::with('cityName', 'stateName', 'countryName')->where('status', '!=', 'D')->where('user_id', $id)->orderBy('id', 'desc')->get();
        //dd($data);
        return view('admin.modules.employers.company')->with($data);
    }

    public function changecompany($id)
    {
        // dd($id);
        $company = Company::find($id);
        if($company->status=='A') {
            $company = Company::where(['id' => $id ])->update(['status' => 'I']);
        }
        else {
            $company = Company::where(['id' => $id ])->update(['status' => 'A']);
        }
        session()->flash('success', 'Company status has been change successfully.');
        return redirect()->back();
    }

    public function deletecompany($id)
    {
        // dd($id);
        $company = Company::where(['id' => $id ])->update(['status' => 'D']);
        session()->flash('success', 'Company has been deleted successfully.');
        return redirect()->back();
    }

    public function jobs($id){
        $data['jobs'] = Job::with('companyName')->where('user_id', $id)->where('status', '!=', 'D')->orderBy('id', 'desc')->get();
        //dd($data);
        return view('admin.modules.employers.jobs')->with($data);
    }

    public function training($id = NULL)
    {
        $data['training'] = Training::with('company','employer')->where('status', '!=', 'D')->where('user_id', $id)->get();
        return view('admin.modules.employers.training')->with($data);
    }

    public function ads($id = NULL) {
        $data['ads'] = Ad::where('user_id', $id)->orderBy('id', 'desc')->get();
        return view('admin.modules.employers.ads')->with($data);
    }

    public function changeads($id = NULL) {
        $ad = Ad::find($id);
        if($ad->status=='A') {
            Ad::where('id', $id)->update(['status' => 'I']);
        }
        else {
            Ad::where('id', $id)->update(['status' => 'A']);
        }
        session()->flash('success', 'Ad status has been change successfully.');
        return redirect()->back();
    }

    public function deleteads($id = NULL) {
        Ad::destroy($id);;
        session()->flash('success', 'Ad has been deleted successfully.');
        return redirect()->back();
    }
}
