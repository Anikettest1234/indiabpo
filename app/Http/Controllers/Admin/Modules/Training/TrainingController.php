<?php

namespace App\Http\Controllers\Admin\Modules\Training;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Training;

class TrainingController extends Controller
{
    public function index() {
    	$data['training'] = Training::with('company','employer')->where('status', '!=', 'D')->get();
    	return view('admin.modules.training.training')->with($data);
    }

    public function change($id = NULL)
    {
    	$training = Training::find($id);
        if($training->status=='A') {
            $training = Training::where(['id' => $id ])->update(['status' => 'I']);
        }
        else {
            $training = Training::where(['id' => $id ])->update(['status' => 'A']);
        }
        session()->flash('success', 'Training status has been change successfully.');
        return redirect()->back();
    }

    public function delete($id)
    {
        $training = Training::where(['id' => $id ])->update(['status' => 'D']);
        session()->flash('success', 'Training has been deleted successfully.');
        return redirect()->back();
    }
}
