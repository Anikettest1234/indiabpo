<?php

namespace App\Http\Controllers\Admin\Modules\Category;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Model\Category;

class CategoryController extends Controller
{
    public function index() {
        $data['categories'] = Category::with('subCategory')->where('parent_id', 0)->where('status', '!=', 'D')->get();
        //dd($data);
        return view('admin.modules.category.category')->with($data);
    }

    public function add() {
        $data['categories'] = Category::where('parent_id', 0)->where('status', '!=', 'D')->get();
        return view('admin.modules.category.addcategory')->with($data);
    }

    public function addcategory(Request $request) {
        $request->validate([
            'name'        => 'required',
            'description' => 'required',
            'image'   => 'nullable|image|mimes:jpeg,jpg,png'
        ]);

        //dd($request->all());
        $new['name']      = @$request->name;
        $new['description']     = @$request->description;

        if(@$request->parent_id){
            $new['parent_id']     = @$request->parent_id;
        }

        if(@$request->image) {
            $filename = time().'_'.$request->image->getClientOriginalName();
            if(!is_dir('public/images/categories')) {
                            mkdir('public/images/categories');
                        }
            $destinationPath = "public/images/categories/";
            $request->image->move($destinationPath, $filename);
            
            $new['image'] = $filename;
        }

        $category = Category::where(['name'=>@$request->name, 'parent_id'=>@$request->parent_id])->first();

        if(@$category){
            \Session::flash('error', 'Category already exists. Change name. Please try again.');
            return redirect()->back();
        }
        //dd($category);

        $add = Category::create($new);

        $slug = str_slug(@$request->name);
        $checkSlug = Category::where('slug', $slug)->first();
        
        if(@$checkSlug){
            $slug=$slug.'-'.$add->id;
        }
        Category::where('id', $add->id )->update(['slug' => $slug]);

        if(@$add) {
            \Session::flash('success', 'New category has been created successfully.');
            return redirect()->back();
        } else {
            \Session::flash('error', 'Somethings went wrong. Please try again.');
            return redirect()->back();
        }
    }

    /* For edit Category */
    public function edit($id)
    {
        //$data['categories'] = Category::where('parent_id' , 0)->where('status', '!=', 'D')->get();
        //$data['category'] = Category::with('subCategory')->where('id', $id)->first();
        $data['category'] = Category::with('parentCategory')->where('id', $id)->first();
        //dd($data);
        return view('admin.modules.category.editcategory')->with($data);
    }

    public function update(request $request){
        $request->validate([
            'name'        => 'required',
            'description' => 'required',
            //'image'   => 'nullable|image|mimes:jpeg,jpg,png'
        ]);

        //dd($request->all());
        $new['name']      = @$request->name;
        $new['description']     = @$request->description;

        /*if(@$request->parent_id){
            $new['parent_id']     = @$request->parent_id;
        }*/

        if(@$request->image) {
            $filename = time().'_'.$request->image->getClientOriginalName();
            $destinationPath = "public/images/categories/";
            $request->image->move($destinationPath, $filename);
            
            $new['image'] = $filename;
        }
        //dd($new);
        $add = Category::where(['id' => $request->id])->update($new);
        //dd($add);

        if(@$add) {
            \Session::flash('success', 'Category has been updated successfully.');
            return redirect()->back();
        } else {
            \Session::flash('error', 'Somethings went wrong. Please try again.');
            return redirect()->back();
        }
    }

    public function change($id)
    {
        // dd($id);
        $categories = Category::find($id);
        if($categories->status=='A') {
            $categories = Category::where(['id' => $id ])->update(['status' => 'I']);
        }
        else {
            $categories = Category::where(['id' => $id ])->update(['status' => 'A']);
        }
        session()->flash('success', 'Category status has been change successfully.');
        return redirect()->back();
    }

    /* For delete Category */
    public function delete($id) {
        // dd($id);
        $categories = Category::where(['id' => $id ])->update(['status' => 'D']);
        session()->flash('success', 'Category has been deleted successfully.');
        return redirect()->back();
    }
}
