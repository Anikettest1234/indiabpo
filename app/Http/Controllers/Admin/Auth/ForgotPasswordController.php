<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use Illuminate\Http\Request;
use App\Mail\ForgotPassword;
use Validator;
use Mail;
use App\Admin;
use Session;
use Hash;
use Auth;
class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.guest:admin');
    }

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLinkRequestForm()
    {
        return view('admin.auth.passwords.email');
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker('admins');
    }


    public function sendResetLinkEmail(Request $request)
    {
        Validator::make($request->all(),
                    [
                        "email" => "required|email"
                    ])->validate();
        $reqData = Admin::where('email', $request->email)->first();
        if(@$reqData) {
            $vcode = mt_rand(1000000, 9999999);
            Admin::where('email', $request->email)->update([
                    'vcode' => $vcode
            ]);
            Mail::send(new ForgotPassword($reqData->email, $reqData->name, $vcode));
            return view('admin.auth.passwords.success_mail');
        } else {
            session()->flash('error', "Please enter correct email address.");
            return redirect()->back();
        }
    }

    public function showResetForm($vcode = null)
    {
        $admin_vcode = Admin::where('vcode', $vcode)->first();
        if(@$admin_vcode) {
            Admin::where('vcode', $vcode)->update([
                'vcode' => ''
            ]);
            return view('admin.auth.passwords.reset');
        } else {
            return view('admin.auth.passwords.expired_mail');
        }
        
    }

    public function updatePassword(Request $request)
    {
        Validator::make($request->all(),
                    [
                        "password"              => "required|min:6",
                        "password_confirmation" => "required|min:6|same:password"
                    ])->validate();
        $email = Admin::where('id', '1')->first();
        Admin::where('email', $email->email)->update([
                'password' => Hash::make($request->password)
        ]);
        session()->flash("success", "Your password change successfully.");
        return redirect()->route('admin.login');
    }

}
