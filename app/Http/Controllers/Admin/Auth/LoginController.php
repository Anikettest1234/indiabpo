<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Cookie;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.guest:admin', ['except' => 'logout']);
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('admin');
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        $data = [
            "admin_email"   =>  Cookie::get('admin_email') == null ? null:  Cookie::get('admin_email'),
            "admin_password"   =>  Cookie::get('admin_password') == null ? null:  Cookie::get('admin_password')
        ];
        return view('admin.auth.login')->with('data', $data);;
    }

    /**
     * Log the user out of the application.
     *
     * @param \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {

        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect()->route('admin.login');
    }

     /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {
        if(@$request->remember) {
                if (Cookie::get('admin_email') == null && Cookie::get('admin_password') == null){

                    $buyer_email_coockie = Cookie::queue('admin_email', $request->email, 10080);
                    $buyer_pass_coockie = Cookie::queue('admin_password', $request->password, 10080);
                } elseif($request->remember == 'on') {
                    
                    // set remember me cookie
                    $buyer_email_coockie = Cookie::queue('admin_email', $request->email, 10080);
                    $buyer_pass_coockie = Cookie::queue('admin_password', $request->password, 10080);
                    $buyer_remember_coockie = Cookie::queue('remember', $request->remember, 10080); 
                }
        } 
        else {
            Cookie::queue(Cookie::forget('admin_email'));
            Cookie::queue(Cookie::forget('admin_password'));
            
        }
    }


}
