<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin;
use Auth;
use Validator;
use Hash;
use session;
use Carbon\Carbon;
use App\Model\Job;
use App\User;
use App\Model\Company;
use App\Model\Training;
use App\Model\Ad;

class HomeController extends Controller
{

    protected $redirectTo = '/admin/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin.auth:admin');
    }

    /**
     * Show the Admin dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data['employers'] = User::where(['status'=>'A', 'user_type'=>'E'])->get();
        $data['jobseeker'] = User::where(['status'=>'A', 'user_type'=>'J'])->get();
        /*$data['company'] = Company::where('status', 'A')->get();
        $data['job'] = Job::where('status', 'A')->get();
        $data['training'] = Training::where('status', 'A')->get();
        $data['ad'] = Ad::where('status', 'A')->get();
        $data['employer'] = User::with('jobs', 'plan')->where('user_type', 'E')->orderBy('id','desc')->take(5)->get();*/
        #code for customer graph
        /*$data['employerGraph'] = User::where('user_type','E')->where('status','A')->select('id','status','created_at',\DB::raw('YEAR(created_at) year, Day(created_at) day'),\DB::raw('Count(id) totalCustomer'))->whereDate('created_at', '>', Carbon::now()->subDays(7))->groupBy('day')->orderBy('day','asc')->get();
        $data['jobseekerGraph'] = User::where('user_type','E')->where('status','A')->select('id','status','created_at',\DB::raw('YEAR(created_at) year, Day(created_at) day'),\DB::raw('Count(id) totalCustomer'))->whereDate('created_at', '>', Carbon::now()->subDays(7))->groupBy('day')->orderBy('day','asc')->get();*/
        //dd($data['employerGraph']);
        return view('admin.modules.dashboard.home')->with($data);
    }

    /**
    *   Method : changePassword
    *   Use    : It is used to change password.
    *   Author : Rajib
    *   Date   : 19-07-20
    */
    public function changePassword(Request $request)
    {
        if(@$request->all()) {
            if(@$request->password) {
                Validator::make($request->all(),
                [
                    "password"              => "required|min:6",
                    "password_confirmation" => "required|min:6|same:password"
                ]
                )->validate();
                if($request->password == $request->password_confirmation) {
                    $data['password'] = Hash::make($request->password);
                } else {
                    session()->flash("error", "password and confirm password should be same");
                }
            }
            if(@$request->name) {
                 $data['name'] = $request->name;
            }
            if(@$request->email) {
                 $data['email'] = $request->email;
            }

            if(@$request->image) {
            $filename = time().'_'.$request->image->getClientOriginalName();
            $destinationPath = "public/images/admin/";
            $request->image->move($destinationPath, $filename);
            $data['image'] = $filename;
            }

            Admin::where('id', Auth::guard('admin')->user()->id)->update($data);
            session()->flash("success", "Your profile updated successfully.");
            return redirect()->back();
        } else {
            return view('admin.modules.profile.admin_profile');
        }
        //
        // 
    }

}