<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserSubscription extends Model
{
    protected $guarded = [];

    public function package()
    {
    	return $this->hasOne('App\Model\Package','id', 'package_id');
    }
}
