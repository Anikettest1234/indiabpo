<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $guarded = [];

    public function job() {
    	return $this->hasOne('App\Model\Job', 'id', 'job_id');
    }
    public function subscription() {
    	return $this->hasOne('App\Model\UserSubscription', 'id', 'subscription_id');
    }

    public function user() {
    	return $this->hasOne('App\User', 'id', 'user_id');
    }
}
