<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class JobType extends Model
{
    protected $guarded = [];

    protected $table = 'job_types';
}
