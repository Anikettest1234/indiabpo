<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class UserFavoriteJob extends Model
{
    protected $guarded = [];

    public function jobs(){
    	return $this->hasOne('App\Model\Job', 'id', 'job_id');
    }

}
