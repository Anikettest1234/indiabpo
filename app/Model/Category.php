<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = [];

    protected $table = 'categories';

    public function subCategory(){
        return $this->hasMany('App\Model\Category', 'parent_id', 'id');
    }

    public function parentCategory(){
    	return $this->hasOne('App\Model\Category', 'id', 'parent_id');
    }

    public function jobs() {
    	return $this->hasMany('App\Model\Job', 'category_id', 'id');
    }
}
