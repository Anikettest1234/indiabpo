<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AppliedJob extends Model
{
    protected $guarded = [];
    protected $table = 'applied_jobs';

    public function job(){
    	return $this->hasOne('App\Model\Job', 'id', 'job_id');
    }

    public function user(){
    	return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function resume() {
    	return $this->hasOne('App\Model\Resume', 'id', 'resume_id');
    }
}
