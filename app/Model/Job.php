<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

use Auth;
class Job extends Model
{
    protected $guarded = [];

    protected $table = 'jobs';

    public function companyName(){
    	return $this->hasOne('App\Model\Company', 'id', 'company_id');
    }

    public function cityName(){
    	return $this->hasOne('App\Model\City', 'id', 'city_id');
    }

    public function stateName(){
        return $this->hasOne('App\Model\State', 'id', 'state_id');
    }

    public function countryName(){
        return $this->hasOne('App\Model\Country', 'id', 'country_id');
    }

    public function JobType(){
    	return $this->hasOne('App\Model\JobType', 'id', 'job_type');
    }

    public function Category(){
        return $this->hasOne('App\Model\Category', 'id', 'category_id');
    }

    public function subCategory(){
        return $this->hasOne('App\Model\Category', 'id', 'subcategory_id');
    }

    public function userFav(){
        return $this->hasOne('App\Model\UserFavoriteJob', 'job_id', 'id')->where(['user_id' => @Auth::user()->id]);
    }

    public function employer(){
        return $this->hasOne('App\User', 'id', 'user_id');
    }
    
    public function appliedJob(){
        return $this->hasOne('App\Model\AppliedJob', 'job_id', 'id')->where(['user_id' => @Auth::user()->id]);
    }

    public function states(){
        return $this->hasMany('App\Model\State', 'country_id', 'country_id');
    }

    public function citys(){
        return $this->hasMany('App\Model\City', 'state_id', 'state_id');
    }

}
