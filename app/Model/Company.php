<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $guarded = [];

    protected $table = 'companies';

    public function companyNamesDetails(){
    	return $this->hasOne('App\Model\Company', 'company_id', 'id');
    }

    public function jobs(){
    	return $this->hasMany('App\Model\Job', 'company_id', 'id');
    }

    public function cityName(){
    	return $this->hasOne('App\Model\City', 'id', 'city');
    }

    public function stateName(){
    	return $this->hasOne('App\Model\State', 'id', 'state');
    }

    public function countryName(){
    	return $this->hasOne('App\Model\Country', 'id', 'country');
    }

    public function states(){
        return $this->hasMany('App\Model\State', 'country_id', 'country');
    }

    public function citys(){
        return $this->hasMany('App\Model\City', 'state_id', 'state');
    }
}
