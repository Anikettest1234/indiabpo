<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class FavouriteUser extends Model
{
    protected $guarded = [];

    public function jobseeker() {
    	return $this->hasOne('App\User', 'id', 'jobseeker_id');
    }
}
