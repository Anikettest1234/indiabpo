<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    protected $guarded = [];

    public function employer() {
    	return $this->hasOne('App\User', 'id', 'user_id');
    }
}
