<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Training extends Model
{
    protected $guarded = [];

    protected $table = 'training';

    public function company()
    {
    	return $this->hasOne('App\Model\Company', 'id', 'company_id');
    }

    public function country()
    {
    	return $this->hasOne('App\Model\Country', 'id', 'country_id');
    }

    public function state()
    {
        return $this->hasOne('App\Model\State', 'id', 'state_id');
    }

    public function city()
    {
        return $this->hasOne('App\Model\City', 'id', 'city_id');
    }

    public function states()
    {
        return $this->hasMany('App\Model\State', 'country_id', 'country_id');
    }
    public function citys()
    {
        return $this->hasMany('App\Model\City', 'state_id', 'state_id');
    }

    public function employer()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }
}
