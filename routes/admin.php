<?php

Route::group(['namespace' => 'Admin'], function() {

    Route::get('/', 'HomeController@index')->name('admin.dashboard');
    Route::get('change-password','HomeController@changePassword')->name('admin.change.password');
    Route::post('change-password','HomeController@changePassword')->name('admin.change.password');
    // Login
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('admin.login');
    Route::post('login', 'Auth\LoginController@login');
    Route::get('logout', 'Auth\LoginController@logout')->name('admin.logout');

    // Register
    Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('admin.register');
    Route::post('register', 'Auth\RegisterController@register');

    // Passwords
    Route::get('password/forget', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('admin.forget.password');
    Route::post('forget-password', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin.forget.password.link');
    Route::get('show-forget-password-form/{vcode}', 'Auth\ForgotPasswordController@showResetForm')->name('admin.show.forget.password.form');
    Route::post('update-forget-password', 'Auth\ForgotPasswordController@updatePassword')->name('admin.update.forget.password');

    // Category
    Route::get('categories', 'Modules\Category\CategoryController@index')->name('admin.categories');
    Route::get('add-categories', 'Modules\Category\CategoryController@add')->name('admin.add.categories');
    Route::post('post-categories', 'Modules\Category\CategoryController@addcategory')->name('admin.addcategories');
    Route::get('/edit-category/{id}', 'Modules\Category\CategoryController@edit')->name('admin.edit.category');
    Route::post('/update-category/', 'Modules\Category\CategoryController@update')->name('admin.update.category');
    Route::get('/change-category/{id}', 'Modules\Category\CategoryController@change')->name('admin.change.category');
    Route::get('/delete-category/{id}', 'Modules\Category\CategoryController@delete')->name('admin.delete.category');

    // Skills
    Route::get('skills', 'Modules\Skill\SkillController@index')->name('admin.skills');
    Route::get('add-skills', 'Modules\Skill\SkillController@add')->name('admin.add.skills');
    Route::post('post-skills', 'Modules\Skill\SkillController@addskills')->name('admin.addskills');
    Route::get('/edit-skills/{id}', 'Modules\Skill\SkillController@edit')->name('admin.edit.skills');
    Route::post('/update-skills/', 'Modules\Skill\SkillController@update')->name('admin.update.skills');
    Route::get('/change-skills/{id}', 'Modules\Skill\SkillController@change')->name('admin.change.skills');
    Route::get('/delete-skills/{id}', 'Modules\Skill\SkillController@delete')->name('admin.delete.skills');



    // employer
    Route::get('employers', 'Modules\Employer\EmployerController@index')->name('admin.employers');
    Route::get('/change-employers/{id}', 'Modules\Employer\EmployerController@change')->name('admin.change.employers');
    Route::get('/delete-employers/{id}', 'Modules\Employer\EmployerController@delete')->name('admin.delete.employers');
    Route::get('/companies/{slug}', 'Modules\Employer\EmployerController@company')->name('admin.employers.company');
    Route::get('/change-company/{id}', 'Modules\Employer\EmployerController@changecompany')->name('admin.change.company');
    Route::get('/delete-company/{id}', 'Modules\Employer\EmployerController@deletecompany')->name('admin.delete.company');
    Route::get('/job/{id}', 'Modules\Employer\EmployerController@jobs')->name('admin.employer.jobs');
    Route::get('/training/{id}', 'Modules\Employer\EmployerController@training')->name('admin.employer.training');
    Route::get('/ads/{id}', 'Modules\Employer\EmployerController@ads')->name('admin.employer.ads');
    Route::get('/change-ads/{id}', 'Modules\Employer\EmployerController@changeads')->name('admin.change.ads');
    Route::get('/delete-ads/{id}', 'Modules\Employer\EmployerController@deleteads')->name('admin.delete.ads');

    // jobs
    Route::get('jobs', 'Modules\Job\JobController@index')->name('admin.jobs');
    Route::get('/change-job/{id}', 'Modules\Job\JobController@change')->name('admin.change.job');
    Route::get('/delete-job/{id}', 'Modules\Job\JobController@delete')->name('admin.delete.job');
    Route::get('/applied-user/{slug}', 'Modules\Job\JobController@applieduser')->name('admin.applied.user');
    


    // jobseekers
    Route::get('jobseekers', 'Modules\Jobseeker\JobseekerController@index')->name('admin.jobseekers');
    Route::get('/change-jobseeker/{id}', 'Modules\Jobseeker\JobseekerController@change')->name('admin.change.jobseeker');
    Route::get('/delete-jobseeker/{id}', 'Modules\Jobseeker\JobseekerController@delete')->name('admin.delete.jobseeker');
    Route::get('/applied-job/{id}', 'Modules\Jobseeker\JobseekerController@appliedjob')->name('admin.applied.job');

    //Settings
    Route::get('premium-job-settings', 'Modules\Setting\SettingController@index')->name('admin.premium.job');
    Route::post('update-premium', 'Modules\Setting\SettingController@update')->name('admin.update.premium');
    Route::get('subscription-settings', 'Modules\Setting\SettingController@subscription')->name('admin.subscription.setting');
    Route::post('update-subscription', 'Modules\Setting\SettingController@updatesubscription')->name('admin.update.subscription');
    //Training
    Route::get('trainings', 'Modules\Training\TrainingController@index')->name('admin.training');
    Route::get('change-training/{id}', 'Modules\Training\TrainingController@change')->name('admin.change.training');
    Route::get('delete-training/{id}', 'Modules\Training\TrainingController@delete')->name('admin.delete.training');

    //Career
    Route::get('career', 'Modules\Career\CareerController@index')->name('admin.career');
    Route::get('add-career', 'Modules\Career\CareerController@add')->name('admin.career.add');
    Route::post('career-add', 'Modules\Career\CareerController@addcareer')->name('admin.addcareer');
    Route::get('edit-career/{id}', 'Modules\Career\CareerController@edit')->name('admin.career.edit');
    Route::post('update-career', 'Modules\Career\CareerController@update')->name('admin.career.update');
    Route::get('change-career/{id}', 'Modules\Career\CareerController@change')->name('admin.career.change');
    Route::get('delete-career/{id}', 'Modules\Career\CareerController@delete')->name('admin.career.delete');

});
