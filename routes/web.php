<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/


Auth::routes();

Route::get('/register-success', 'Auth\RegisterController@registerSuccess')->name('register.success');
Route::get('/verification-success', 'Auth\RegisterController@verificationSuccess')->name('verification.success');
Route::get('/verify/{vcode}/{userId}', 'Auth\RegisterController@verify')->name('verify');

Route::get('/login/{social}/{userType?}','Auth\LoginController@socialLogin')->where('social','twitter|facebook|linkedin|google|github|bitbucket')->where('userType','E|J');

Route::get('/login/{social}/callback','Auth\LoginController@handleProviderCallback')->where('social','twitter|facebook|linkedin|google|github|bitbucket');

Route::get('/', 'Modules\Home\HomeController@index')->name('home1');

Route::any('/search/{slug?}', 'Modules\Search\SearchController@index')->name('search');
Route::any('/companies', 'Modules\Company\CompanyController@index')->name('companies');
Route::any('/company/{slug}', 'Modules\Company\CompanyController@details')->name('company');
Route::any('/job-seekers/{slug?}', 'Modules\JobSeeker\JobSeekerController@index')->name('job.seekers');
Route::any('/contact-us', 'Modules\Contact\ContactController@index')->name('contact.us');
Route::any('/sitemap', 'Modules\Sitemap\SitemapController@index')->name('sitemap');
Route::any('/career', 'Modules\Career\CareerController@index')->name('career');
Route::any('/feedback', 'Modules\Feedback\FeedbackController@index')->name('feedback');
Route::any('/terms', 'Modules\TermsAndCondition\TermsAndConditionController@index')->name('terms');
Route::any('/privacy-policy', 'Modules\TermsAndCondition\TermsAndConditionController@privacyPolicy')->name('privacy.policy');
Route::any('/about', 'Modules\About\AboutController@index')->name('about');
Route::any('/training', 'Modules\Training\TrainingController@index')->name('training');
Route::any('/training-details/{slug}', 'Modules\Training\TrainingController@details')->name('training.details');

Route::get('/job/{slug}', 'Modules\Job\JobController@index')->name('job.details');
Route::get('/employer/{slug}', 'Modules\Employer\EmployerController@index')->name('employer.details');
Route::get('/job-seeker/{slug}', 'Modules\JobSeeker\JobSeekerController@details')->name('job.seekers.details');

// for all user
Route::get('/dashboard', 'Modules\Dashboard\DashboardController@index')->name('dashboard');
Route::post('/update-profile', 'Modules\Dashboard\DashboardController@updateProfile')->name('update.profile');
Route::post('/update-password', 'Modules\Dashboard\DashboardController@updatePassword')->name('update.password');
Route::get('/get-sub-category', 'Modules\Dashboard\DashboardController@getSubCategory')->name('get.sub.category');

// for jobseeker
Route::get('/my-resume', 'Modules\MyResume\MyResumeController@index')->name('my.resume');
Route::get('/add-resume', 'Modules\MyResume\MyResumeController@create')->name('add.resume');
Route::post('/store-resume', 'Modules\MyResume\MyResumeController@store')->name('store.resume');
Route::get('/edit-resume/{id}', 'Modules\MyResume\MyResumeController@edit')->name('edit.resume');
Route::post('/update-resume', 'Modules\MyResume\MyResumeController@update')->name('update.resume');
Route::get('/delete-resume/{id}', 'Modules\MyResume\MyResumeController@delete')->name('delete.resume');


Route::get('/favourite-jobs', 'Modules\FavouriteJob\FavouriteJobController@index')->name('favourite.job');
Route::post('/add-remove-favourite-jobs', 'Modules\FavouriteJob\FavouriteJobController@addRemoveToFav')->name('add.job.to.fav');
Route::get('/delete-favourite-job/{id}', 'Modules\FavouriteJob\FavouriteJobController@delete')->name('delete.fav.job');

Route::get('/applied-jobs', 'Modules\AppliedJob\AppliedJobController@index')->name('applied.job');
Route::post('apply-job', 'Modules\Job\JobController@appliedjob')->name('apply.job');



Route::group(['middleware'=>'App\Http\Middleware\Employer'], function() {
	// 	for employer
	Route::get('/my-jobs', 'Modules\MyJob\MyJobController@index')->name('my.job');
	Route::post('/add-job', 'Modules\MyJob\MyJobController@addJob')->name('add.job');
	Route::get('/post-job', 'Modules\MyJob\MyJobController@postJob')->name('post.job');
	Route::get('/edit-job/{id}', 'Modules\MyJob\MyJobController@edit')->name('edit.job');
	Route::post('/update-job', 'Modules\MyJob\MyJobController@update')->name('update.job');
	Route::get('/change-job/{id}', 'Modules\MyJob\MyJobController@change')->name('change.job');
	Route::get('/delete-job/{id}', 'Modules\MyJob\MyJobController@delete')->name('delete.job');
	Route::get('/payment/init', 'Modules\MyJob\MyJobController@makePayment')->name('paymnet.init');
	Route::get('/payment/return', 'Modules\MyJob\MyJobController@paymentReturn')->name('payment.return');
	Route::get('/payment/cancel', 'Modules\MyJob\MyJobController@paymentCancel')->name('payment.cancel');
	Route::get('/payment/success', 'Modules\MyJob\MyJobController@paymentSuccess')->name('payment.success');


	Route::get('/applied-jobseeker/{id}', 'Modules\MyJob\MyJobController@appliedjobseeker')->name('applied.jobseeker');
	Route::get('/jobseeker-details/{slug}', 'Modules\MyJob\MyJobController@jobseekerdetails')->name('applied.jobseeker.details');

	

	Route::get('/my-companies', 'Modules\MyCompany\MyCompanyController@index')->name('my.company');
	Route::get('/create-company', 'Modules\MyCompany\MyCompanyController@create')->name('add.company');
	Route::post('/store-company', 'Modules\MyCompany\MyCompanyController@store')->name('store.company');
	Route::get('/edit-company/{id}', 'Modules\MyCompany\MyCompanyController@edit')->name('edit.company');
	Route::post('/update-company', 'Modules\MyCompany\MyCompanyController@update')->name('update.company');
	Route::get('/delete-company/{id}', 'Modules\MyCompany\MyCompanyController@delete')->name('delete.company');
	Route::get('/company-details/{slug}', 'Modules\MyCompany\MyCompanyController@view')->name('view.company');

	Route::get('/favourite-users', 'Modules\FavouriteUser\FavouriteUserController@index')->name('favourite.user');
	Route::get('/add-favourite-users/{slug}', 'Modules\FavouriteUser\FavouriteUserController@add')->name('add.favourite.user');
	Route::get('/delete-favourite-users/{slug}', 'Modules\FavouriteUser\FavouriteUserController@delete')->name('delete.favourite.user');

	Route::get('/my-ads', 'Modules\MyAd\MyAdController@index')->name('my.ads');
	Route::get('/add-ad', 'Modules\MyAd\MyAdController@add')->name('add.ad');
	Route::post('/ad-add', 'Modules\MyAd\MyAdController@addad')->name('addad');
	Route::get('/edit-ad/{id}', 'Modules\MyAd\MyAdController@edit')->name('edit.ad');
	Route::post('/update-ad', 'Modules\MyAd\MyAdController@update')->name('update.ad');
	Route::get('/change-ad/{id}', 'Modules\MyAd\MyAdController@change')->name('change.ad');
	Route::get('/delete-ad/{id}', 'Modules\MyAd\MyAdController@delete')->name('delete.ad');

	Route::get('/membership', 'Modules\Membership\MembershipController@index')->name('membership');
	Route::post('/membership/payment', 'Modules\Membership\MembershipController@payment')->name('subscription.payment');
	Route::get('/membership/return', 'Modules\Membership\MembershipController@paymentReturn')->name('membership.return');
	Route::get('/membership/cancel', 'Modules\Membership\MembershipController@paymentCancel')->name('membership.cancel');
	Route::get('/membership/success', 'Modules\Membership\MembershipController@paymentSuccess')->name('membership.success');

	Route::get('/transaction', 'Modules\Transaction\TransactionController@index')->name('transaction');
	Route::get('/my-training', 'Modules\MyTraining\MyTrainingController@index')->name('my.training');
	Route::get('/add-training', 'Modules\MyTraining\MyTrainingController@add')->name('add.training');
	Route::post('/post-training', 'Modules\MyTraining\MyTrainingController@addtraining')->name('post.training');
	Route::get('/edit-training/{slug}', 'Modules\MyTraining\MyTrainingController@edittraining')->name('edit.training');
	Route::post('/update-training', 'Modules\MyTraining\MyTrainingController@updatetraining')->name('update.training');
	Route::get('/change-training/{id}', 'Modules\MyTraining\MyTrainingController@changetraining')->name('change.training');
	Route::get('/delete-training/{id}', 'Modules\MyTraining\MyTrainingController@deletetraining')->name('delete.training');
});
	
Route::post('/get-sub-category', 'Modules\MyJob\MyJobController@fetchSubcat')->name('sub.cat.fetch');
Route::post('/get-state', 'Modules\MyCompany\MyCompanyController@fetchState')->name('state.fetch');
Route::post('/get-city', 'Modules\MyCompany\MyCompanyController@fetchCity')->name('city.fetch');


//Clear configurations:
Route::get('/config-clear', function() {
	$status = Artisan::call('config:clear');
	return '<h1>Configurations cleared</h1>';
});

//Clear cache:
Route::get('/cache-clear', function() {
	$status = Artisan::call('cache:clear');
	return '<h1>Cache cleared</h1>';
});

//Clear configuration cache:
Route::get('/config-cache', function() {
	$status = Artisan::call('config:cache');
	return '<h1>Configurations cache cleared</h1>';
});