   <div class="navbar-area fixed-top">

            <!-- Menu For Mobile Device -->
            <div class="mobile-nav">
                <a href="index.html" class="logo">
                    <img src="assets/img/logo.png" alt="Logo">
                </a>
            </div>

            <!-- Menu For Desktop Device -->
            <div class="main-nav">
                <div class="container">
                    <nav class="navbar navbar-expand-md navbar-light">
                        <a class="navbar-brand" href="index.html">
                            <img src="{{ URL::to('public/assets/img/logo.png')}}" alt="Logo">
                        </a>
                        <div class="collapse navbar-collapse mean-menu" id="navbarSupportedContent">
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a href="#" class="nav-link dropdown-toggle active">Home <i class="icofont-simple-down"></i></a>
                                    <ul class="dropdown-menu">
                                        <li class="nav-item">
                                            <a href="index.html" class="nav-link active">Home Page 1</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="index-2.html" class="nav-link">Home Page 2</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="index-3.html" class="nav-link">Home Page 3</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a href="about.html" class="nav-link">About</a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link dropdown-toggle">Jobs <i class="icofont-simple-down"></i></a>
                                    <ul class="dropdown-menu">
                                        <li class="nav-item">
                                            <a href="job-list.html" class="nav-link">Job List</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="favourite-job.html" class="nav-link">Favourite Jobs</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="job-details.html" class="nav-link">Job Details</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="post-a-job.html" class="nav-link">Post A Job</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link dropdown-toggle">Candidates <i class="icofont-simple-down"></i></a>
                                    <ul class="dropdown-menu">
                                        <li class="nav-item">
                                            <a href="candidate-list.html" class="nav-link">Candidate List</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="candidate-details.html" class="nav-link">Candidate Details</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="single-resume.html" class="nav-link">Single Resume</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="submit-resume.html" class="nav-link">Submit Resume</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="pricing.html" class="nav-link">Pricing</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="dashboard.html" class="nav-link">Candidate dashboard</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link dropdown-toggle">Pages <i class="icofont-simple-down"></i></a>
                                    <ul class="dropdown-menu">
                                        <li class="nav-item">
                                            <a href="company-list.html" class="nav-link">Company List</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="company-details.html" class="nav-link">Company Details</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="login.html" class="nav-link">Login Page</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="create-account.html" class="nav-link">Create Account Page</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="profile.html" class="nav-link">Profile</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="single-profile.html" class="nav-link">Single Profile</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="404.html" class="nav-link">404</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="faq.html" class="nav-link">FAQ</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="terms-and-conditions.html" class="nav-link">Terms and Conditions</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="privacy-policy.html" class="nav-link">Privacy Policy</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link dropdown-toggle">Blogs <i class="icofont-simple-down"></i></a>
                                    <ul class="dropdown-menu">
                                        <li class="nav-item">
                                            <a href="blog.html" class="nav-link">Blog</a>
                                        </li>
                                        <li class="nav-item">
                                            <a href="blog-details.html" class="nav-link">Blog Details</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="nav-item">
                                    <a href="contact.html" class="nav-link">Contact</a>
                                </li>
                            </ul>
                            <div class="common-btn">
                                <a class="login-btn" href="{{ route('login') }}">
                                    <i class="icofont-plus-square"></i>
                                    Login
                                </a>
                                <a class="sign-up-btn" href="{{ route('register') }}">
                                    <i class="icofont-user-alt-4"></i>
                                    Sign Up
                                </a>
                            </div>
                        </div>
                    </nav>
                </div>
            </div>
        </div>