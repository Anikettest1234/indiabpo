<!-- Essential JS -->
        <script src="{{ URL::to('public/assets/js/jquery.min.js') }}"></script>
        <script src="{{ URL::to('public/assets/js/popper.min.js') }}"></script>
        <script src="{{ URL::to('public/assets/js/bootstrap.min.js') }}"></script>
        <!-- Meanmenu JS -->
        <script src="{{ URL::to('public/assets/js/jquery.meanmenu.js') }}"></script>
        <!-- Nice Select JS -->
        <script src="{{ URL::to('public/assets/js/jquery.nice-select.min.js') }}"></script>
        <!-- Mixitup JS -->
        <script src="{{ URL::to('public/assets/js/jquery.mixitup.min.js') }}"></script>
        <!-- Counter JS -->
        <script src="{{ URL::to('public/assets/js/waypoints.min.js') }}"></script>
        <script src="{{ URL::to('public/assets/js/jquery.counterup.min.js') }}"></script>
        <!-- Owl Carousel JS -->
        <script src="{{ URL::to('public/assets/js/owl.carousel.min.js') }}"></script>
        <!-- Form Ajaxchimp JS -->
        <script src="{{ URL::to('public/assets/js/jquery.ajaxchimp.min.js') }}"></script>
        <!-- Form Validator JS -->
        <script src="{{ URL::to('public/assets/js/form-validator.min.js') }}"></script>
        <!-- Contact JS -->
        <script src="{{ URL::to('public/assets/js/contact-form-script.js') }}"></script>
        <!-- Wow JS -->
        <script src="{{ URL::to('public/assets/js/wow.min.js') }}"></script>
        <!-- Custom JS -->
        <script src="{{ URL::to('public/assets/js/custom.js') }}"></script>


<script>
 $( function() {
    $('.toggle1').click(function(){
        //alert();
          var has = jQuery(this);
          $(this).parent().next().slideToggle();
         // console.log($(this).parent().next());
          if(has.hasClass('collapsed')){
                 
                 
                 jQuery(this).find('i').removeClass('icofont-minus');
              jQuery(this).find('i').addClass('icofont-plus');
                 $(this).removeClass('collapsed');
                //$(this).parent().next().attr('style','display: none !important');
          }
          else{
              $(this).addClass('collapsed');
              jQuery(this).find('i').removeClass('icofont-plus');
                 jQuery(this).find('i').addClass('icofont-minus');
              //$(this).parent().next().attr('style','display: block !important');
          }
    });
   });
</script>