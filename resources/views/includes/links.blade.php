<!-- Bootstrap CSS -->
        <link rel="stylesheet" href="{{ URL::to('public/assets/css/bootstrap.min.css') }}">
        <!-- Meanmenu CSS -->
        <link rel="stylesheet" href="{{ URL::to('public/assets/css/meanmenu.css') }}">
        <!-- Icofont -->
        <link rel="stylesheet" href="{{ URL::to('public/assets/css/icofont.min.css') }}">
        <!-- Nice Select JS -->
        <link rel="stylesheet" href="{{ URL::to('public/assets/css/nice-select.css') }}">
        <!-- Owl Carousel CSS -->
        <link rel="stylesheet" href="{{ URL::to('public/assets/css/owl.carousel.min.css') }}">
        <link rel="stylesheet" href="{{ URL::to('public/assets/css/owl.theme.default.min.css') }}">
        <!-- Animate CSS -->
        <link rel="stylesheet" href="{{ URL::to('public/assets/css/animate.css') }}">
        <!-- Flaticon CSS -->
        <link rel="stylesheet" href="{{ URL::to('public/assets/fonts/flaticon.css') }}">
        <!--font-awesome-->
        <link href="{{ URL::to('public/assets/css/font-awesome.min.css') }}" type="text/css" rel="stylesheet" media="all"/>
        <!-- Style CSS -->
        <link rel="stylesheet" href="{{ URL::to('public/assets/css/style.css') }}">
        <!-- Responsive CSS -->
        <link rel="stylesheet" href="{{ URL::to('public/assets/css/responsive.css') }}">

        <title>Home Page</title>

        <link rel="icon" type="image/png" href="{{ URL::to('public/assets/img/favicon.png') }}">