@extends('layouts.app')

@section('title', 'Forentry | Job Seeker Profile')

@section('style')
@include('includes.style')
<style type="text/css">
    .success-div{
        margin: 100px auto 200px;
        text-align: center;
    }
    .success-div-inner{
        background: #eee;
        width: 50%;
        margin: 0 auto;
        padding: 30px;
        box-shadow: 0px 0px 7px 2px #cacaca;
    }
    .success-div-inner i{
        font-size: 83px;
        color: #0075b9;

    }
    .success-div-inner h3{
        margin-top: 20px;
    }
    .success-div-inner h5{
        margin-top: 15px;
    }

</style>
@endsection

@section('header')
@include('includes.header')
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-xl-812 col-lg-12">
            <div class="success-div">
                <div class="success-div-inner">
                    <i class="fa fa-check-circle"></i>
                    <h3>Registration successful.</h3>
                    <h5>A verification link has been sent to your registered email address. Please verify you email.</h5>
                    {{-- <h5></h5> --}}
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('footer')
@include('includes.footer')
@endsection

@section('script')
@include('includes.script')

@endsection