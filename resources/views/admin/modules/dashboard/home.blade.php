@extends('admin.layouts.app')
@section('title', 'IndiaBpo | Admin | Dashboard')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<script>
window.onload = function () {
    var jobseeker  = [];
   
    var employer  = [];
    
    console.log(jobseeker)
    console.log(jobseeker)
    var chart = new CanvasJS.Chart("chartContainer", {
        animationEnabled: true,
        title:{
            text: "User Register in Last 7 Days"
        },
        axisX: {
            valueFormatString: "DD MMM,YY"
        },
        legend:{
            cursor: "pointer",
            fontSize: 16,
            itemclick: toggleDataSeries
        },
        toolTip:{
            shared: true
        },
        data: [{
            name: "Job Seeker",
            type: "spline",
            yValueFormatString: "###",
            showInLegend: true,
            dataPoints: jobseeker
        },
        {
            name: "Employer",
            type: "spline",
            yValueFormatString: "###",
            showInLegend: true,
            dataPoints: employer
        }]
    });
    chart.render();

    function toggleDataSeries(e){
        if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
            e.dataSeries.visible = false;
        }
        else{
            e.dataSeries.visible = true;
        }
        chart.render();
    }

    var chart = new CanvasJS.Chart("donutChart", {
        animationEnabled: true,
        data: [{
            type: "doughnut",
            startAngle: 60,
            //innerRadius: 60,
            indexLabelFontSize: 17,
            indexLabel: "{label} - #percent%",
            toolTipContent: "<b>{label}:</b> {y} (#percent%)",
            dataPoints: [
                { y: 1, label: "Job Seeker" },
                { y: 2, label: "Employer" }
            ]
        }]
    });
    chart.render();

}
</script>
<!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->                      
            <div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Dashboard</h4>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="content">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-4 col-sm-4 col-lg-4">
                                        <div class="mini-stat clearfix bx-shadow bg-white">
                                            <span class="mini-stat-icon bg-warning"><i class="fa fa-id-card"></i></span>
                                            <div class="mini-stat-info text-right text-dark">
                                                <span class="counter text-dark">0</span>
                                                Total no of employers
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-lg-4">
                                        <div class="mini-stat clearfix bx-shadow bg-white">
                                            <span class="mini-stat-icon bg-warning"><i class="fa fa-users"></i></span>
                                            <div class="mini-stat-info text-right text-dark">
                                                <span class="counter text-dark">0</span>
                                                Total no of job seekers
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-lg-4">
                                        <div class="mini-stat clearfix bx-shadow bg-white">
                                            <span class="mini-stat-icon bg-warning"><i class="fa fa-building-o"></i></span>
                                            <div class="mini-stat-info text-right text-dark">
                                                <span class="counter text-dark">0</span>
                                                Total no of companies
                                            </div>
                                        </div>
                                    </div>
                                   {{--  <div class="col-md-4 col-sm-4 col-lg-4">
                                        <div class="mini-stat clearfix bx-shadow bg-white">
                                            <span class="mini-stat-icon bg-warning"><i class="fa fa-book fa-fw"></i></span>
                                            <div class="mini-stat-info text-right text-dark">
                                                <span class="counter text-dark"></span>
                                                Total no of jobs
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-lg-4">
                                        <div class="mini-stat clearfix bx-shadow bg-white">
                                            <span class="mini-stat-icon bg-warning"><i class="fa fa-certificate"></i></span>
                                            <div class="mini-stat-info text-right text-dark">
                                                <span class="counter text-dark"></span>
                                                Total no of Trainings
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-lg-4">
                                        <div class="mini-stat clearfix bx-shadow bg-white">
                                            <span class="mini-stat-icon bg-warning"><i class="fa fa-newspaper-o"></i></span>
                                            <div class="mini-stat-info text-right text-dark">
                                                <span class="counter text-dark"></span>
                                                Total no of ads
                                            </div>
                                        </div>
                                    </div> --}}
                                </div>
                                
                                <div class="clearfix"></div>
                                <div class="row">
                                    <!-- ============================================================== -->
                                    <!-- recent orders  -->
                                    <!-- ============================================================== -->
                                    
                                    <!-- ============================================================== -->
                                    <!-- end recent orders  -->
                                    <!-- ============================================================== -->
                                </div>
                            </div>
                            <!-- container -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- container -->
    </div>
    <!-- content -->
    {{-- <footer class="footer text-right">
        {{ date('Y') }} &copy; NearO.in
    </footer> --}}
</div>

<!--Chart js-->
<script type="text/javascript" src="adminassets/assets/plugins/charts/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="adminassets/assets/plugins/charts/jquery.canvasjs.min.js"></script>
            <!-- ============================================================== -->
            <!-- End Right content here -->
            <!-- ============================================================== -->
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection

@section('scripts')
@include('admin.includes.scripts')
@endsection


