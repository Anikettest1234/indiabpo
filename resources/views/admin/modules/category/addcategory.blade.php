@extends('admin.layouts.app')
@section('title', env('APP_NAME', 'IndiaBpo').' | Admin | Category')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Add Categories</h4>
                    <div class="submit-login no_mmg pull-right">
                        <a href="{{route('admin.categories') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body table-rep-plugin">
                            <div class="row top_from">
                                @include('includes.message')
                                <div class="card-body">
                                    <form id="categories_form" action="{{ route('admin.addcategories') }}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        <div class="row">
                                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                <label for="input-select" class="col-form-label">Parent Category <small>( Leave this if it Parent Category )</small></label>
                                                <select class="form-control" id="input-select" name="parent_id">
                                                    <option value="0">Select Category</option>
                                                    @foreach($categories as $row)
                                                    <option value="{{ @$row->id }}">
                                                        {{ @$row->name }}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                <label for="inputText3" class="col-form-label">Category Name *</label>
                                                <input id="inputText3" name="name" type="text" required="" class="form-control" placeholder="Category Name ">
                                            </div>
                                            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <label for="exampleFormControlTextarea1">Description * </label>
                                                <textarea class="form-control" required="" name="description" id="exampleFormControlTextarea1" rows="3"></textarea>
                                            </div>
                                            
                                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                <!-- <p for="" class="col-form-label">Category Picture</p> -->
                                                <input type="file" accept="image/*" name="image" class="custom-file-input inpt" id="customFile">
                                                <label class="custom-file-label extrlft" for="customFile">Upload Category Picture</label>
                                                <span class="uploadButton-file-name">Use 64x64px size for better view.</span>
                                            </div>

                                            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <label for="inputPassword" class="col-form-label"></label>
                                                <button type="submit" name="Submit" class="btn btn-primary">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection

@section('scripts')
@include('admin.includes.scripts')

<script type="text/javascript">
    $(document).ready(function() {
        console.log('adsfsdf')
        $('#categories_form').validate();
    });
</script>
@endsection
