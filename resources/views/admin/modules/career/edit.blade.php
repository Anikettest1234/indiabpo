@extends('admin.layouts.app')
@section('title', env('APP_NAME', 'Forentry').' | Admin | Career')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Edit Career</h4>
                    <div class="submit-login no_mmg pull-right">
                        <a href="{{route('admin.career') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body table-rep-plugin">
                            <div class="row top_from">
                                @include('includes.message')
                                <div class="card-body">
                                    <form id="categories_form" action="{{ route('admin.career.update') }}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        <input type="hidden" name="id" value="{{ @$career->id }}">
                                        <div class="row">
                                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                <label for="inputText3" class="col-form-label">Title *</label>
                                                <input id="inputText3" name="title" type="text" required="" class="form-control" placeholder="Enter Career Title " value="{{ @$career->title }}">
                                            </div>
                                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                <label for="input-select" class="col-form-label">Category </label>
                                                <select class="form-control" id="input-select" name="category_id">
                                                    <option value="0">Select Category</option>
                                                    @foreach($category as $row)
                                                    <option value="{{ @$row->id }}" @if($row->id==$career->category_id) selected="" @endif>
                                                        {{ @$row->name }}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                <label for="inputText3" class="col-form-label">Location *</label>
                                                <input id="inputText3" name="location" type="text" required="" class="form-control" placeholder="Enter location" value="{{ @$career->location }}">
                                            </div>
                                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                <label for="inputText3" class="col-form-label">Year *</label>
                                                <input id="inputText3" name="year" type="text" required="" class="form-control" placeholder="Enter Year" value="{{ @$career->year }}">
                                            </div>
                                            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <label for="description">Description * </label>
                                                <textarea class="form-control" required="" name="description" id="description" rows="3">{{ @$career->description }}</textarea>
                                            </div>
                                            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <label for="inputPassword" class="col-form-label"></label>
                                                <button type="submit" name="Submit" class="btn btn-primary">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection

@section('scripts')
@include('admin.includes.scripts')

<script type="text/javascript">
    $(document).ready(function() {
        console.log('adsfsdf')
        $('#categories_form').validate();
    });
</script>
@endsection
