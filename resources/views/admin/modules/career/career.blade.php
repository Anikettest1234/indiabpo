@extends('admin.layouts.app')
@section('title', env('APP_NAME', 'Forentry').' | Admin | Career')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Career List</h4>
                    <div class="submit-login no_mmg pull-right">
                        {{-- <a href="{{route('admin.add.categories') }}" title="Add"><button type="button" class="btn btn-default">Add</button></a> --}}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body table-rep-plugin">
                            <div class="row top_from">
                                <div class="col-lg-8 col-md-8 col-sm-9 col-xs-12 npdd">
                                    <div class="dess5">
                                        <i class="fa fa-pencil-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Edit</span></i>
                                        <i class="fa fa-trash-o cncl" aria-hidden="true"> <span class="cncl_oopo">Delete</span></i>
                                        <i class="fa fa-check cncl" aria-hidden="true"> <span class="cncl_oopo">Active</span></i>
                                        <i class="fa fa-times cncl" aria-hidden="true" style="border: none;"> <span class="cncl_oopo">Inactive</span>
                                        </i>
                                    </div>
                                </div>
                                <div class="submit-login no_mmg pull-right">
                                    <a href="{{ route('admin.career.add') }}" title="add"><button type="button" class="btn btn-default">+ Add</button></a>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                        <table id="datatable" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    <th>Title</th>
                                                    <th>Description</th>
                                                    <th>Category</th>
                                                    <th>Location</th>
                                                    <th>Year</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach(@$career as $row)
                                                <tr>
                                                    <td>{{ @$row->title }}</td>
                                                    <td>{{ @$row->description }}</td>
                                                    <td>{{ @$row->category->name }}</td>
                                                    <td>{{ @$row->location }}</td>
                                                    <td>{{ @$row->year }}</td>
                                                    <td>
                                                        @if(@$row->status == 'A') Active
                                                        @else Inactive
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <a href="{{ route('admin.career.edit',[@$row->id]) }}"> <i class="fa fa-pencil-square-o cncl" title="Edit"> </i> </a>
                                                        @if(@$row->status == 'I')
                                                        <a href="{{ route('admin.career.change',[@$row->id]) }}"> <i class="fa fa-check cncl" title="Active"> </i> </a>
                                                        @else
                                                        <a href="{{ route('admin.career.change',[@$row->id]) }}"> <i class="fa fa-times cncl" title="Inactive"> </i> </a>
                                                        @endif
                                                        <a href="{{ route('admin.career.delete',[@$row->id]) }}" onclick="return confirm('Do you really want to delete this career?')"> <i class="fa fa-trash-o cncl" title="Delete" style="border: none;"> </i> </a>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection
@section('scripts')
@include('admin.includes.scripts')
@endsection