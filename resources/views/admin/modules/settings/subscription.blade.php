@extends('admin.layouts.app')
@section('title', env('APP_NAME', 'Forentry').' | Admin | Package')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Subscription Settings</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body table-rep-plugin">
                            <div class="row top_from">
                                @include('includes.message')
                                <div class="card-body">
                                    <form id="subscription_form" action="{{ route('admin.update.subscription') }}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        <div class="row">
                                            @foreach(@$package as $key=>$row)
                                            <div class="col-md-4 col-lg-4 col-md-12 col-sm-12">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading"><h3>{{ @$row->plan_name }}</h3></div>
                                                    <div class="panel-body">
                                                        <div class="row">
                                                            <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-10 col-10">
                                                                <label for="inputText3" class="col-form-label">Ad Post Limit *</label>
                                                                <input id="ad_post_limit" name="ad_post_limit[{{ $key }}]" type="text" required="" class="form-control" placeholder="Enter Amount " value="{{ @$row->ad_post_limit }}">
                                                            </div>
                                                            <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-10 col-10">
                                                                <label for="exampleFormControlTextarea1">Ad expiry in Days * </label>
                                                                <input id="ad_expiry_in" name="ad_expiry_in[{{ $key }}]" type="text" required="" class="form-control" placeholder="Enter Days " value="{{ @$row->ad_expiry_in }}">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="panel-heading"><h4>Featured</h4></div>
                                                            <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-10 col-10">
                                                                <label for="inputText3" class="col-form-label">Amount *($)</label>
                                                                <input id="featured_ad_fee" name="featured_ad_fee[{{ $key }}]" type="text" required="" class="form-control" placeholder="Enter Amount " value="{{ @$row->featured_ad_fee }}">
                                                            </div>
                                                            <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-10 col-10">
                                                                <label for="exampleFormControlTextarea1">Days * </label>
                                                                <input id="featured_ad_days" name="featured_ad_days[{{ $key }}]" type="text" required="" class="form-control" placeholder="Enter Days " value="{{ @$row->featured_ad_days }}">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="panel-heading"><h4>Urgent</h4></div>
                                                            <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-10 col-10">
                                                                <label for="inputText3" class="col-form-label">Amount *($)</label>
                                                                <input id="urgent_ad_fee" name="urgent_ad_fee[{{ $key }}]" type="text" required="" class="form-control" placeholder="Enter Amount " value="{{ @$row->urgent_ad_fee }}">
                                                            </div>
                                                            <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-10 col-10">
                                                                <label for="exampleFormControlTextarea1">Days * </label>
                                                                <input id="urgent_ad_days" name="urgent_ad_days[{{ $key }}]" type="text" required="" class="form-control" placeholder="Enter Days " value="{{ @$row->urgent_ad_days }}">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="panel-heading"><h4>Highlight</h4></div>
                                                            <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-10 col-10">
                                                                <label for="inputText3" class="col-form-label">Amount *($)</label>
                                                                <input id="highlight_ad_fee" name="highlight_ad_fee[{{ $key }}]" type="text" required="" class="form-control" placeholder="Enter Amount " value="{{ @$row->highlight_ad_fee }}">
                                                            </div>
                                                            <div class="form-group col-xl-6 col-lg-6 col-md-6 col-sm-10 col-10">
                                                                <label for="exampleFormControlTextarea1">Days * </label>
                                                                <input id="highlight_ad_days" name="highlight_ad_days[{{ $key }}]" type="text" required="" class="form-control" placeholder="Enter Days " value="{{ @$row->highlight_ad_days }}">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                                <div class="form-check">
                                                                    <input type="checkbox" value="Y" name="show_top_search_result_cat[{{ $key }}]" @if(@$row->show_top_search_result_cat=='Y') checked @endif class="form-check-input" id="exampleCheck1">
                                                                    <label class="form-check-label" for="exampleCheck1">Top in search results and category</label>
                                                                </div>
                                                                <div class="form-check">
                                                                    <input type="checkbox" value="Y" name="show_ad_on_home_premium[{{ $key }}]" @if(@$row->show_ad_on_home_premium=='Y') checked @endif class="form-check-input" id="exampleCheck1">
                                                                    <label class="form-check-label" for="exampleCheck1">Show ad on home page premium ad section</label>
                                                                </div>
                                                                <div class="form-check">
                                                                    <input type="checkbox" value="Y" name="show_add_search_home[{{ $key }}]" @if(@$row->show_add_search_home=='Y') checked @endif class="form-check-input" id="exampleCheck1">
                                                                    <label class="form-check-label" for="exampleCheck1">Show ad on home page search result list</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                                <label for="inputText3" class="col-form-label"><h4>Total Plan Amount *($)</h4></label>
                                                                <input id="plan_price" name="plan_price[{{ $key }}]" type="text" required="" class="form-control" placeholder="Enter Amount " value="{{ @$row->plan_price }}">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                        </div>
                                        
                                        <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <label for="inputPassword" class="col-form-label"></label>
                                            <button type="submit" name="Submit" class="btn btn-primary" onclick="return confirm('Do you really want to change this setting?')">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection

@section('scripts')
@include('admin.includes.scripts')

<script type="text/javascript">
    $(document).ready(function() {
        console.log('adsfsdf')
        $('#subscription_form').validate();
    });
</script>
@endsection
