@extends('admin.layouts.app')
@section('title', env('APP_NAME', 'Forentry').' | Admin | Category')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Premium Job Settings</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body table-rep-plugin">
                            <div class="row top_from">
                                @include('includes.message')
                                <div class="card-body">
                                    <form id="premium_form" action="{{ route('admin.update.premium') }}" method="post" enctype="multipart/form-data">
                                        @csrf
                                        <div class="panel panel-default">
                                            <div class="panel-heading"><h4>Featured</h4></div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-10 col-10">
                                                        <label for="inputText3" class="col-form-label">Amount *($)</label>
                                                        <input id="featured_amount" name="featured_amount" type="text" required="" class="form-control" placeholder="Enter Amount " value="{{ @$premium->featured_amount }}">
                                                    </div>
                                                    <div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-10 col-10">
                                                        <label for="exampleFormControlTextarea1">Days * </label>
                                                        <input id="featured_days" name="featured_days" type="text" required="" class="form-control" placeholder="Enter Days " value="{{ @$premium->featured_days }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading"><h4>Urgent</h4></div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-10 col-10">
                                                        <label for="inputText3" class="col-form-label">Amount *($)</label>
                                                        <input id="urgent_amount" name="urgent_amount" type="text" required="" class="form-control" placeholder="Enter Amount " value="{{ @$premium->urgent_amount }}">
                                                    </div>
                                                    <div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-10 col-10">
                                                        <label for="exampleFormControlTextarea1">Days * </label>
                                                        <input id="urgent_days" name="urgent_days" type="text" required="" class="form-control" placeholder="Enter Days " value="{{ @$premium->urgent_days }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading"><h4>Highlight</h4></div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-10 col-10">
                                                        <label for="inputText3" class="col-form-label">Amount *($)</label>
                                                        <input id="highlight_amount" name="highlight_amount" type="text" required="" class="form-control" placeholder="Enter Amount " value="{{ @$premium->highlight_amount }}">
                                                    </div>
                                                    <div class="form-group col-xl-4 col-lg-4 col-md-4 col-sm-10 col-10">
                                                        <label for="exampleFormControlTextarea1">Days * </label>
                                                        <input id="highlight_days" name="highlight_days" type="text" required="" class="form-control" placeholder="Enter Days " value="{{ @$premium->highlight_days }}">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <label for="inputPassword" class="col-form-label"></label>
                                            <button type="submit" name="Submit" class="btn btn-primary" onclick="return confirm('Do you really want to change this setting?')">Save</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection

@section('scripts')
@include('admin.includes.scripts')

<script type="text/javascript">
    $(document).ready(function() {
        console.log('adsfsdf')
        $('#premium_form').validate();
    });
</script>
@endsection
