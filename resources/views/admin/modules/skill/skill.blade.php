@extends('admin.layouts.app')
@section('title', env('APP_NAME', 'IndiaBpo').' | Admin | Skills')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Skills List</h4>
                    <div class="submit-login no_mmg pull-right">
                        {{-- <a href="{{route('admin.add.categories') }}" title="Add"><button type="button" class="btn btn-default">Add</button></a> --}}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body table-rep-plugin">
                            <div class="row top_from">
                                <div class="col-lg-8 col-md-8 col-sm-9 col-xs-12 npdd">
                                   <div class="dess5">
                                      <i class="fa fa-pencil-square-o cncl" aria-hidden="true"> <span class="cncl_oopo">Edit</span></i>
                                      <i class="fa fa-trash-o cncl" aria-hidden="true"> <span class="cncl_oopo">Delete</span></i>
                                      <i class="fa fa-check cncl" aria-hidden="true"> <span class="cncl_oopo">Active</span></i>
                                      <i class="fa fa-times cncl" aria-hidden="true" style="border: none;"> <span class="cncl_oopo">Inactive</span>
                                      </i>
                                   </div>
                                </div>
                                <div class="submit-login no_mmg pull-right">
                                    <a href="{{ route('admin.add.skills') }}" title="add"><button type="button" class="btn btn-default">+ Add</button></a>
                                </div>
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <div class="table-responsive" data-pattern="priority-columns">
                                        <table id="datatable" class="table table-striped table-bordered">
                                            <thead>
                                                <tr>
                                                    
                                                    <th>Skill</th>
                                                    <th>Status</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                              @foreach(@$skills as $row)
                                                <tr>
                                                  <td>{{ $row->skill }}</td>
                                                  <td>
                                                    @if(@$row->status == 'A') Active
                                                    @else Inactive
                                                    @endif
                                                  </td>
                                                  <td>
                                                    @if(@$row->status == 'I')
                                                    <a href="{{ route('admin.change.skills',[@$row->id]) }}"> <i class="fa fa-check cncl" title="Active"> </i> </a>
                                                    @else
                                                    <a href="{{ route('admin.change.skills',[@$row->id]) }}"> <i class="fa fa-times cncl" title="Inactive"> </i> </a>
                                                    @endif
                                                    <a href="{{ route('admin.edit.skills',[@$row->id]) }}"> <i class="fa fa-pencil-square-o cncl" title="Edit"> </i> </a>
                                                    <a href="{{ route('admin.delete.skills',[@$row->id]) }}" onclick="return confirm('Do you really want to delete this skill?')"> <i class="fa fa-trash-o cncl" title="Delete" style="border: none;"> </i> </a>
                                                  </td>
                                                </tr>
                                              @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection

@section('scripts')
@include('admin.includes.scripts')
<script>  
  function is_show(row_id){
    var status = $('#is_show'+row_id).val();
    //alert(status);
   
    if(status == 0) {
      var msg = 'Do you want to show the category image?';
    } else {
      var msg = 'Do you want to hide the category image?';
    }
    if(confirm(msg)) {      
      $.ajax({
        type:"post",
        {{-- url:"{{route('is.show')}}", --}}
        data:{
          'category_id': row_id,
          '_token': '{{csrf_token()}}' },
        success:function(res){
          if(res=='1'){
            $('#is_show'+row_id).prop('checked',true);
            $('#is_show'+row_id).val('1');
          } else if(res=='0'){
            $('#is_show'+row_id).prop('checked',false);
            $('#is_show'+row_id).val('0');
          }
        }
      });
    } else {
      if(status=='0'){
        $('#is_show'+row_id).prop('checked',false);
      } else if(status=='1'){
        $('#is_show'+row_id).prop('checked',true);
      }
      //return false;
    }
    
  }



  function categoryImgHeader(row_id){
    if($('.removeCategoryImg_'+row_id).prop("checked") == false)
    {
        $.ajax({
                type:"post",
                {{-- url:"{{route('category.img.header.delete')}}", --}}
                data:{
                  '_token': '{{csrf_token()}}',
                  'category_id': row_id
                },
                success:function(res){
                    toastr.success('Successfully removed from category header.');
                    $('#new_arrivals'+row_id).prop('checked',false);
                    $('.soumen_'+row_id).hide();
                   /* console.log(res);
                  if(res.status=='F'){
                    $('#myModal2').modal('show');
                    $('.product_id').val(row_id);
                  } else if(res.status=='T'){
                    toastr.error('You can not add more than three category images in header.');
                     $('#new_arrivals'+row_id).prop('checked',false);
                  }*/
                }
        });

    } else {
        $.ajax({
                type:"post",
                {{-- url:"{{route('category.img.header.count')}}", --}}
                data:{
                  '_token': '{{csrf_token()}}' },
                success:function(res){
                    console.log(res);
                  if(res.status=='F'){
                    $('#myModal2').modal('show');
                    $('.product_id').val(row_id);
                  } else if(res.status=='T'){
                    toastr.error('You can not add more than three category images in header.');
                    $('#new_arrivals'+row_id).prop('checked',false);
                   
                  }
                }
        });
    }
    $('body').on('click','.close',function(){
        $('#new_arrivals'+row_id).prop('checked',false);
    });
    $('#myModal2').click(function(){
         $('#new_arrivals'+row_id).prop('checked',false);
    });

    
}
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.category_header').click(function(){
            var category_id = $(this).data('id');
            $.ajax({
                type:"post",
                {{-- url:"{{route('category.img.header.edit')}}", --}}
                data:{
                  'category_id': category_id,
                  '_token': '{{csrf_token()}}' 
                },
                success:function(res){
                    $('#myModal4').modal('show');
                    $('.category_id').val(category_id);
                    $('#output4').attr('src', 'adminassets/uploads/category_images/category_img_header/'+res.category.category_header_img) .width('250px');
                    // console.log(res);
                }
            });
        });
    });
</script>
<script>
  var loadFile = function(event) {
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('output3');
      output.style.width ='250px';
      output.src = reader.result;
    };
    reader.readAsDataURL(event.target.files[0]);
  };
</script>
<script>
  var loadFile5 = function(event) {
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('output4');
      output.style.width ='250px';
      output.src = reader.result;
    };
    reader.readAsDataURL(event.target.files[0]);
  };
</script>
@endsection