@extends('admin.layouts.app')
@section('title', env('APP_NAME', 'IndiaBpo').' | Admin | Edit skills')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<div class="content-page">
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Edit skills</h4>
                    <div class="submit-login no_mmg pull-right">
                        <a href="{{route('admin.skills') }}" title="Back"><button type="button" class="btn btn-default">Back</button></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body table-rep-plugin">
                            <div class="row top_from">
                                @include('includes.message')
                                <div class="card-body">
                                    <form enctype="multipart/form-data" id="categories_form" action="{{ route('admin.update.skills') }}" method="post">
                                        @csrf
                                        <input type="hidden" name="id" value="{{ $skill->id }}">
                                        <div class="row">
                                           
                                            <div class="form-group col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                                <label for="inputText3" class="col-form-label">Skill Name </label>
                                                <input id="inputText3" name="skill" value="{{ $skill->skill }}" type="text" required="" class="form-control" placeholder="Category Name ">
                                            </div>
                                            
                                           
                                          
                                            <div class="form-group col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <label for="inputPassword" class="col-form-label"></label>
                                                <button type="submit" name="Submit" class="btn btn-primary">Save</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection

@section('scripts')
@include('admin.includes.scripts')

<script type="text/javascript">
    $(document).ready(function() {
        console.log('adsfsdf')
        $('#categories_form').validate();
    });
</script>
@endsection
