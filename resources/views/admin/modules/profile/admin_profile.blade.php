@extends('admin.layouts.app')
@section('title', 'IndiaBpo | Admin | Profile')
@section('header')
@include('admin.includes.header')
@endsection
@section('sidebar')
@include('admin.includes.sidebar')
@endsection
@section('content')
<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->     
<style type="text/css">
    .image{
        margin-top: 35px;
    }
</style>                 
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="pull-left page-title">Admin Profile</h4>
                    <div class="submit-login no_mmg pull-right">  
                        <a href="{{route('admin.dashboard')}}" ><button type="button" class="btn btn-default">Back</button></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-body table-rep-plugin">
                            <div class="row">
                                <form id="myform" method="post" action="{{ route('admin.change.password') }}" onsubmit="return validateForm()" enctype="multipart/form-data">
                                    @csrf
                                    <input type="hidden" name="id" value="{{ Auth::guard('admin')->user()->id }}">
                                    <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                        <div class="your-mail">
                                            <label for="exampleInputEmail1">Name</label>
                                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Name" value="{{ Auth::guard('admin')->user()->name }}" name="name">
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                        <div class="your-mail">
                                            <label for="exampleInputEmail2">Email</label>
                                            <input type="text" class="form-control" id="exampleInputEmail2" value="{{ Auth::guard('admin')->user()->email }}" name="email">
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                        <div class="your-mail">
                                            <label for="password">New Password</label>
                                            <input type="password" class="form-control" id="password" placeholder="New Password" name="password">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                        <div class="your-mail">
                                            <label for="exampleInputEmail4">Confirm Password</label>
                                            <input type="password" class="form-control" id="exampleInputEmail4" placeholder="Confirm Password" name="password_confirmation">
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                        <div class="image">
                                            
                                        <!-- <p for="" class="col-form-label">Category Picture</p> -->
                                        <input type="file" accept="image/*" name="image" class="custom-file-input inpt" id="customFile">
                                        <label class="custom-file-label extrlft" for="customFile">Upload Category Picture</label>
                                        <span class="uploadButton-file-name">Use 64x64px size for better view.</span>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                        <div class="image"></div>    
                                        <div class="m-r-10"><img src="{{ @Auth::guard('admin')->user()->image ? url('public/images/admin/'.Auth::guard('admin')->user()->image) : url('public/images/default.png') }}" alt="user" class="rounded" width="45"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-4 col-sm-4 col-xs-12 col-lg-4">
                                        <div class="submit-login no_mmg ppo">
                                            <button type="submit" class="btn btn-default">Save</button>
                                        </div>
                                    </div>
                                </form>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Row -->
        </div>
        <!-- container -->
    </div>
    <!-- content -->
    <!--<footer class="footer text-right">
        2015 © Moltran.
        </footer>-->
</div>
<!-- ============================================================== -->
<!-- End Right content here -->
<!-- ============================================================== -->

<style type="text/css">
    .error{
    color:red !important;
    }
</style>
@endsection
@section('footer')
@include('admin.includes.footer')
@endsection

@section('scripts')
@include('admin.includes.scripts')
<script type="text/javascript">
    function validateForm()
    {
        var pass = $('#password').val();
        if(pass) {
            $('#password').addClass('required');
            $('#exampleInputEmail4').addClass('required');
            $("#exampleInputEmail4").rules("add", {"equalTo" : "#password"});
            
        } else {
            $('#password').removeClass('required');
            $('#exampleInputEmail4').removeClass('required');
            $("#exampleInputEmail4").rules("remove", "equalTo");
            return true;
        }
        
        
    }
    $(document).ready(function () {

    $("#myform").validate({
        rules: {
            /*password: {
                required: true,
                minlength: 6
            },*/
            name: {
                required: true
            },
            email: {
                required: true,
                email: true
            }/*,
            password_confirmation: {
                required : true,
                minlength: 6,
                equalTo  : "#password" 
            }*/
        },
        messages: {
            /*password: {
                required: "Password field is required.",
                minlength: "Password should be at least six characters long."
            },
            password_confirmation: {
                required : "Confirm password field is required.",
                minlength: "Confirm password should be at least six characters long.",
                equalTo  : "Confirm password should be same as password."
            }*/
        },
        /*submitHandler: function (form) { 
            alert('valid form');  
            return false;  
        }*/
    });

});
</script>
@endsection