@extends('admin.layouts.app')
@section('title', 'Bouji | Admin | Forget Password')
@section('content')
<div class="wrapper-page">
    <div class="panel panel-color panel-primary panel-pages">
        <div class="panel-heading bg-img">
            <div class="bg-overlay"></div>
            <h3 class="text-center m-t-10 text-white"><img src="{{ url('storage/app/public/admin/logo.png') }}" width="130px;"></h3>
        </div>
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST" action="{{ route('admin.update.forget.password') }}" id="resetfrm1" name="resetfrm1">
                {{ csrf_field() }}
                {{-- <input type="hidden" name="token" value="{{ $token }}"> --}}
                {{-- <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <label for="email" class="col-md-4 control-label">E-Mail Address</label>
                    <div class="col-md-8">
                        <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" autofocus>
                        @if ($errors->has('email'))
                        <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                </div> --}}
                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <label for="password" class="col-md-4 control-label">Password</label>
                    <div class="col-md-8">
                        <input id="password" type="password" class="form-control" name="password">
                        @if ($errors->has('password'))
                        <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                    <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>
                    <div class="col-md-8">
                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                        @if ($errors->has('password_confirmation'))
                        <span class="help-block">
                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                        Reset Password
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@section('scripts')
@include('admin/includes/scripts')
<script type="text/javascript">
    $(document).ready(function () {
    $("#resetfrm1").validate({
        rules: {
            password: {
                required: true
            },
            password_confirmation: {
                required: true,
                equalTo: "#password"  
            }
        },
        messages: {
            password: {
                required: "Password field is required"
            },
            password_confirmation: {
                required: "Confirm password is required.",
                equalTo: "Confirm password should be same as password." 
            }
        },
        
    });
});
</script>
@endsection