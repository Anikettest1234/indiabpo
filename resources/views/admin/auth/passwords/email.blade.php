@extends('admin.layouts.app')
@section('title', 'Bouji | Admin | Reset Password')
@section('content')
<!-- Main Content -->
<div class="wrapper-page">
    <div class="panel panel-color panel-primary panel-pages">
        <div class="panel-heading bg-img">
            <div class="bg-overlay"></div>
            <h3 class="text-center m-t-10 text-white"><img src="{{ url('storage/app/public/admin/logo.png') }}" width="130px"></h3>
        </div>
        <div class="panel-body">
            @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
            @endif
            <form class="form-horizontal" role="form" method="POST" action="{{ route('admin.forget.password.link') }}" id="resetfrm" name="resetfrm">
                {{ csrf_field() }}
                <div class="form-group{{-- {{ $errors->has('email') ? ' has-error' : '' }} --}}">
                    <label for="email" class="col-md-4 control-label">E-Mail Address</label>
                    <div class="col-md-8">
                        <input id="email" type="email" class="form-control required" name="email" value="{{ old('email') }}">
                        @if (session('errors'))
                            <span class="help-block" style="color: red;">
                                {{ $errors->first('email') }}
                            </span>
                        @endif

                        @if (session()->has('error'))
                            <span class="help-block" style="color: red;">
                                
                                    {{ session()->get('error') }}
                            </span>
                        @endif

                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                        Send Password Reset Link
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#resetfrm').validate();
    })
</script>
@endsection