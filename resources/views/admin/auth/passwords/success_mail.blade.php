<!DOCTYPE html>
<html>
    <head>
        <title>Mail Send</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        @include('mail.layouts.header')
    </head>
    <body>
        <div class="wrapper-page">
            <div class="panel panel-color panel-primary panel-pages">
                <div class="panel-heading bg-img">
                    <div class="bg-overlay"></div>
                    {{-- <h3 class="text-center m-t-10 text-white"><img src="{{ url('storage/app/public/admin/logo.png') }}" width="130px;"></h3> --}}
                </div>
                <div class="panel-body" style="text-align: center;padding: 100px 50px;">
                    <h3>Mail has been successfully send....</h3>
                    <h4>Please go to your email box to get a link for forget password.</h4>
                </div>
            </div>
        </div>
    </body>
</html>
    