<!DOCTYPE html>
<html>
    <head>
        <title>Mail Expired</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        @include('mail.layouts.header')
    </head>
    <body>
        <div class="wrapper-page">
            <div class="panel panel-color panel-primary panel-pages">
                <div class="panel-heading bg-img">
                    <div class="bg-overlay"></div>
                </div>
                <<div class="panel-body" style="text-align: center;padding: 100px 50px;">
                    <h3>This link has been expired.</h3>
                    <h4>Please resend mail and recover your account.</h4>
                </div>
            </div>
        </div>
    </body>
</html>
    