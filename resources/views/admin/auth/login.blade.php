@extends('admin.layouts.app')
@section('title', 'Forentry | Admin | Login')
@section('content')
<div class="wrapper-page">
    <div class="panel panel-color panel-primary panel-pages">
        <div class="panel-heading bg-img"> 
            <div class="bg-overlay"></div>
            <h3 class="text-center m-t-10 text-white" style="color: #0ec70e; font-weight: 600; font-size: 40px;"><img src="{{URL::to('public/images/logo.png')}}"></h3>
        </div> 
        <div class="panel-body">
            @if ($errors->has('email'))
                <span class="help-block" style="text-align: center;" class="alert alert-danger">
                    <strong style="color: red;">{{ $errors->first('email') }}</strong>
                </span>
            @endif
            @if ($errors->has('password'))
                <span class="help-block" style="text-align: center;" class="alert alert-danger">
                    <strong style="color: red;">{{ $errors->first('password') }}</strong>
                </span>
            @endif
                <form id='loginfrm' name='loginfrm' class="form-horizontal m-t-20" role="form" method="POST" action="{{ url('/admin/login') }}">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="col-xs-12">
                            <input class="form-control input-lg required email" id="email" name="email" type="text"  placeholder="Email" value="{{ $data['admin_email'] == null ? old('email'): $data['admin_email']}}" autofocus>{{-- 
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif --}}
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <div class="col-xs-12">
                            <input class="form-control input-lg required" id="password" name="password" type="password"  placeholder="Password" value="{{ $data['admin_password'] }}" autofocus>
                            {{-- @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif --}}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-xs-12">
                            <div class="checkbox checkbox-primary">
                                <input id="checkbox-signup" type="checkbox" name="remember" @if($data['admin_email'] != null) checked @endif>
                                <label for="checkbox-signup">
                                    Remember me
                                </label>
                            </div>
                            
                        </div>
                    </div>
                    
                    <div class="form-group text-center m-t-40 submit-login">
                        <div class="col-xs-12">
                            <button class="btn btn-default" type="submit" style="background: #199c09 !important;">
                            Log In</button>
                        </div>
                    </div>

                    <div class="form-group m-t-30">
                        <div class="col-sm-7">
                            <a href="{{ route('admin.forget.password') }}"><i class="fa fa-lock m-r-5"></i> Forgot your password?</a>
                        </div>
                        <!--<div class="col-sm-5 text-right">
                            <a href="register.html">Create an account</a>
                        </div>-->
                    </div>
                </form> 
                </div>                                 
                
            </div>
        </div>


@endsection
@section('scripts')
@include('admin.includes.scripts')
<script type="text/javascript">
    $(document).ready(function(){
        $('#loginfrm').validate();
    })
</script>
@endsection