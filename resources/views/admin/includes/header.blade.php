<!-- Top Bar Start -->
<div class="topbar">
  <!-- LOGO -->
  <div class="topbar-left">
    <div class="text-center">
      <a href="{{ route('admin.dashboard') }}" class="logo"><img src="{{URL::to('public/images/logo.png')}}" class="img-responsive" alt=""></a>
    </div>
  </div>
  <!-- Button mobile view to collapse sidebar menu -->
  <div class="navbar navbar-default" role="navigation">
    <div class="container">
      <div class="">
        <div class="pull-left">
          <button class="button-menu-mobile open-left">
              <i class="fa fa-bars"></i>
          </button>
          <span class="clearfix"></span>
      </div>
    <!--<form class="navbar-form pull-left" role="search">
        <div class="form-group">
            <input type="text" class="form-control search-bar" placeholder="Type here for search...">
        </div>
        <button type="submit" class="btn btn-search"><i class="fa fa-search"></i></button>
    </form>-->
        <ul class="nav navbar-nav navbar-right pull-right">
          <li class="dropdown">
            <a href="#" class="dropdown-toggle profile" data-toggle="dropdown" aria-expanded="true"><img src="{{ @Auth::guard('admin')->user()->image ? url('public/images/admin/'.Auth::guard('admin')->user()->image) : url('public/images/default.png') }}" alt="user-img')}}" class="img-circle"></a>
            <ul class="dropdown-menu">
              <li><a href="{{ route('admin.change.password') }}"><i class="md md-face-unlock"></i> Profile</a></li>
              <li><a href="{{ route('admin.logout') }}"><i class="md md-settings-power"></i> Logout</a></li>
            </ul>
          </li>
        </ul>
      </div>
      <!--/.nav-collapse -->
    </div>
  </div>
</div>
<!-- Top Bar End -->