<!-- Scripts -->
{{-- <script src="{{URL::asset('public/adminassets/js/app.js') }}"></script> --}}


<script>
  window.Laravel = <?php echo json_encode([
  	'csrfToken' => csrf_token(),
  ]); ?>
</script>


<script>
            var resizefunc = [];
        </script>

        <!-- Main  -->
        
        <script src="{{URL::asset('public/adminassets/assets/js/jquery-ui.js')}}"></script>
        <script src="{{URL::asset('public/adminassets/assets/js/bootstrap.min.js')}}"></script>
        <script src="{{URL::asset('public/adminassets/assets/js/detect.js')}}"></script>
        <script src="{{URL::asset('public/adminassets/assets/js/fastclick.js')}}"></script>
        <script src="{{URL::asset('public/adminassets/assets/js/jquery.slimscroll.js')}}"></script>
        <script src="{{URL::asset('public/adminassets/assets/js/jquery.blockUI.js')}}"></script>
        <script src="{{URL::asset('public/adminassets/assets/js/waves.js')}}"></script>
        <script src="{{URL::asset('public/adminassets/assets/js/wow.min.js')}}"></script>
        <script src="{{URL::asset('public/adminassets/assets/js/jquery.nicescroll.js')}}"></script>
        <script src="{{URL::asset('public/adminassets/assets/js/jquery.scrollTo.min.js')}}"></script>

        <script src="{{URL::asset('public/adminassets/assets/js/jquery.toast.js')}}"></script>  
        <script src="{{URL::asset('public/adminassets/assets/js/jquery.validate.js')}}"></script>  

        <script src="{{URL::asset('public/adminassets/assets/js/jquery.app.js')}}"></script>

<script src="{{URL::asset('public/adminassets/assets/js/modernizr.min.js')}}"></script>
<script src="{{URL::asset('public/adminassets/assets/js/jquery.classybox.min.js')}}"></script>

<!-- Datatables-->
        <script src="{{URL::asset('public/adminassets/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
        <script src="{{URL::asset('public/adminassets/assets/plugins/datatables/dataTables.bootstrap.js')}}"></script>
        <script src="{{URL::asset('public/adminassets/assets/plugins/datatables/dataTables.buttons.min.js')}}"></script>
        <script src="{{URL::asset('public/adminassets/assets/plugins/datatables/buttons.bootstrap.min.js')}}"></script>
        <script src="{{URL::asset('public/adminassets/assets/plugins/datatables/jszip.min.js')}}"></script>
        <script src="{{URL::asset('public/adminassets/assets/plugins/datatables/pdfmake.min.js')}}"></script>
        <script src="{{URL::asset('public/adminassets/assets/plugins/datatables/vfs_fonts.js')}}"></script>
        <script src="{{URL::asset('public/adminassets/assets/plugins/datatables/buttons.html5.min.js')}}"></script>
        <script src="{{URL::asset('public/adminassets/assets/plugins/datatables/buttons.print.min.js')}}"></script>
        <script src="{{URL::asset('public/adminassets/assets/plugins/datatables/dataTables.fixedHeader.min.js')}}"></script>
        <script src="{{URL::asset('public/adminassets/assets/plugins/datatables/dataTables.keyTable.min.js')}}"></script>
        <script src="{{URL::asset('public/adminassets/assets/plugins/datatables/dataTables.responsive.min.js')}}"></script>
        <script src="{{URL::asset('public/adminassets/assets/plugins/datatables/responsive.bootstrap.min.js')}}"></script>
        <script src="{{URL::asset('public/adminassets/assets/plugins/datatables/dataTables.scroller.min.js')}}"></script>
        <script src="{{URL::asset('public/adminassets/assets/js/chosen.jquery.js')}}"></script>
        <script src="{{URL::asset('public/adminassets/assets/js/bootstrap-colorpicker.min.js')}}"></script>
        <!-- Datatable init js -->
        <script src="{{URL::asset('public/adminassets/assets/pages/datatables.init.js')}}"></script>
        <script src="{{URL::asset('public/adminassets/assets/js/jquery.classybox.min.js')}}"></script>
        {{-- <script src="{{URL::asset('public/adminassets/assets/js/jquery.app.js')}}"></script>
		<script src="{{URL::asset('public/adminassets/assets/js/jquery-ui.js')}}"></script> --}}
        <script>
            $(".color_picker").spectrum({
                change: function(color) {
                        var color = color.toHexString();
                        $('.color_picker').val(color);
                }
            });
        </script>
        <script type="text/javascript">
            $(document).ready(function() {
				
				
				$('#category').change(function(){
					var v=$(this).val();
					if(v==1){
						$('#cack').show();
						$('#candles').hide();
						$('#gifts').hide();
						$('#flowers').hide();
						$('#nnb_no').hide();
					} else if(v==2){
						$('#cack').hide();
						$('#candles').show();
						$('#gifts').hide();
						$('#flowers').hide();
						$('#nnb_no').show();
					} else if(v==3){
						$('#cack').hide();
						$('#candles').hide();
						$('#gifts').show();
						$('#flowers').hide();	
						$('#nnb_no').show();
					} else if(v==4){
					$('#cack').hide();
						$('#candles').hide();
						$('#gifts').hide();
						$('#flowers').show();
						$('#nnb_no').show();
					}
						
					
					});
				
				$('#datatable_length').hide();
                $('#datatable').dataTable({
					 "ordering": false,
        				"info":     false,
						"searching": false,
						 "dom": '<"toolbar">frtip'
					});

                $(".check_check").click(function(){
						if($(this).is(":checked"))
						{
							$('.ch_1').show();
						}
						else
						{
							$('.ch_1').hide();
						}
						});
				//$("div.toolbar").html('<div class="status">Status: <select name="" class="drop"><option value="1">Active</option><option value="1">Inactive</option></select></div><div class="clndr">Order From date : <input type="text" name="from" id="frm_date"> </div><div class="clndr">Order To date : <input type="text" name="to" id="to_date"> </div><div class="clndr">Keyword: <input type="text" name="abc"></div><div class="srch"><input type="submit" name="submit" value="Search"> </div>');	
               // $('#datatable-keytable').DataTable( { keys: true } );
               //$('#datatable-responsive').DataTable();
                //$('#datatable-scroller').DataTable( { ajax: "assets/plugins/datatables/json/scroller-demo.json", deferRender: true, scrollY: 380, scrollCollapse: true, scroller: true } );
                //var table = $('#datatable-fixed-header').DataTable( { fixedHeader: true } );
            } );
            TableManageButtons.init();
        </script>


{{-- <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-65046120-1', 'auto');
  ga('send', 'pageview');

</script> --}}


<script>
	$(function() {
		$("#frm_date").datepicker({dateFormat: "yy-mm-dd",
			changeMonth: true,
			changeYear: true,
			numberOfMonths: 1,
			defaultDate: new Date(),
			onClose: function( selectedDate ) {
				 $( "#to_date").datepicker( "option", "minDate", selectedDate );
			}
		});
		$("#to_date").datepicker({dateFormat: "yy-mm-dd",
			changeMonth: true,
			changeYear: true,
			numberOfMonths: 1,
			onClose: function( selectedDate ) {
					$( "#frm_date" ).datepicker( "option", "maxDate", selectedDate );
			}
		});
	});

	$(function() {
		$("#datepicker").datepicker({dateFormat: "yy-mm-dd",
			minDate : 0,
			changeMonth: true,
			changeYear: true,
			numberOfMonths: 1,
			defaultDate: new Date(),
			onClose: function( selectedDate ) {
				 $( "#datepicker1").datepicker( "option", "minDate", selectedDate );
			}
		});
		$("#datepicker1").datepicker({dateFormat: "yy-mm-dd",
			changeMonth: true,
			changeYear: true,
			numberOfMonths: 1,
			onClose: function( selectedDate ) {
					$( "#datepicker" ).datepicker( "option", "maxDate", selectedDate );
			}
		});
	});
</script>
{{-- <script>
	$(function() {
		$( "#datepicker" ).datepicker();
	});
</script>
<script>
	$(function() {
		$( "#datepicker1" ).datepicker();
	});
</script> --}}

<!--............................start popup script................................--> 
<script>
$(document).ready(function(e) {
    $('#upload').click(function(){
		$('.pop1').fadeIn('slow');
		$('.login_outer').fadeIn('slow');
		$('html, body').animate({
        scrollTop: ($('.custom_popup').offset().top - 85)+'px'
		}, 800);
		});
		$('.close,.login_outer').click(function(){
		$('.custom_popup').fadeOut('slow');
		$('.login_outer').fadeOut('slow');
		});
});
</script>

    <script>
    $(document).ready(function(){
		
	 $(".abb").click(function(){
		 
		 $('.lg_csl').click();
		 $('.pop2').fadeIn('slow');
		$('.login_outer').fadeIn('slow');
		//$('html, body').animate({
        //scrollTop: ($('.custom_popup1').offset().top - 85)+'px'
		//}, 800);

			
		});
		$(".abs2").click(function(){
			$('.lg_cl').click();
		 $('.pop1').fadeIn('slow');
		$('.login_outer').fadeIn('slow');
			
		});
		$(".abs3").click(function(){
			$('.lg_cl').click();
			$('.pop3').fadeIn('slow');
			$('.login_outer').fadeIn('slow');
			
		});
		
    });
    </script>
<script type="text/javascript">
    $(function() {
    // Multiple images preview in browser
    var imagesPreview = function(input, placeToInsertImagePreview) { 
        if (input.files) {
            var filesAmount = input.files.length;
            $('div.gallary').html('');
            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();
                reader.onload = function(event) {
                    $($.parseHTML('<span class="uploaded_pic"><img src="'+ event.target.result +'"></span>')).appendTo(placeToInsertImagePreview);
    
                }
                reader.readAsDataURL(input.files[i]);
            }
        }
    };
    
    $('#original').on('change', function() {
       // alert('11');
        imagesPreview(this, 'div.gallary');
    });
    });
</script>
<script>
        $(document).ready(function(){
        
          $(".openBTn_5").click(function(){
            
              $(".login_outer").show();
              $(".new_sty_pop4").show();
              $('html, body').animate({
              scrollTop: ($('.new_sty_pop4').offset().top - 85)+'px'
              }, 800);
          });
        
            //// send to friend section
        var src = "";
        $('#send_to_friend').click(function(event)
        {
            $("#send_friend").validate(
            {
                rules  : 
                {
                   
                    rejectionmessage        :   {required: true},
                   
                },
                messages : 
                {                
                    
                    rejectionmessage        : "Please enter rejection reason.",
                    
                },
            });
            
            var check=$("#send_friend").valid();
            
            if(check==true)
            {
                $('.for_button').hide();
                $('.loader1').show();
            }
           
            var reqData = 
            {
                'jsonrpc': '2.0',
                'method': 'request_price',
                'data': 
                    {
                        
                        'rejectionmessage'              :       $('#rejectionmessage').val(),
                        'emailid'                       :       $('#emailid').val(),
                        'user_name'                       :       $('#user_name').val(),
                        'user_id'                       :       $('#user_id').val(),
                    }
                
            };
           
            
           
            $.ajax(
            {
                url: src,
                dataType: "json",
                data: reqData,
                success: function(response) 
                {
                   if(response.result.success) 
                   {
                    
                    $('#rejectionmessage').val(''),
                    $('#emailid').val(''),
                    $('#user_name').val(''),
                    $('#user_id').val(''),
                    $(".login_outer").hide();$(".popupmain").hide()
                    $('.loader1').hide();
                    $('.for_button').show();
                    toastr.success('Document rejected succesfully')
                   } 
                },
                error:function(error) 
                {
                    console.log(error.responseText);
                }
            });
        });
        $("#crossBtn,.login_outer").click(function(){
        $(".login_outer").hide();$(".popupmain").hide()
        });



          
        });
</script> 
<script type="text/javascript">
    $(document).ready(function () {
        $("#change_password").validate({
            rules: {
                category: {
                    required: true
                }
            },
            messages: {
                category: {
                    required: "Category field is required"
                }
            }
        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#attribute').chosen();
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#validate_attribute").validate({
            rules: {
                attribute1: {
                    required: true
                },
                display_order: {
                    required: true,
                    number:true
                }
            },
            messages: {
                attribute1: {
                    required: "Attribute field is required"
                },
                display_order: {
                    required: "Display order field is required.",
                    number: "Please enter valid display order."
                }
            }
        });
    });
</script>
<!--............................end popup script................................--> 

<script src="{{URL::asset('public/adminassets/assets/js/timepicker.min.js')}}"></script>