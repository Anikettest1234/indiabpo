<base href="{{ url('') }}/" />
<link rel="shortcut icon" href="{{URL::asset('public/images/favicon.png')}}">

{{-- <link rel="manifest" href="/manifest.json"> --}}
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff"> 

<link href="{{URL::asset('public/adminassets/assets/plugins/ion-rangeslider/ion.rangeSlider.css')}}" rel="stylesheet" type="text/css">
<link href="{{URL::asset('public/adminassets/assets/plugins/ion-rangeslider/ion.rangeSlider.skinFlat.css')}}" rel="stylesheet" type="text/css">

<link href="{{URL::asset('public/adminassets/assets/css/jquery.ui.autocomplete.css')}}" type="text/css" rel="stylesheet" media="all"/>
<link href="{{URL::asset('public/adminassets/assets/css/jquery-ui.css')}}" type="text/css" rel="stylesheet" media="all"/>

<!-- DataTables -->
<link href="{{URL::asset('public/adminassets/assets/plugins/datatables/jquery.dataTables.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{URL::asset('public/adminassets/assets/plugins/datatables/buttons.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{URL::asset('public/adminassets/assets/plugins/datatables/fixedHeader.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{URL::asset('public/adminassets/assets/plugins/datatables/responsive.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{URL::asset('public/adminassets/assets/plugins/datatables/scroller.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />

<link href="{{URL::asset('public/adminassets/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<link href="{{URL::asset('public/adminassets/assets/css/core.css')}}" rel="stylesheet" type="text/css">
<link href="{{URL::asset('public/adminassets/assets/css/icons.css')}}" rel="stylesheet" type="text/css">
<link href="{{URL::asset('public/adminassets/assets/css/components.css')}}" rel="stylesheet" type="text/css">
<link href="{{URL::asset('public/adminassets/assets/css/pages.css')}}" rel="stylesheet" type="text/css">
<link href="{{URL::asset('public/adminassets/assets/css/menu.css')}}" rel="stylesheet" type="text/css">
<link href="{{URL::asset('public/adminassets/assets/css/responsive.css')}}" rel="stylesheet" type="text/css">
<link href="{{URL::asset('public/adminassets/assets/css/calender.css')}}" rel="stylesheet" type="text/css">
<link href="{{URL::asset('public/adminassets/assets/css/chosen.css')}}" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="{{URL::asset('public/adminassets/assets/css/timepicker.min.css')}}">
<link href="{{URL::to('public/adminassets/assets/css/jquery.toast.css')}}" rel="stylesheet" type="text/css">
<link href="{{URL::to('public/adminassets/assets/css/bootstrap-colorpicker.min.css')}}" rel="stylesheet" type="text/css"> 
<link href="{{URL::to('public/adminassets/assets/css/jquery.classybox.css')}}" rel="stylesheet" type="text/css"> 
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        
<style type="text/css">
.your-mail input[type='submit'] {
    color: #fff;
    font-family: 'Open Sans', sans-serif;
    font-size: 18px;
    margin-bottom: 29px;
    border-radius: 6px;
    background: #dc95ab !important;
    padding: 8px 13px;
}    
</style>
<script src="{{URL::asset('public/adminassets/assets/js/jquery.min.js')}}"></script>