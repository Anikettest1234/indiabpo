<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <div class="user-details">
            <div class="pull-left">
                <img src="{{ @Auth::guard('admin')->user()->image ? url('public/images/admin/'.Auth::guard('admin')->user()->image) : url('public/images/default.png') }}" alt="" class="thumb-md img-circle">
            </div>
            <div class="user-info">
                <div class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="font-size: 15px;">{{ Auth::guard('admin')->user()->name }} <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="{{ route('admin.change.password') }}"><i class="md md-face-unlock"></i> Profile<div class="ripple-wrapper"></div></a></li>
                        <li><a href="{{ route('admin.logout') }}"><i class="md md-settings-power"></i> Logout</a></li>
                    </ul>
                </div>
                <p class="text-muted m-0">Administrator</p>
            </div>
        </div>
        <!--- Divider -->
        <div id="sidebar-menu">
            <ul>
                <li>

                    <a href="{{ route('admin.dashboard') }}" class="waves-effect<?php if(Request::segment(2) == '' ) { echo 'subdrop active active1' ; }?>"><i class="fa fa-tachometer" aria-hidden="true"></i><span> Dashboard </span></a>
                </li>
                {{-- <li><a class="sidebar-sub-toggle waves-effect @if(in_array(Request::segment(2), ['premium-job-settings', 'subscription-settings'])) active @endif"><i class="fa fa-cog "></i>    <span class="sidebar-collapse-icon ti-angle-down">Settings</span></a>
                  <ul>
                    <li><a href="{{ route('admin.premium.job') }}">Premium Job Settings</a></li>
                    <li><a href="{{ route('admin.subscription.setting') }}">Subscription Settings</a></li>
                  </ul>
                </li> --}}
                <li>
                    <a href="{{ route('admin.categories') }}" class="waves-effect @if(in_array(Request::segment(2), ['categories', 'add-categories'])) active @endif"><i class="fa fa-list-alt" aria-hidden="true"></i><span> Category </span></a>
                </li>
                <li>
                    <a href="{{ route('admin.skills') }}" class="waves-effect @if(in_array(Request::segment(2), ['skills', 'add-skills','edit-skills'])) active @endif"><i class="fa fa-list-alt" aria-hidden="true"></i><span> Skills </span></a>
                </li>
                {{-- <li>
                    <a href="{{ route('admin.employers') }}" class="waves-effect @if(in_array(Request::segment(2), ['employers', 'companies', 'job', 'training'])) active @endif"><i class="fa fa-id-card" aria-hidden="true"></i><span> Employer </span></a>
                </li>
                <li>
                    <a href="{{ route('admin.jobs') }}" class="waves-effect @if(in_array(Request::segment(2), ['jobs', 'applied-user'])) active @endif"><i class="fa fa-book fa-fw" aria-hidden="true"></i><span> Jobs </span></a>
                </li>
                <li>
                    <a href="{{ route('admin.jobseekers') }}" class="waves-effect @if(in_array(Request::segment(2), ['jobseekers', 'applied-job'])) active @endif"><i class="fa fa-users" aria-hidden="true"></i><span> Job Seekers </span></a>
                </li>
                <li>
                    <a href="{{ route('admin.training') }}" class="waves-effect @if(in_array(Request::segment(2), ['trainings'])) active @endif"><i class="fa fa-certificate" aria-hidden="true"></i><span> Training </span></a>
                </li>
                <li>
                    <a href="{{ route('admin.career') }}" class="waves-effect @if(in_array(Request::segment(2), ['career', 'add-career', 'edit-career'])) active @endif"><i class="fa fa-sitemap" aria-hidden="true"></i><span> Career </span></a>
                </li> --}}
            
            </ul>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<!-- Left Sidebar End -->
