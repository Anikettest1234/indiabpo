<!DOCTYPE html>
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <base href="{{URL::asset('public/adminassets/')}}">
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width,initial-scale=1">
            <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
            <meta name="author" content="Coderthemes">
            <meta name="csrf-token" content="{{ csrf_token() }}">
            {{-- <link rel="shortcut icon" href="assets/images/favicon_1.png"> --}}
            <title>@yield('title')</title>
            
            <link rel="apple-touch-icon" sizes="57x57" href="{{url('public/fav/apple-icon-57x57.png')}}">
            <link rel="apple-touch-icon" sizes="60x60" href="{{url('public/fav/apple-icon-60x60.png')}}">
            <link rel="apple-touch-icon" sizes="72x72" href="{{url('public/fav/apple-icon-72x72.png')}}">
            <link rel="apple-touch-icon" sizes="76x76" href="{{url('public/fav/apple-icon-76x76.png')}}">
            <link rel="apple-touch-icon" sizes="114x114" href="{{url('public/fav/apple-icon-114x114.png')}}">
            <link rel="apple-touch-icon" sizes="120x120" href="{{url('public/fav/apple-icon-120x120.png')}}">
            <link rel="apple-touch-icon" sizes="144x144" href="{{url('public/fav/apple-icon-144x144.png')}}">
            <link rel="apple-touch-icon" sizes="152x152" href="{{url('public/fav/apple-icon-152x152.png')}}">
            <link rel="apple-touch-icon" sizes="180x180" href="{{url('public/fav/apple-icon-180x180.png')}}">
            <link rel="icon" type="image/png" sizes="192x192"  href="{{url('public/fav/android-icon-192x192.png')}}">
            <link rel="icon" type="image/png" sizes="32x32" href="{{url('public/fav/favicon-32x32.png')}}">
            <link rel="icon" type="image/png" sizes="96x96" href="{{url('public/fav/favicon-96x96.png')}}">
            <link rel="icon" type="image/png" sizes="16x16" href="{{url('public/fav/favicon-16x16.png')}}">
            <link rel="manifest" href="{{url('public/fav/manifest.json')}}">
            <meta name="msapplication-TileColor" content="#ffffff">
            <meta name="msapplication-TileImage" content="{{url('public/fav/ms-icon-144x144.png')}}">
            <meta name="theme-color" content="#ffffff">

            @include('admin/includes/links')
            <style type="text/css" media="screen">
                .error{
                    color: red !important;
                    font-size: 15px !important;
                    font-weight: bold !important;
                }  
            </style>
        </head>
        <body class="fixed-left">
        <div class="login_outer" style="display:none;"></div>
            <div id="wrapper">
                @yield('header')
                @yield('sidebar')
                @yield('content')
                @yield('rightsidebar')
                @yield('footer')
                @yield('scripts')
                
            </div>
            <div class="loading" style="display: none;">Loading&#8230;</div>
            @if(@session()->get('validator_error'))
                <script>
                    @foreach(session()->get('validator_error') as $key=>$item)
                        toastr.error('{{$item[0]}}');
                    @endforeach
                </script>
                @endif
    
                @if($errors->any())
                <script>
                    @foreach($errors->all() as $er)
                        toastr.error('{{$er}}');
                    @endforeach
                </script>
                @endif

                @if(@session()->get('success'))
                <script>           
                    toastr.success("{{session()->get('success')}}");
                </script>
                @endif

                @if(@session()->get('error'))
                <script>           
                    toastr.error("{{session()->get('error')}}");
                </script>
                @endif
        </body>
    </html>
