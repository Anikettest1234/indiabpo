@extends('layouts.app')
@section('title', 'IndiaBpo | Home ')
@section('links')
@include('includes.links')
@endsection
@section('header')
@include('includes.header')
@endsection
@section('content')
    <section class="search-body">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-3">
                    <div class="lft-search">
                        <h3>Filter jobs  <a href="#"><img src="{{ URL::to('public/assets/img/fcrss.png')}}"></a></h3>
                        <div class="collabse_area">
                          <div class="">
                            <div class="col-sm-12 npdb">
                              <div class="accordion-group">
                                <div class="accordion-heading"> <a class="toggle1"  href="javascript:void(0);"> 
                                <i class="icofont-plus"></i> Roles </a> </div>
                                <div id="collapseOne" class="accordion-body collapse hdd" style="height:auto;">
                                  <div class="accordion-inner">
                                    <div class="form-group sub_categories">
                                        <div class="categories_check">
                                            <div class="main-check">
                                                <input type="text" class="type-srch" name="" placeholder="Type here...">
                                            </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                       
                        <div class="collabse_area">
                          <div class="">
                            <div class="col-sm-12 npdb">
                              <div class="accordion-group">
                                <div class="accordion-heading"> <a class="toggle1"  href="javascript:void(0);"> <i class="icofont-plus"></i> Show results from </a> </div>
                                <div id="collapseOne" class="accordion-body collapse hdd" style="height:auto;">
                                  <div class="accordion-inner">
                                    <div class="form-group sub_categories">
                                      <div class="categories_check">
                                        <div class="all-radio">
                                            <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1" checked>
                                                    <label class="form-check-label" for="inlineRadio1">All Locations</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2">
                                                    <label class="form-check-label" for="inlineRadio2">All India</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option2">
                                                    <label class="form-check-label" for="inlineRadio3">International</label>
                                                </div>
                                        </div>                                
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                        <div class="collabse_area">
                          <div class="">
                            <div class="col-sm-12 npdb">
                              <div class="accordion-group">
                                <div class="accordion-heading"> <a class="toggle1"  href="javascript:void(0);"> <i class="icofont-plus"></i> Monthly Salary </a> </div>
                                <div id="collapseOne" class="accordion-body collapse hdd" style="height:auto;">
                                  <div class="accordion-inner">
                                    <div class="form-group sub_categories">
                                      <div class="categories_check">
                                        <div class="checkbox-group">
                                          <input id="checkiz30" type="checkbox">
                                          <label for="checkiz30" class="Fs16"> <span class="check find_chek"></span> <span class="box W25 boxx"></span> ₹ 5,000 and above </label>
                                        </div>
                                        <div class="checkbox-group">
                                          <input id="checkiz31" type="checkbox">
                                          <label for="checkiz31" class="Fs16"> <span class="check find_chek"></span> <span class="box W25 boxx"></span> ₹ 6,000 and above </label>
                                        </div>
                                        <div class="checkbox-group">
                                          <input id="checkiz32" type="checkbox">
                                          <label for="checkiz32" class="Fs16"> <span class="check find_chek"></span> <span class="box W25 boxx"></span> ₹ 7,000 and above </label>
                                        </div>
                                        
                                        <div class="checkbox-group">
                                          <input id="checkiz33" type="checkbox">
                                          <label for="checkiz33" class="Fs16"> <span class="check find_chek"></span> <span class="box W25 boxx"></span> ₹ 8,000 and above </label>
                                        </div>
                                        
                                        <div class="checkbox-group">
                                          <input id="checkiz34" type="checkbox">
                                          <label for="checkiz34" class="Fs16"> <span class="check find_chek"></span> <span class="box W25 boxx"></span> ₹ 9,000 and above </label>
                                        </div>
                                        
                                        <div class="checkbox-group">
                                          <input id="checkiz35" type="checkbox">
                                          <label for="checkiz35" class="Fs16"> <span class="check find_chek"></span> <span class="box W25 boxx"></span> ₹ 10,000 and above </label>
                                        </div>

                                        <div class="checkbox-group">
                                          <input id="checkiz350" type="checkbox">
                                          <label for="checkiz350" class="Fs16"> <span class="check find_chek"></span> <span class="box W25 boxx"></span> ₹ 11,000 and above </label>
                                        </div>
                                        
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                        <div class="collabse_area">
                          <div class="">
                            <div class="col-sm-12 npdb">
                              <div class="accordion-group">
                                <div class="accordion-heading"> <a class="toggle1"  href="javascript:void(0);"> <i class="icofont-plus"></i> Job Type </a> </div>
                                <div id="collapseOne" class="accordion-body collapse hdd" style="height:auto;">
                                  <div class="accordion-inner">
                                    <div class="form-group sub_categories">
                                      <div class="categories_check">
                                        <div class="checkbox-group">
                                          <input id="checki27" type="checkbox">
                                          <label for="checki27" class="Fs16"> <span class="check find_chek"></span> <span class="box W25 boxx"></span> Work From Home </label>
                                        </div>
                                        <div class="checkbox-group">
                                          <input id="checkiz26" type="checkbox">
                                          <label for="checkiz26" class="Fs16"> <span class="check find_chek"></span> <span class="box W25 boxx"></span> Part Time Jobs</label>
                                        </div>
                                        <div class="checkbox-group">
                                          <input id="checkiz25" type="checkbox">
                                          <label for="checkiz25" class="Fs16"> <span class="check find_chek"></span> <span class="box W25 boxx"></span> Full Time Jobs </label>
                                        </div>
                                        <div class="checkbox-group">
                                          <input id="checkiz26" type="checkbox">
                                          <label for="checkiz26" class="Fs16"> <span class="check find_chek"></span> <span class="box W25 boxx"></span> Internships </label>
                                        </div>
                                        <div class="checkbox-group">
                                          <input id="checkiz27" type="checkbox">
                                          <label for="checkiz27" class="Fs16"> <span class="check find_chek"></span> <span class="box W25 boxx"></span> Work Abroad </label>
                                        </div>
                                        
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        
                        <div class="collabse_area">
                          <div class="">
                            <div class="col-sm-12 npdb">
                              <div class="accordion-group">
                                <div class="accordion-heading"> <a class="toggle1"  href="javascript:void(0);"> <i class="icofont-plus"></i> Company</a> </div>
                                <div id="collapseOne" class="accordion-body collapse hdd" style="height:auto;">
                                  <div class="accordion-inner">
                                    <div class="form-group sub_categories">
                                      <div class="categories_check">
                                        <div class="main-check">
                                                <input type="text" class="type-srch" name="" placeholder="Type here...">
                                            </div>
                                        <div class="all-radio">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio4" value="option1" checked>
                                                <label class="form-check-label" for="inlineRadio4">Telecom</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio5" value="option2">
                                                <label class="form-check-label" for="inlineRadio5">Bpo</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio6" value="option2">
                                                <label class="form-check-label" for="inlineRadio6">Info Data Solution</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio7" value="option2">
                                                <label class="form-check-label" for="inlineRadio7">www.symetricservices.com</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio8" value="option2">
                                                <label class="form-check-label" for="inlineRadio8">DATA ESOLUTIONS</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio9" value="option2">
                                                <label class="form-check-label" for="inlineRadio9">Apex Enterprises</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio10" value="option2">
                                                <label class="form-check-label" for="inlineRadio10">GRS INFOTECH</label>
                                            </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>                            
                        
                        <div class="text-center">
                            <button type="submit" class="btn create-ac-btn">Apply</button>
                        </div>
                    </div>
                    <div class="beware-area">
                        <h3>Beware of Job Scams!</h3>
                        <p>Demo does not promise a Job or an Interview in exchange of money.</p>
                        <a href="#">Know More</a>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-6">
                    <div class="top-total">
                        <h5>Found 240 projects</h5>
                        <div class="sort-filter">
                            <p>Sort by </p>
                            <div class="job-category-area">
                                <select>
                                    <option>Sort by Popular</option>
                                    <option>Sort by Recent</option>
                                    <option>Sort by Oldest</option>
                                </select>   
                            </div>
                        </div>
                    </div>
                    <div class="all-search-area">
                        <div class="search-one">
                            <h3><a href="#">No Experience require basic knowledge of computer </a></h3>
                            <ul>
                                <li>
                                   <span><i class="icofont-rupee"></i></span>
                                   <h4>Monthly</h4>
                                   <h5>24,000 - 28,000</h5> 
                                </li>
                                <li>
                                   <span><i class="icofont-tie"></i></span>
                                   <h4>Job Type</h4>
                                   <h5>Work From Home</h5> 
                                </li>
                                <li>
                                   <span><i class="icofont-building-alt"></i></span>
                                   <h4>Company</h4>
                                   <h5>Greet Indo Typing</h5> 
                                </li>
                                <li>
                                   <span><i class="icofont-clock-time"></i></span>
                                   <h4>Experience</h4>
                                   <h5>Not mentioned</h5> 
                                </li>
                            </ul>
                            <ul class="locations-div">
                                <p><i class="icofont-user-alt-7"></i> Data Entry /Back Office</p>
                                <p><i class="icofont-location-pin"></i> <strong>Mumbai </strong>- Aarey Road, Beverly Park, Chembur East, Haffkine Institute  + 1 more </p>
                            </ul>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing eiusmod tempor incididunt ut laibore et dolore magna aliqua.labor consectetur is adipiscing eiusmod tempor incididunt ut labore et dolore magna aliqua text labor labore et dolore magna aliqua.</p> 

                            <div class="left-50">
                                <p>
                                Posted by <strong>Info Data Solution</strong><br>
                                05 Sep 2020, 04:14 PM
                                </p>
                            </div>
                            <div class="right-50">
                               <p><i class="icofont-check-circled"></i> Phone verified</p>
                               <a href="#">Apply Now</a>
                            </div>
                            
                        </div>

                        <div class="search-one">
                            <h3><a href="#">No Experience require basic knowledge of computer </a></h3>
                            <ul>
                                <li>
                                   <span><i class="icofont-rupee"></i></span>
                                   <h4>Monthly</h4>
                                   <h5>24,000 - 28,000</h5> 
                                </li>
                                <li>
                                   <span><i class="icofont-tie"></i></span>
                                   <h4>Job Type</h4>
                                   <h5>Work From Home</h5> 
                                </li>
                                <li>
                                   <span><i class="icofont-building-alt"></i></span>
                                   <h4>Company</h4>
                                   <h5>Greet Indo Typing</h5> 
                                </li>
                                <li>
                                   <span><i class="icofont-clock-time"></i></span>
                                   <h4>Experience</h4>
                                   <h5>Not mentioned</h5> 
                                </li>
                            </ul>
                            <ul class="locations-div">
                                <p><i class="icofont-user-alt-7"></i> Data Entry /Back Office</p>
                                <p><i class="icofont-location-pin"></i> <strong>Mumbai </strong>- Aarey Road, Beverly Park, Chembur East, Haffkine Institute  + 1 more </p>
                            </ul>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing eiusmod tempor incididunt ut laibore et dolore magna aliqua.labor consectetur is adipiscing eiusmod tempor incididunt ut labore et dolore magna aliqua text labor labore et dolore magna aliqua.</p> 

                            <div class="left-50">
                                <p>
                                Posted by <strong>Info Data Solution</strong><br>
                                05 Sep 2020, 04:14 PM
                                </p>
                            </div>
                            <div class="right-50">
                               <p><i class="icofont-check-circled"></i> Phone verified</p>
                               <a href="#">Apply Now</a>
                            </div>
                            
                        </div>

                        <div class="search-one">
                            <h3><a href="#">No Experience require basic knowledge of computer </a></h3>
                            <ul>
                                <li>
                                   <span><i class="icofont-rupee"></i></span>
                                   <h4>Monthly</h4>
                                   <h5>24,000 - 28,000</h5> 
                                </li>
                                <li>
                                   <span><i class="icofont-tie"></i></span>
                                   <h4>Job Type</h4>
                                   <h5>Work From Home</h5> 
                                </li>
                                <li>
                                   <span><i class="icofont-building-alt"></i></span>
                                   <h4>Company</h4>
                                   <h5>Greet Indo Typing</h5> 
                                </li>
                                <li>
                                   <span><i class="icofont-clock-time"></i></span>
                                   <h4>Experience</h4>
                                   <h5>Not mentioned</h5> 
                                </li>
                            </ul>
                            <ul class="locations-div">
                                <p><i class="icofont-user-alt-7"></i> Data Entry /Back Office</p>
                                <p><i class="icofont-location-pin"></i> <strong>Mumbai </strong>- Aarey Road, Beverly Park, Chembur East, Haffkine Institute  + 1 more </p>
                            </ul>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing eiusmod tempor incididunt ut laibore et dolore magna aliqua.labor consectetur is adipiscing eiusmod tempor incididunt ut labore et dolore magna aliqua text labor labore et dolore magna aliqua.</p> 

                            <div class="left-50">
                                <p>
                                Posted by <strong>Info Data Solution</strong><br>
                                05 Sep 2020, 04:14 PM
                                </p>
                            </div>
                            <div class="right-50">
                               <p><i class="icofont-check-circled"></i> Phone verified</p>
                               <a href="#">Apply Now</a>
                            </div>
                            
                        </div>

                        <div class="search-one">
                            <h3><a href="#">No Experience require basic knowledge of computer </a></h3>
                            <ul>
                                <li>
                                   <span><i class="icofont-rupee"></i></span>
                                   <h4>Monthly</h4>
                                   <h5>24,000 - 28,000</h5> 
                                </li>
                                <li>
                                   <span><i class="icofont-tie"></i></span>
                                   <h4>Job Type</h4>
                                   <h5>Work From Home</h5> 
                                </li>
                                <li>
                                   <span><i class="icofont-building-alt"></i></span>
                                   <h4>Company</h4>
                                   <h5>Greet Indo Typing</h5> 
                                </li>
                                <li>
                                   <span><i class="icofont-clock-time"></i></span>
                                   <h4>Experience</h4>
                                   <h5>Not mentioned</h5> 
                                </li>
                            </ul>
                            <ul class="locations-div">
                                <p><i class="icofont-user-alt-7"></i> Data Entry /Back Office</p>
                                <p><i class="icofont-location-pin"></i> <strong>Mumbai </strong>- Aarey Road, Beverly Park, Chembur East, Haffkine Institute  + 1 more </p>
                            </ul>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing eiusmod tempor incididunt ut laibore et dolore magna aliqua.labor consectetur is adipiscing eiusmod tempor incididunt ut labore et dolore magna aliqua text labor labore et dolore magna aliqua.</p> 

                            <div class="left-50">
                                <p>
                                Posted by <strong>Info Data Solution</strong><br>
                                05 Sep 2020, 04:14 PM
                                </p>
                            </div>
                            <div class="right-50">
                               <p><i class="icofont-check-circled"></i> Phone verified</p>
                               <a href="#">Apply Now</a>
                            </div>
                            
                        </div>

                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-3">
                    <div class="hire-box">
                        <h4>Hire people for your business</h4>
                        <div class="main-hire">
                            <span><i class="icofont-ui-user-group"></i></span>
                            <p>Find over 1 crore resumes</p>
                            <a href="#">Hire</a>
                        </div>
                    </div>

                    <div class="urgent-box">
                        <div class="top-urgent">
                            <span><img src="{{ URL::to('public/assets/img/logo.png')}}" alt="Logo"></span>
                            <p>Office Assistant/Helper</p>
                            <h3>Sri Balaji Group</h3>
                        </div>
                        <div class="botom-urgent">
                            <h3><a href="#">Urgent Requirement For Helper..</a></h3>
                            <p><i class="icofont-rupee"></i> 10000 - 12000 monthly</p>
                            <p><i class="icofont-clock-time"></i> Full Time Jobs</p>
                            <p><i class="icofont-location-pin"></i> Kolkata</p>
                            <a href="#" class="see-dtls">See Details</a>
                        </div>
                    </div>

                    <div class="adds-area">
                        <img src="{{ URL::to('public/assets/img/ads1.jpg')}}" alt="#">
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('footer')
@include('includes.footer')
@endsection  
@section('scripts')
@include('includes.scripts')
@endsection


        

       
       