<!DOCTYPE html>
<html lang="zxx">    
<!-- Mirrored from templates.hibootstrap.com/gable/default/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 05 Sep 2020 13:08:21 GMT -->
<head>
        @yield('meta')
    
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    @yield('links')
        
    </head>
    <body>

        @yield('header')

        @yield('content')

        @yield('footer')

        @yield('scripts')

        <!-- Preloader -->
        <!-- <div class="loader">
            <div class="d-table">
                <div class="d-table-cell">
                    <div class="spinner">
                        <div class="rect1"></div>
                        <div class="rect2"></div>
                        <div class="rect3"></div>
                        <div class="rect4"></div>
                        <div class="rect5"></div>
                    </div>
                </div>
            </div>
        </div> -->
        <!-- End Preloader -->

        <!-- Start Navbar Area -->
     
        <!-- End Navbar Area -->

        


        

        <!-- Footer -->
        
        <!-- End Footer -->


        

    </body>

<!-- Mirrored from templates.hibootstrap.com/gable/default/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 05 Sep 2020 13:09:00 GMT -->
</html>